<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}


require IA_ROOT . '/addons/yunphp_shop/version.php';
require IA_ROOT . '/addons/yunphp_shop/defines.php';
require YUNPHP_SHOP_INC . 'functions.php';
require YUNPHP_SHOP_INC . 'receiver.php';
class Yunphp_shopModuleReceiver extends Receiver
{
	public function receive()
	{
		parent::receive();
	}
}


?>