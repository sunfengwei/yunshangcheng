<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
class Receive_YunphpShopPage extends MobileLoginPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$receiveid = intval($_GPC['receiveid']);
		$openid = $_W['openid'];
		$sql = 'SELECT * FROM ' . tablename('yunphp_shop_receive') . ' WHERE `uniacid`=:uniacid AND `id`=:id limit 1';
		$receive = pdo_fetch($sql, array(':uniacid' => $_W['uniacid'], ':id' => $receiveid));
		if (empty($receive['tasktype'])) {
			$total = pdo_fetchcolumn('SELECT COUNT(*) FROM ' . tablename('yunphp_shop_receive_log') . ' WHERE `uniacid`=:uniacid AND `receiveid`=:receiveid AND `openid`=:openid limit 1', array(':uniacid' => $_W['uniacid'], ':receiveid' => $receiveid, ':openid' => $openid));
			if (empty($total)) {
				$tasktype = 0;
			} else {
				$tasktype = 1;
			}
		} else {
			$tasktype = 0;
		}
		if (empty($tasktype)) {
			$member_info = m('member')->getMember($openid);
			
			
			
			
			$this->reward($member_info, $receive);
			header('location: ' . mobileUrl('sale/coupon/my'), true);
			
			
			
			
			
		} else {
			header('location: ' . mobileUrl('sale/coupon/my'), true);
		}
	}
	//加入主动领取优惠券
	public function posterbyreceive($list, $openid, $gettype) 
	{
		global $_W;
		global $_GPC;
		$pposter = p('poster');
		if (!($pposter)) 
		{
			return;
		}
		$num = 0;
		$showkey = random(20);
		$data = m('common')->getPluginset('coupon');
		if (empty($data['showtemplate']) || ($data['showtemplate'] == 2)) 
		{
			$url = $this->getUrl('sale/coupon/my/showcoupons3', array('key' => $showkey), true);
		}
		else 
		{
			$url = $this->getUrl('sale/coupon/my/showcoupons', array('key' => $showkey), true);
		}
		foreach ($list as $taskdata ) 
		{
			$couponnum = 0;
			$couponnum = intval($taskdata['sendnum']);
			$num += $couponnum;
			$i = 1;
			while ($i <= $couponnum) 
			{
				$couponlog = array('uniacid' => $_W['uniacid'], 'openid' => $openid, 'logno' => m('common')->createNO('coupon_log', 'logno', 'CC'), 'couponid' => $taskdata['couponid'], 'status' => 1, 'paystatus' => -1, 'creditstatus' => -1, 'createtime' => time(), 'getfrom' => intval($gettype));
				pdo_insert('ty_shop_coupon_log', $couponlog);
				$data = array('uniacid' => $_W['uniacid'], 'openid' => $openid, 'couponid' => $taskdata['couponid'], 'gettype' => intval($gettype), 'gettime' => time());
				pdo_insert('ty_shop_coupon_data', $data);
				$coupondataid = pdo_insertid();
				$data = array('showkey' => $showkey, 'uniacid' => $_W['uniacid'], 'openid' => $openid, 'coupondataid' => $coupondataid);
				pdo_insert('ty_shop_coupon_sendshow', $data);
				++$i;
			}
			
		}
		$msg = '恭喜您获得' . $num . '张优惠券!';
		$ret = m('message')->sendCustomNotice($openid, $msg, $url);
	}
	
	
	public function reward($member_info, $receive) 
	{
		if (empty($member_info) || empty($receive)) 
		{
			return false;
		}
		global $_W;
		
		load()->func('logging');
		$reward_data = unserialize($receive['reward_data']);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			$reward = serialize($reward_data['rec']);
			$sub_reward = serialize($reward_data['sub']);
			$reward_log = array('uniacid' => $_W['uniacid'], 'openid' => $member_info['openid'], 'receiveid' => $receive['id'], 'tasktype' => $receive['tasktype'], 'subdata' => $sub_reward, 'recdata' => $reward, 'createtime' => time());
			
			pdo_insert('yunphp_shop_receive_log', $reward_log);
			$log_id = pdo_insertid();
			foreach ($reward_data as $key => $val ) 
			{
				if ($key == 'rec') 
				{
					if (isset($val['credit']) && (0 < $val['credit'])) 
					{
						m('member')->setCredit($member_info['openid'], 'credit1', $val['credit'], array(0, '活动赠送积分+' . $val['credit']));
					}
					if (isset($val['money']) && (0 < $val['money']['num'])) 
					{
						$pay = $val['money']['num'];
						if ($val['money']['type'] == 1) 
						{
							$pay *= 100;
						}
						m('finance')->pay($member_info['openid'], $val['money']['type'], $pay, '', '活动赠送奖励', false);
					}
					if (isset($val['bribery']) && (0 < $val['bribery'])) 
					{
						$setting = uni_setting($_W['uniacid'], array('payment'));
						if (!(is_array($setting['payment']))) 
						{
							return error(1, '没有设定支付参数');
						}
						$sec = m('common')->getSec();
						$sec = iunserializer($sec['sec']);
						$certs = $sec;
						$wechat = $setting['payment']['wechat'];
						$sql = 'SELECT `key`,`secret` FROM ' . tablename('account_wechats') . ' WHERE `uniacid`=:uniacid limit 1';
						$row = pdo_fetch($sql, array(':uniacid' => $_W['uniacid']));
						$tid = rand(1, 1000) . time() . rand(1, 10000);
						$params = array('openid' => $member_info['openid'], 'tid' => $tid, 'send_name' => '推荐奖励', 'money' => $val['bribery'], 'wishing' => '推荐奖励', 'act_name' => $receive['taskname'], 'remark' => '推荐奖励');
						$wechat = array('appid' => $row['key'], 'mchid' => $wechat['mchid'], 'apikey' => $wechat['apikey'], 'certs' => $certs);
						$err = m('common')->sendredpack($params, $wechat);
						if (!(is_error($err))) 
						{
							$reward = unserialize($reward);
							$reward['briberyOrder'] = $tid;
							$reward = serialize($reward);
							$upgrade = array('recdata' => $reward);
							pdo_update('yunphp_shop_receive_log', $upgrade, array('id' => $log_id));
						}
					}
					if (isset($val['coupon']) && !(empty($val['coupon']))) 
					{
						$cansendreccoupon = false;
						$plugin_coupon = com('coupon');
						unset($val['coupon']['total']);
						foreach ($val['coupon'] as $k => $v ) 
						{
							if ($plugin_coupon) 
							{
								if (!(empty($v['id'])) && (0 < $v['couponnum'])) 
								{
									$reccoupon = $plugin_coupon->getCoupon($v['id']);
									if (!(empty($reccoupon))) 
									{
										$cansendreccoupon = true;
									}
								}
							}
							if ($cansendreccoupon) 
							{
								$plugin_coupon->taskposter($member_info, $v['id'], $v['couponnum']);
							}
						}
					}
				}
				else if ($key == 'sub') 
				{
					if (0 < $val['credit']) 
					{
						m('member')->setCredit($openid, 'credit1', $val['credit'], array(0, '扫码关注积分+' . $val['credit']));
					}
					if (0 < $val['bribery']) 
					{
						$setting = uni_setting($_W['uniacid'], array('payment'));
						if (!(is_array($setting['payment']))) 
						{
							return error(1, '没有设定支付参数');
						}
						$sec = m('common')->getSec();
						$sec = iunserializer($sec['sec']);
						$certs = $sec;
						$wechat = $setting['payment']['wechat'];
						$sql = 'SELECT `key`,`secret` FROM ' . tablename('account_wechats') . ' WHERE `uniacid`=:uniacid limit 1';
						$row = pdo_fetch($sql, array(':uniacid' => $_W['uniacid']));
						$tid = rand(1, 1000) . time() . rand(1, 10000);
						$params = array('openid' => $openid, 'tid' => $tid, 'send_name' => '推荐奖励', 'money' => $val['bribery'], 'wishing' => '推荐奖励', 'act_name' => $receive['taskname'], 'remark' => '推荐奖励');
						$wechat = array('appid' => $row['key'], 'mchid' => $wechat['mchid'], 'apikey' => $wechat['apikey'], 'certs' => $certs);
						$err = m('common')->sendredpack($params, $wechat);
						if (!(is_error($err))) 
						{
							$sub_reward = unserialize($sub_reward);
							$sub_reward['briberyOrder'] = $tid;
							$sub_reward = serialize($sub_reward);
							$upgrade = array('subdata' => $sub_reward);
							pdo_update('yunphp_shop_task_log', $upgrade, array('id' => $log_id));
						}
						else 
						{
							logging_run('bribery' . $err['message']);
						}
					}
					if (0 < $val['money']['num']) 
					{
						$pay = $val['money']['num'];
						if ($val['money']['type'] == 1) 
						{
							$pay *= 100;
						}
						$res = m('finance')->pay($openid, $val['money']['type'], $pay, '', '任务活动奖励', false);
						if (is_error($res)) 
						{
							logging_run($res['message']);
						}
					}
					if (isset($val['coupon']) && isset($val['coupon']) && !(empty($val['coupon']))) 
					{
						$cansendreccoupon = false;
						$plugin_coupon = com('coupon');
						unset($val['coupon']['total']);
						foreach ($val['coupon'] as $k => $v ) 
						{
							if ($plugin_coupon) 
							{
								if (!(empty($v['id'])) && (0 < $v['couponnum'])) 
								{
									$reccoupon = $plugin_coupon->getCoupon($v['id']);
									if (!(empty($reccoupon))) 
									{
										$cansendreccoupon = true;
									}
								}
							}
							if ($cansendreccoupon) 
							{
								$plugin_coupon->taskposter($member_info, $v['id'], $v['couponnum']);
							}
						}
					}
				}
			}
			m('message')->sendCustomNotice($member_info['openid'], '亲爱的' . $member_info['nickname'] . '恭喜您领取了活动奖励，请点击查看详情。', mobileUrl('sale/coupon/my', array('tabpage' => 'complete'), true));
			
			
		
		
	}
	
	
	
	
	
}