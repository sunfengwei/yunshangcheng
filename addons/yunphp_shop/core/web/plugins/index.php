<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
class Index_YunphpShopPage extends WebPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$category = m('plugin')->getList(1);
		$apps = false;
		if (($_W['role'] == 'founder') || empty($_W['role'])) 
		{
			$apps = true;
		}
		include $this->template();
	}
}
?>