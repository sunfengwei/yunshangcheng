<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
class Index_YunphpShopPage extends ComWebPage 
{
	public function __construct($_com = 'coupon') 
	{
		parent::__construct($_com);
	}
	public function main() 
	{
		global $_W;
		global $_GPC;
		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$condition = ' uniacid = :uniacid and merchid=0';
		$params = array(':uniacid' => $_W['uniacid']);
		if (!(empty($_GPC['keyword']))) 
		{
			$_GPC['keyword'] = trim($_GPC['keyword']);
			$condition .= ' AND couponname LIKE :couponname';
			$params[':couponname'] = '%' . trim($_GPC['keyword']) . '%';
		}
		if (!(empty($_GPC['catid']))) 
		{
			$_GPC['catid'] = trim($_GPC['catid']);
			$condition .= ' AND catid = :catid';
			$params[':catid'] = (int) $_GPC['catid'];
		}
		if (empty($starttime) || empty($endtime)) 
		{
			$starttime = strtotime('-1 month');
			$endtime = time();
		}
		if (!(empty($_GPC['time']['start'])) && !(empty($_GPC['time']['end']))) 
		{
			$starttime = strtotime($_GPC['time']['start']);
			$endtime = strtotime($_GPC['time']['end']);
			if (!(empty($starttime))) 
			{
				$condition .= ' AND createtime >= :starttime';
				$params[':starttime'] = $starttime;
			}
			if (!(empty($endtime))) 
			{
				$condition .= ' AND createtime <= :endtime';
				$params[':endtime'] = $endtime;
			}
		}
		if ($_GPC['gettype'] != '') 
		{
			$condition .= ' AND gettype = :gettype';
			$params[':gettype'] = intval($_GPC['gettype']);
		}
		if ($_GPC['type'] != '') 
		{
			$condition .= ' AND coupontype = :coupontype';
			$params[':coupontype'] = intval($_GPC['type']);
		}
		$sql = 'SELECT * FROM ' . tablename('yunphp_shop_receive') . ' ' . ' where  1 and ' . $condition . ' ORDER BY displayorder DESC,id DESC LIMIT ' . (($pindex - 1) * $psize) . ',' . $psize;
		$list = pdo_fetchall($sql, $params);
		
		
		
		
		foreach ($list as &$row ) 
		{
			
			$row['reward_data'] = unserialize($row['reward_data']);
			
			$url = mobileUrl('sale/receive', array('receiveid' => $row['id']), true);
			$row['qrcode'] = m('qrcode')->createQrcode($url);
		}
		unset($row);
		$total = pdo_fetchcolumn('SELECT COUNT(*) FROM ' . tablename('yunphp_shop_receive') . ' where 1 and ' . $condition, $params);
		$pager = pagination($total, $pindex, $psize);
		
		include $this->template();
	}
	public function add() 
	{
		$this->post();
	}
	public function edit() 
	{
		$this->post();
	}
	protected function post() 
	{
		global $_W;
		global $_GPC;
		$uniacid = intval($_W['uniacid']);
		$id = intval($_GPC['id']);
		if (!(empty($id))) 
		{
			$item = pdo_fetch('SELECT *  FROM ' . tablename('yunphp_shop_receive') . ' WHERE uniacid = ' . $uniacid . ' and id =' . $id);
			$item = set_medias($item, array('thumb'));
			if (!(empty($item['couponid']))) 
			{
				$coupon = pdo_fetch('SELECT id,couponname as title , thumb  FROM ' . tablename('yunphp_shop_coupon') . ' WHERE uniacid = ' . $uniacid . ' and id =' . $item['couponid']);
			}
		}
		if ($_W['ispost']) 
		{
			if (intval($_GPC['sendnum']) < 1) 
			{
				show_json(0, '发送数量不能小于1');
			}
			if (intval($_GPC['num']) < 0) 
			{
				show_json(0, '剩余数量不能小于0');
			}
			
			$data = array('uniacid' => $uniacid, 'enough' => floatval($_GPC['enough']), 'couponid' => intval($_GPC['couponid']), 'starttime' => strtotime($_GPC['sendtime']['start']), 'endtime' => strtotime($_GPC['sendtime']['end']) + 86399, 'sendnum' => intval($_GPC['sendnum']), 'num' => intval($_GPC['num']), 'sendpoint' => intval($_GPC['sendpoint']), 'status' => intval($_GPC['status']), 'tasktype' => intval($_GPC['tasktype']), 'taskname' => trim($_GPC['taskname']));
			
			$reward = array();
			$rec_reward = htmlspecialchars_decode($_GPC['rec_reward_data']);
			$rec_reward = json_decode($rec_reward, 1);
			$rec_data = array();
			if (!(empty($rec_reward))) 
			{
				foreach ($rec_reward as $val ) 
				{
					if ($val['type'] == 1) 
					{
						$rec_data['credit'] = intval($val['num']);
					}
					else if ($val['type'] == 2) 
					{
						$rec_data['money']['num'] = intval($val['num']);
						$rec_data['money']['type'] = intval($val['moneytype']);
					}
					else if ($val['type'] == 3) 
					{
						$rec_data['bribery'] = intval($val['num']);
					}
					else if ($val['type'] == 4) 
					{
						$goods_id = intval($val['goods_id']);
						$goods_name = trim($val['goods_name']);
						$goods_price = floatval($val['goods_price']);
						$goods_total = intval($val['goods_total']);
						$goods_spec = intval($val['goods_spec']);
						$goods_specname = trim($val['goods_specname']);
						if (isset($rec_data['goods'][$goods_id]['spec'])) 
						{
							$oldspec = $rec_data['goods'][$goods_id]['spec'];
						}
						else 
						{
							$oldspec = array();
						}
						$rec_data['goods'][$goods_id] = array('id' => $goods_id, 'title' => $goods_name, 'marketprice' => $goods_price, 'total' => $goods_total, 'spec' => $oldspec);
						if (0 < $goods_spec) 
						{
							$rec_data['goods'][$goods_id]['spec'][$goods_spec] = array('goods_spec' => $goods_spec, 'goods_specname' => $goods_specname, 'marketprice' => $goods_price, 'total' => $goods_total);
						}
						else 
						{
							$rec_data['goods'][$goods_id]['spec'] = '';
						}
					}
					else if ($val['type'] == 5) 
					{
						$coupon_id = intval($val['coupon_id']);
						$coupon_name = trim($val['coupon_name']);
						$coupon_num = intval($val['coupon_num']);
						$rec_data['coupon'][$coupon_id] = array('id' => $coupon_id, 'couponname' => $coupon_name, 'couponnum' => $coupon_num);
						if (isset($rec_data['coupon']['total'])) 
						{
							$rec_data['coupon']['total'] += $coupon_num;
						}
						else 
						{
							$rec_data['coupon']['total'] = 0;
							$rec_data['coupon']['total'] += $coupon_num;
						}
					}
				}
			}
			$sub_reward = htmlspecialchars_decode($_GPC['sub_reward_data']);
			$sub_reward = json_decode($sub_reward, 1);
			$sub_data = array();
			if (!(empty($sub_reward))) 
			{
				foreach ($sub_reward as $val ) 
				{
					if ($val['type'] == 1) 
					{
						$sub_data['credit'] = intval($val['num']);
					}
					else if ($val['type'] == 2) 
					{
						$sub_data['money']['num'] = intval($val['num']);
						$sub_data['money']['type'] = intval($val['moneytype']);
					}
					else if ($val['type'] == 3) 
					{
						$sub_data['bribery'] = intval($val['num']);
					}
					else if ($val['type'] == 5) 
					{
						$coupon_id = intval($val['coupon_id']);
						$coupon_name = trim($val['coupon_name']);
						$coupon_num = intval($val['coupon_num']);
						$sub_data['coupon'][$coupon_id] = array('id' => $coupon_id, 'couponname' => $coupon_name, 'couponnum' => $coupon_num);
						if (isset($sub_data['coupon']['total'])) 
						{
							$sub_data['coupon']['total'] += $coupon_num;
						}
						else 
						{
							$sub_data['coupon']['total'] = 0;
							$sub_data['coupon']['total'] += $coupon_num;
						}
					}
				}
			}
			$reward['rec'] = $rec_data;
			$reward['sub'] = $sub_data;
			$data['reward_data'] = serialize($reward);
			
			
			if (!(empty($id))) 
			{
				pdo_update('yunphp_shop_receive', $data, array('id' => $id));
				plog('receive.edit', '修改活动赠送任务 ID: ' . $id);
			}
			else 
			{
				$data['createtime'] = time();
				pdo_insert('yunphp_shop_receive', $data);
				$id = pdo_insertid();
				plog('receive.add', '添加活动赠送任务 ID: ' . $id);
			}
			show_json(1, array('url' => webUrl('sale.receive')));
		}
		if (!(empty($item))) 
		{
			$reward = unserialize($item['reward_data']);
			$rec_reward = $reward['rec'];
			$sub_reward = $reward['sub'];
		}
		else 
		{
			$rec_reward = '';
			$sub_reward = '';
		}
		if (empty($item['starttime'])) 
		{
			$item['starttime'] = time();
			$item['endtime'] = strtotime(date('Y-m-d H:i', $item['starttime']) . '+30 days');
		}
		include $this->template();
	}
	public function delete() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		$item = pdo_fetch('SELECT * FROM ' . tablename('yunphp_shop_receive') . ' WHERE id  = :id  and uniacid=:uniacid ', array(':id' => $id, ':uniacid' => $_W['uniacid']));
		if (!(empty($item))) 
		{
			pdo_delete('yunphp_shop_receive', array('id' => $id, 'uniacid' => $_W['uniacid']));
		}
		show_json(1);
	}
}