<?php
error_reporting(0);
require '../../../../../framework/bootstrap.inc.php';
require '../../../../../addons/yunphp_shop/defines.php';
require '../../../../../addons/yunphp_shop/core/inc/functions.php';
global $_W;
global $_GPC;
ignore_user_abort();
set_time_limit(0);
$uniacids = m('cache')->get('couponwillcloseuniacid', 'global');
if (empty($uniacids)) 
{
	return;
}
foreach ($uniacids as $uniacid ) 
{
	if (empty($uniacid)) 
	{
		continue;
	}
	$_W['uniacid'] = $uniacid;
	$trade = m('common')->getSysset('trade', $_W['uniacid']);
	
	$minute = intval($trade['willclosecoupon']);
	if ($days <= 0) 
	{
		exit();
	}
	if ($minute == 0) 
	{
		$minute = 30;
	}
	$minute *= 60;
	
	
	
	
	$coupons = pdo_fetchall('select d.id,d.couponid,d.gettime,c.timelimit,c.coupontype,c.timedays,c.timestart,c.timeend,c.thumb,c.couponname,c.enough,c.backtype,c.deduct,c.discount,c.backmoney,c.backcredit,c.backredpack,c.bgcolor,c.thumb,c.merchid,c.tagtitle,c.settitlecolor,c.titlecolor,c.grouptype from ' . tablename('yunphp_shop_coupon_data') . ' d' ' left join ' . tablename('yunphp_shop_coupon') . ' c on d.couponid = c.id' ' where d.uniacid=' . $_W['uniacid'] . ' and d.used=0 and d.couponwillcancelmessage<>1 and ( (c.timelimit =0 and c.timedays<>0 and  c.timedays*86400 + d.gettime + ' . $minute . ' >unix_timestamp()) or (c.timelimit=1 and c.timeend + ' . $minute . ' >unix_timestamp() ))'' order by d.gettime desc  LIMIT ' );
	
	
	foreach ($coupons as $c ) 
	{
		$onew = pdo_fetch('select id,used  from ' . tablename('yunphp_shop_coupon_data') . ' where id=:id and used=0 limit 1', array(':id' => $c['id']));
		if (!(empty($onew)) && ($onew['status'] == 0)) 
		{
			m('notice')->sendCouponWillCancelMessage($onew['id']);
		}
	}
}
?>