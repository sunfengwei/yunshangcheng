<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
define('YUNPHP_SHOP_DEBUG', false);
!(defined('YUNPHP_SHOP_PATH')) && define('YUNPHP_SHOP_PATH', IA_ROOT . '/addons/yunphp_shop/');
!(defined('YUNPHP_SHOP_CORE')) && define('YUNPHP_SHOP_CORE', YUNPHP_SHOP_PATH . 'core/');
!(defined('YUNPHP_SHOP_DATA')) && define('YUNPHP_SHOP_DATA', YUNPHP_SHOP_PATH . 'data/');
!(defined('YUNPHP_SHOP_VENDOR')) && define('YUNPHP_SHOP_VENDOR', YUNPHP_SHOP_PATH . 'vendor/');
!(defined('YUNPHP_SHOP_CORE_WEB')) && define('YUNPHP_SHOP_CORE_WEB', YUNPHP_SHOP_CORE . 'web/');
!(defined('YUNPHP_SHOP_CORE_MOBILE')) && define('YUNPHP_SHOP_CORE_MOBILE', YUNPHP_SHOP_CORE . 'mobile/');
!(defined('YUNPHP_SHOP_CORE_SYSTEM')) && define('YUNPHP_SHOP_CORE_SYSTEM', YUNPHP_SHOP_CORE . 'system/');
!(defined('YUNPHP_SHOP_PLUGIN')) && define('YUNPHP_SHOP_PLUGIN', YUNPHP_SHOP_PATH . 'plugin/');
!(defined('YUNPHP_SHOP_PROCESSOR')) && define('YUNPHP_SHOP_PROCESSOR', YUNPHP_SHOP_CORE . 'processor/');
!(defined('YUNPHP_SHOP_INC')) && define('YUNPHP_SHOP_INC', YUNPHP_SHOP_CORE . 'inc/');
!(defined('YUNPHP_SHOP_URL')) && define('YUNPHP_SHOP_URL', $_W['siteroot'] . 'addons/yunphp_shop/');
!(defined('YUNPHP_SHOP_TASK_URL')) && define('YUNPHP_SHOP_TASK_URL', $_W['siteroot'] . 'addons/yunphp_shop/core/task/');
!(defined('YUNPHP_SHOP_LOCAL')) && define('YUNPHP_SHOP_LOCAL', '../addons/yunphp_shop/');
!(defined('YUNPHP_SHOP_STATIC')) && define('YUNPHP_SHOP_STATIC', YUNPHP_SHOP_URL . 'static/');
!(defined('YUNPHP_SHOP_PREFIX')) && define('YUNPHP_SHOP_PREFIX', 'yunphp_shop_');
define('YUNPHP_SHOP_PLACEHOLDER', '../addons/yunphp_shop/static/images/placeholder.png');
?>