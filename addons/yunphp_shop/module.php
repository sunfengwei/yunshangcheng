<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}


require_once IA_ROOT . '/addons/yunphp_shop/version.php';
require_once IA_ROOT . '/addons/yunphp_shop/defines.php';
require_once YUNPHP_SHOP_INC . 'functions.php';
class Yunphp_shopModule extends WeModule
{
	public function welcomeDisplay()
	{
		header('location: ' . webUrl());
		exit();
	}
}


?>