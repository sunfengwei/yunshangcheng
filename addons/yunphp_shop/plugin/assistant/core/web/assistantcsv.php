<?php
if (!defined('IN_IA')) 
{
	exit('Access Denied');
}
define('IA_ROOT', str_replace('\\', '/', dirname(dirname(__FILE__))));
require_once IA_ROOT . '/framework/library/phpexcel/PHPExcel/Reader/CSV.php';
class Assistantcsv_YunphpShopPage extends PluginWebPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		if ($_W['ispost']) 
		{
			$express = trim($_GPC['express']);
			$expresscom = trim($_GPC['expresscom']);
			$rows = m('excel')->import('excelfile');
			$num = count($rows);
			$time = time();
			$i = 0;
			$err_array = array();
			foreach ($rows as $rownum => $col ) 
			{
				$realname = trim($col[0]);
				$mobile = trim($col[1]);
				$address = trim($col[2]);
				$goodsname = trim($col[3]);
				$expresssn = trim($col[4]);
				$refund_flag = 0;
				if (empty($mobile)) 
				{
					continue;
				}
				if (empty($realname)) 
				{
					$err_array[] = $realname;
					continue;
				}
				/* $sql = 'select id,status,refundid from ' . tablename('yunphp_shop_order') . ' where ordersn=:ordersn and uniacid=:uniacid and isparent=0 and merchid=:merchid';
				$sql .= ' and status in (1,2) and `isverify`=0 and `isvirtual`=0 and `virtual`=0 and `addressid` >0 limit 1';
				$order = pdo_fetch($sql, array(':ordersn' => $ordersn, ':uniacid' => $_W['uniacid'], ':merchid' => 0)); */
				if (!(empty($realname))) 
				{
					$data = array();
					$data['status'] = 2;
					$data['express'] = $express;
					$data['expresscom'] = $expresscom;
					$data['expresssn'] = $expresssn;
					$data['realname'] = $realname;
					$data['mobile'] = $mobile;
					$data['address'] = $address;
					$data['goodsname'] = $goodsname;
					$data['sendtime'] = $time;
					$data['uniacid'] = $_W['uniacid'];
					pdo_insert('yunphp_shop_assistant', $data);
					plog('assistant.assistantcsv', '导入线下订单 ID: ' . $order['id'] . ' 姓名: ' . $realname . ' <br/>快递公司: ' . $expresscom . ' 快递单号: ' . $expresssn);
					++$i;
				}
				else 
				{
					$err_array[] = $realname;
				}
			}
			$tip = '';
			$msg = $i . '个订单导入成功！';
			if ($i < $num) 
			{
				$url = '';
				if (!(empty($err_array))) 
				{
					$j = 1;
					$tip .= '<br>' . count($err_array) . '个订单导入失败,失败的订单收件人姓名: <br>';
					foreach ($err_array as $k => $v ) 
					{
						$tip .= $v . ' ';
						if (($j % 2) == 0) 
						{
							$tip .= '<br>';
						}
						++$j;
					}
				}
			}
			else 
			{
				$url = webUrl('assistant/assistantcsv');
			}
			$this->message($msg . $tip, $url, '');
		}
		$express_list = m('express')->getExpressList();
		include $this->template();
	}
	public function import() 
	{
		$columns = array();
		$columns[] = array('title' => '收件人姓名', 'field' => '', 'width' => 16);
		$columns[] = array('title' => '收件人电话', 'field' => '', 'width' => 32);
		$columns[] = array('title' => '收件人地址', 'field' => '', 'width' => 64);
		$columns[] = array('title' => '商品名称', 'field' => '', 'width' => 64);
		$columns[] = array('title' => '快递单号', 'field' => '', 'width' => 32);
		m('excel')->temp('线下订单上传数据模板', $columns);
	}
}
?>