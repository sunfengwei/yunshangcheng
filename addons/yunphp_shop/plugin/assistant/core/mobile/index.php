<?php

if (!(defined('IN_IA'))) {
	exit('Access Denied');
}


require YUNPHP_SHOP_PLUGIN . 'creditshop/core/page_mobile.php';
class Index_YunphpShopPage extends CreditshopMobilePage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		$this->diyPage('creditshop');
		$openid = $_W['openid'];
		$uniacid = $_W['uniacid'];
		$shop = m('common')->getSysset('shop');
		$advs = pdo_fetchall('select id,advname,link,thumb from ' . tablename('yunphp_shop_assistant_adv') . ' where uniacid=:uniacid and enabled=1 order by displayorder desc', array(':uniacid' => $uniacid));
		$advs = set_medias($advs, 'thumb');
		

		include $this->template();
	}
	public function query()
	{
		global $_W;
		global $_GPC;
		
		$openid = $_W['openid'];
		$uniacid = $_W['uniacid'];
		$mobile = $_W['mobile'];
		$shop = m('common')->getSysset('shop');
		$list = pdo_fetchall('select * from ' . tablename('yunphp_shop_assistant') . ' where uniacid=:uniacid and mobile=:mobile order by sendtime desc', array(':uniacid' => $uniacid,':mobile' => $mobile));
		
		

		include $this->template();
	}
}


?>