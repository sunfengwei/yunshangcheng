<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
require_once IA_ROOT . '/addons/yunphp_shop/defines.php';
require_once YUNPHP_SHOP_INC . 'plugin_processor.php';
require_once YUNPHP_SHOP_INC . 'receiver.php';
class PosterProcessor extends PluginProcessor 
{
	public function __construct() 
	{
		parent::__construct('exchange');
	}
}
?>