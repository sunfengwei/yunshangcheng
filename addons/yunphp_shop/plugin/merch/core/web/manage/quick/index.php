<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
require YUNPHP_SHOP_PLUGIN . 'merch/core/inc/page_merch.php';
class Index_YunphpShopPage extends MerchWebPage 
{
	public function main() 
	{
		global $_W;
		include $this->template();
	}
}
?>