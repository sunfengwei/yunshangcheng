<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
//服务订单
class Order_YunphpShopPage extends PluginWebPage {
    //订单列表
    public function main() {
        global $_W;
        global $_GPC;
	$pindex = max(1, intval($_GPC['page']));
        $psize = 20;
        $condition = ' WHERE   g.`uniacid` = :uniacid  AND g.`status` > 0 and  g.`deleted`=0';
        $params = array();
        $params[':uniacid'] = $_W['uniacid'];
        $order = strtolower(trim($_GPC['order']));
        empty($order) && ($_GPC['order'] = $order = 'business');
        $condition .= $this->orderwhere($order);
        $starttime = strtotime($_GPC['time']['start']);
        $endtime = strtotime($_GPC['time']['end']);
        //时间
        if (!(empty($starttime))  ||  !(empty($endtime))){
            $condition .= ' and   addtime > '.$starttime.'  and  addtime <'.$endtime;
        }
        //关键词
        if (!(empty($_GPC['keyword']))) {
            $_GPC['keyword'] = trim($_GPC['keyword']);
            $condition .= '  and   g.`title`   LIKE :keyword  ';
            $params[':keyword'] = '%' . $_GPC['keyword'] . '%';
        }
        $condition .= '    order by  addtime  desc';
        $sqlcondition  =' left join ' . tablename('yunphp_shop_business_order') . ' a  on   a.goods_id = g.id   and  a.goods_type = g.type';
        $sql = 'SELECT  g.id,g.title,g.marketprice,g.type,a.id  as oid,a.user_id    FROM ' . tablename('yunphp_shop_goods') . 'g' .$sqlcondition . $condition;
        $total_all = pdo_fetchall($sql,$params);
        $total = count($total_all);
        unset($total_all);
        if (!(empty($total))) {
            $sql = 'SELECT  g.id,g.title,g.marketprice,g.type,a.id  as oid,a.user_id,g.displayorder,g.thumb,a.addtime,a.orderid    FROM ' . tablename('yunphp_shop_goods') . 'g' .$sqlcondition . $condition . '   LIMIT ' . (($pindex - 1) * $psize) . ',' . $psize;
            $list = pdo_fetchall($sql, $params);
            $pager = pagination($total, $pindex, $psize);
            if(!empty($list)){
                foreach ($list as  $key=>$val){
                    $member = m('member')->getMember($val['user_id']);
                    $list[$key]['member'] = $member['nickname'];
                    $list[$key]['addtime'] = date('Y-m-d',$val['addtime']) ;
                }
            }
        }
        
        include $this->template();
    }
    //订单操作
    public function edit() {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $condition = ' WHERE  	orderid = :orderid  order  by  id  desc';
        $params = array();
        $params[':orderid'] = $id;
        $list = pdo_fetchall('SELECT  *   FROM ' . tablename('yunphp_shop_business_order_handle') . $condition,$params );
        
        //订单信息
        $ordercondition = ' WHERE   g.`uniacid` = :uniacid  and  a.id = :id  and   g.`status` > 0 and  g.`deleted`=0';
        $orderparams = array();
        $orderparams[':uniacid'] = $_W['uniacid'];
        $orderparams[':id'] = $id;
        $order = strtolower(trim($_GPC['order']));
        empty($order) && ($_GPC['order'] = $order = 'business');
        //$ordercondition .= $this->orderwhere($order);
        
        $sqlcondition  =' left join ' . tablename('yunphp_shop_business_order') . ' a  on   a.goods_id = g.id   and  a.goods_type = g.type';
        $sql = 'SELECT  g.id,g.title,g.marketprice,g.type,a.id  as oid,a.user_id,g.displayorder,g.thumb,a.addtime,a.orderid    FROM ' . tablename('yunphp_shop_goods') . 'g' .$sqlcondition . $ordercondition;
        $info = pdo_fetch($sql,$orderparams);
        if(!empty($info)){
            $member = m('member')->getMember($info['user_id']);
            $info['member'] = $member['nickname'];
            $info['addtime'] = date('Y-m-d',$info['addtime']) ;
        }
        
        if(!empty($list)){
            foreach ($list as  $key=>$val){
                $list[$key]['operatingtime'] = date('Y-m-d',$val['operatingtime']) ;
                $list[$key]['addtime'] = date('Y-m-d',$val['addtime']) ;
            }
        }
        include $this->template();
    }
    
    //订单添加操作
    public function tianjia() {
        global $_W;
        global $_GPC;
        $orderid = intval($_GPC['id']);
        if(!empty($_GPC['name'])){
            $data = array();
            $data['orderid'] = $_GPC['orderid'];
            $data['name'] = $_GPC['name'];
            if($data['name'] == ''){
                show_json(0, '经办人不能为空！');
            }
            $data['operatingtime'] = strtotime($_GPC['operatingtime']);
            if($data['operatingtime'] == ''){
                show_json(0, '办理时间不能为空！');
            }
            $data['handle'] = $_GPC['handle'];
            if($data['handle'] == ''){
                show_json(0, '办理事项不能为空！');
            }
            $data['addtime'] = time();
            pdo_insert('yunphp_shop_business_order_handle', $data);
            $ids = pdo_insertid();
            if($ids > 0){
                show_json(1, array('url' => webUrl('business/goods/order/edit', array('id' => $orderid))));
            }  else {
                show_json(0, '添加失败！');
            }
        }
        include   $this->template('business/goods/order/tianjia');
    }
    
    public function handleedit() {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $orderid = intval($_GPC['orderid']);
        if($id == ''){
            show_json(0, 'id不能为空！');
        }
        $orderid = intval($_GPC['orderid']);
        if($_GPC['status'] == 'type'){
            $data = array();
            $data['orderid'] = $_GPC['orderid'];
            if($data['orderid'] == ''){
                show_json(0, 'id1不能为空！');
            }
            $data['name'] = $_GPC['name'];
            if($data['name'] == ''){
                show_json(0, '经办人不能为空！');
            }
            $data['operatingtime'] = strtotime($_GPC['operatingtime']);
            if($data['operatingtime'] == ''){
                show_json(0, '办理时间不能为空！');
            }
            $data['handle'] = $_GPC['handle'];
            if($data['handle'] == ''){
                show_json(0, '办理事项不能为空！');
            }
            pdo_update('yunphp_shop_business_order_handle', $data, array('id' => $id));
            show_json(1, array('url' => webUrl('business/goods/order/edit', array('id' => $orderid))));
        }
        $condition = ' WHERE  	id = :id ';
        $params = array();
        $params[':id'] = $id;
        $item = pdo_fetch('SELECT  *   FROM ' . tablename('yunphp_shop_business_order_handle') . $condition,$params );
        if(!empty($item)){
            $item['operatingtime'] = date('Y-m-d',$item['operatingtime']) ;
        }
        include   $this->template('business/goods/order/handleedit');
    }
    
    public function  orderwhere($order) {
        if ($order == 'business') {
            $condition = '  and  g.`type`=11';
        }
        //代理记账
        if ($order == 'act') {
            $condition = '  and  g.`type`=12';
        }
        //视觉设计
        if ($order == 'visual') {
            $condition = '   and  g.`type`=13';
        }
        //知识产权
        if ($order == 'intellectual') {
            $condition = '  and  g.`type`=14';
        }
        // 软件开发
        if ($order == 'software') {
            $condition = '  and  g.`type`=15';
        }
        // 营销推广
        if ($order == 'marketing') {
            $condition = '  and  g.`type`=16';
        }
        return  $condition;
    }
    
    
}