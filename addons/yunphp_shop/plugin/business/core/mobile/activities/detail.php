<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
require YUNPHP_SHOP_PLUGIN . 'business/core/page_amobile.php';
class Detail_YunphpShopPage extends BusinessAmobilePage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$openid = $_W['openid'];
		$uniacid = $_W['uniacid'];
		$id = intval($_GPC['id']);
		$activities = pdo_fetch('select * from ' . tablename('yunphp_shop_business_activities') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
		$thumbs = iunserializer($activities['thumb_url']);
		if (empty($thumbs)) 
		{
			$thumbs = array($activities['thumb']);
		}
		if (!(empty($activities['thumb_first'])) && !(empty($activities['thumb']))) 
		{
			$thumbs = array_merge(array($activities['thumb']), $thumbs);
		}
		$share = m('common')->getSysset('share');
		$share['goods_detail_text'] = nl2br($share['goods_detail_text']);
		include $this->template();
	}
}