<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
class Heartbeat_YunphpShopPage extends PluginWebPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$condition = ' and uniacid=:uniacid';
		$params = array(':uniacid' => $_W['uniacid']);
		if (empty($starttime) || empty($endtime)) {
			$starttime = strtotime('-1 month');
			$endtime = time();
		}


		if (!empty($_GPC['datetime']['start']) && !empty($_GPC['datetime']['end'])) {
			$starttime = strtotime($_GPC['datetime']['start']);
			$endtime = strtotime($_GPC['datetime']['end']);
			$condition .= ' AND posttime >= :starttime AND posttime <= :endtime ';
			$params[':starttime'] = $starttime;
			$params[':endtime'] = $endtime;
		}
		if ($_GPC['status'] != '') 
		{
			$condition .= ' and status=' . intval($_GPC['status']);
		}
		if (!(empty($_GPC['keyword']))) 
		{
			$_GPC['keyword'] = trim($_GPC['keyword']);
			$condition .= ' and jqid  like :keyword';
			$params[':keyword'] = '%' . $_GPC['keyword'] . '%';
		}
		$list = pdo_fetchall('SELECT * FROM ' . tablename('yunphp_iot_heartbeat') . ' WHERE 1 ' . $condition . '  ORDER BY id DESC limit ' . (($pindex - 1) * $psize) . ',' . $psize, $params);
		$total = pdo_fetchcolumn('SELECT count(*) FROM ' . tablename('yunphp_iot_heartbeat') . ' WHERE 1 ' . $condition, $params);
		$pager = pagination($total, $pindex, $psize);
		include $this->template();
	}
	
	public function delete() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		if (empty($id)) 
		{
			$id = ((is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0));
		}
		$items = pdo_fetchall('SELECT id,jqid FROM ' . tablename('yunphp_iot_heartbeat') . ' WHERE id in( ' . $id . ' ) AND uniacid=' . $_W['uniacid']);
		foreach ($items as $item ) 
		{
			pdo_delete('yunphp_iot_heartbeat', array('id' => $item['id']));
			plog('tyiot.heartbeat.delete', '删除终端通讯日志 ID: ' . $item['id'] . ' 终端ID: ' . $item['jqid'] . ' ');
		}
		show_json(1, array('url' => referer()));
	}
	public function deleteall() 
	{
		global $_W;
		global $_GPC;
		pdo_delete('yunphp_iot_heartbeat');
		plog('tyiot.heartbeat.delete', '删除终端全部通讯日志' . ' ');
		show_json(1, array('url' => referer()));
	}
	public function deleteweek() 
	{
		global $_W;
		global $_GPC;
		$weektime = strtotime('-1 week');
		pdo_delete('yunphp_iot_heartbeat',array('posttime <' => $weektime));
		plog('tyiot.heartbeat.delete', '删除终端一周前通讯日志' . ' ');
		show_json(1, array('url' => referer()));
	}
	
}
?>