<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
class Terminal_YunphpShopPage extends PluginWebPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$groups = $this->model->getGroups();
		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$params = array(':uniacid' => $_W['uniacid']);
		$condition = '';
		$keyword = trim($_GPC['keyword']);
		if (!(empty($keyword))) 
		{
			$condition .= ' and ( u.merchname like :keyword or u.realname like :keyword or u.mobile like :keyword)';
			$params[':keyword'] = '%' . $keyword . '%';
		}
		if ($_GPC['groupid'] != '') 
		{
			$condition .= ' and u.groupid=' . intval($_GPC['groupid']);
		}
		if ($_GPC['status'] != '') 
		{
			$condition .= ' and u.status=' . intval($_GPC['status']);
		}
		if ($_GPC['status'] == '0') 
		{
			$sortfield = 'u.applytime';
		}
		else 
		{
			$sortfield = 'u.jointime';
		}
		$sql = 'select  u.*,g.catename  from ' . tablename('yunphp_iot_terminal') . '  u ' . ' left join  ' . tablename('yunphp_iot_tertype') . ' g on u.cateid = g.id ' . ' where u.uniacid=:uniacid ' . $condition . ' ORDER BY ' . $sortfield . ' desc';
		if (empty($_GPC['export'])) 
		{
			$sql .= ' limit ' . (($pindex - 1) * $psize) . ',' . $psize;
		}
		$list = pdo_fetchall($sql, $params);
		$total = pdo_fetchcolumn('select count(*) from' . tablename('yunphp_iot_terminal') . ' u  ' . ' left join  ' . tablename('yunphp_shop_merch_group') . ' g on u.cateid = g.id ' . ' where u.uniacid = :uniacid ' . $condition, $params);
		if (!empty($total)){
			foreach ($list as $key => &$value ) 
			{
				$url = $value['short_url'];
				$value['qrcode'] = m('qrcode')->createQrcode($url);
			}
		}
		if ($_GPC['export'] == '1') 
		{
			ca('tyiot.terminal.export');
			plog('tyiot.terminal.export', '导出商户数据');
			foreach ($list as &$row ) 
			{
				$row['applytime'] = ((empty($row['applytime']) ? '-' : date('Y-m-d H:i', $row['applytime'])));
				$row['checktime'] = ((empty($row['checktime']) ? '-' : date('Y-m-d H:i', $row['checktime'])));
				$row['groupname'] = ((empty($row['groupid']) ? '无分组' : $row['groupname']));
				$row['statusstr'] = ((empty($row['status']) ? '待审核' : (($row['status'] == 1 ? '通过' : '未通过'))));
				$row['accounttime'] = date('Y-m-d H:i', $row['accounttime']);
			}
			unset($row);
			m('excel')->export($list, array( 'title' => '商户数据-' . date('Y-m-d-H-i', time()), 'columns' => array( array('title' => 'ID', 'field' => 'id', 'width' => 12), array('title' => '商户名', 'field' => 'merchname', 'width' => 24), array('title' => '主营项目', 'field' => 'salecate', 'width' => 12), array('title' => '联系人', 'field' => 'realname', 'width' => 12), array('title' => '手机号', 'field' => 'moible', 'width' => 12), array('title' => '子帐号数', 'field' => 'accounttotal', 'width' => 12), array('title' => '到期时间', 'field' => 'accounttime', 'width' => 12), array('title' => '申请时间', 'field' => 'applytime', 'width' => 12), array('title' => '审核时间', 'field' => 'checktime', 'width' => 12), array('title' => '状态', 'field' => 'createtime', 'width' => 12) ) ));
		}
		$pager = pagination($total, $pindex, $psize);
		load()->func('tpl');
		include $this->template();
	}
	public function add() 
	{
		$this->post();
	}
	public function edit() 
	{
		$this->post();
	}
	protected function post() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		/* if (empty($id)) 
		{
			$max_flag = $this->model->checkMaxAgentTer(1);
			if ($max_flag == 1) 
			{
				$this->message('已经达到最大终端数量,不能再添加商户，如需要继续添加请先修改代理商终端数量。', webUrl('merch/user'), 'error');
			}
		} */
		$item = pdo_fetch('select * from ' . tablename('yunphp_iot_terminal') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
		if (!(empty($item['openid']))) 
		{
			$member = m('member')->getMember($item['openid']);
		}
		if (!(empty($item['agentuid']))) 
		{
			$user = m('member')->getMember($item['agentuid']);
		}
		if (empty($item) || empty($item['accounttime'])) 
		{
			$accounttime = strtotime('+365 day');
		}
		else 
		{
			$accounttime = $item['accounttime'];
		}
		
		if ($_W['ispost']) 
		{
			$fdata = array();
			$status = intval($_GPC['status']);
			$terminalid = trim($_GPC['terminalid']);
			if (empty($_GPC['cateid'])) 
			{
				show_json(0, '请选择终端分类!');
			}
			if (!is_numeric($terminalid)) 
			{
				show_json(0, '终端编号只能是数字，请正确填写!');
			}
			//如果终端ID发生变化，则重新生成二维码短网址
			if ($terminalid != $item['terminalid']) {
				$url = mobileUrl('tyiot/goods', array('jqid' => $terminalid, 'cate' => 22), true);
				//调用微信短网址接口
				load()->func('communication');
				$longurl = trim($url);
				$token = WeAccount::token(WeAccount::TYPE_WEIXIN);
				$url = "https://api.weixin.qq.com/cgi-bin/shorturl?access_token={$token}";
				$send = array();
				$send['action'] = 'long2short';
				$send['long_url'] = $longurl;
				$response = ihttp_request($url, json_encode($send));
				if(is_error($response)) {
					$result = error(-1, "访问公众平台接口失败, 错误: {$response['message']}");
				}
				$result = @json_decode($response['content'], true);
				if(empty($result)) {
					$result =  error(-1, "接口调用失败, 元数据: {$response['meta']}");
				} elseif(!empty($result['errcode'])) {
				$result = error(-1, "访问微信接口错误, 错误代码: {$result['errcode']}, 错误信息: {$result['errmsg']}");
				}
				if(is_error($result)) {
					exit(json_encode(array('errcode' => -1, 'errmsg' => $result['message'])));
				}
				$short_url = $result['short_url'];
			} else {
				$short_url = $item['short_url'];
			}
			
			$terwhere = ' terminalid=:terminalid';
			$params = array(':terminalid' => $terminalid);
			if (!empty($id)) 
				{
					$terwhere .= ' and id<>:id';
					$params[':id'] = $id;
				}
			$usercount = pdo_fetchcolumn('select count(*) from ' . tablename('yunphp_iot_terminal') . ' where ' . $terwhere . ' limit 1', $params);
			if (0 < $usercount) 
			{
				show_json(0, '终端ID： ' . $terminalid . ' 已经存在!');
			}
			
			$data = array('uniacid' => $_W['uniacid'], 'terminalid' => $terminalid, 'salecate' => trim($_GPC['salecate']), 'realname' => trim($_GPC['realname']), 'mobile' => trim($_GPC['mobile']), 'address' => trim($_GPC['address']), 'tel' => trim($_GPC['tel']), 'lng' => $_GPC['map']['lng'], 'lat' => $_GPC['map']['lat'], 'accounttime' => strtotime($_GPC['accounttime']), 'accounttotal' => intval($_GPC['accounttotal']),  'cateid' => intval($_GPC['cateid']), 'isrecommand' => intval($_GPC['isrecommand']), 'remark' => trim($_GPC['remark']), 'status' => $status, 'desc' => trim($_GPC['desc1']), 'agentuid' => trim($_GPC['agentuid']), 'payrate' => trim($_GPC['payrate']), 'short_url' => $short_url);
			if (empty($item['jointime']) && ($status == 1)) 
			{
				$data['jointime'] = time();
			}
			if (empty($item)) {
				$item['applytime'] = time();
				pdo_insert('yunphp_iot_terminal', $data);
				$id = pdo_insertid();
				plog('tyiot.terminal.add', '添加终端 ID: ' . $id . ' 终端编号: ' . $data['terminalid'] . '<br/>到期时间: ' . date('Y-m-d', $data['accounttime']));
			} else {
				
				pdo_update('yunphp_iot_terminal', $data, array('id' => $id));
				plog('tyiot.terminal.edit', '编辑终端 ID: ' . $id . ' 终端编号: ' . $item['terminalid'] . ' -> ' . $data['terminalid'] . '<br/>到期时间: ' . date('Y-m-d', $item['accounttime']) . ' -> ' . date('Y-m-d', $data['accounttime']));
			}
			show_json(1, array('url' => webUrl('tyiot/terminal', array('status' => $item['status']))));
		}
		
		$category = $this->model->getterCategory();
		include $this->template();
	}
	public function status() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		if (empty($id)) 
		{
			$id = ((is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0));
		}
		$items = pdo_fetchall('SELECT id,merchname FROM ' . tablename('yunphp_shop_merch_user') . ' WHERE id in( ' . $id . ' ) AND uniacid=' . $_W['uniacid']);
		foreach ($items as $item ) 
		{
			pdo_update('yunphp_shop_merch_user', array('status' => intval($_GPC['status'])), array('id' => $item['id']));
			plog('merch.group.edit', (('修改商户分组账户状态<br/>ID: ' . $item['id'] . '<br/>商户名称: ' . $item['merchname'] . '<br/>状态: ' . $_GPC['status']) == 1 ? '启用' : '禁用'));
		}
		show_json(1);
	}
	public function delete() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		if (empty($id)) 
		{
			$id = ((is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0));
		}
		$uniacid = $_W['uniacid'];
		$change_data = array();
		$change_data['merchid'] = 0;
		$change_data['status'] = 0;
		$items = pdo_fetchall('SELECT * FROM ' . tablename('yunphp_shop_merch_user') . ' WHERE id in( ' . $id . ' ) AND uniacid=' . $_W['uniacid']);
		foreach ($items as $item ) 
		{
			pdo_update('yunphp_shop_goods', $change_data, array('merchid' => $item['id'], 'uniacid' => $uniacid));
			pdo_delete('yunphp_shop_merch_account', array('merchid' => $item['id'], 'uniacid' => $uniacid));
			pdo_delete('yunphp_shop_merch_user', array('id' => $item['id'], 'uniacid' => $uniacid));
			plog('tyiot.terminal.delete', '删除`商户 <br/>商户:  ID: ' . $item['id'] . ' / 名称:   ' . $item['merchname']);
		}
		show_json(1);
	}
	public function query() 
	{
		global $_W;
		global $_GPC;
		$kwd = trim($_GPC['keyword']);
		$params = array();
		$params[':uniacid'] = $_W['uniacid'];
		$condition = 'uniacid=:uniacid AND status=1';
		if (!(empty($kwd))) 
		{
			$condition .= ' AND `merchname` LIKE :keyword';
			$params[':keyword'] = '%' . $kwd . '%';
		}
		$ds = pdo_fetchall('SELECT id,merchname FROM ' . tablename('yunphp_shop_merch_user') . ' WHERE ' . $condition . ' order by id asc', $params);
		include $this->template();
		exit();
	}
}
?>