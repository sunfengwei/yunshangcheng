<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}

class TyiotMobilePage extends PluginMobilePage
{
	public function __construct()
	{
		parent::__construct();
		global $_W;
		global $_GPC;
		if (($_W['action'] != 'register') && ($_W['action'] != 'myshop') && ($_W['action'] != 'share')) {
			$member = m('member')->getMember($_W['openid']);
			if (($member['isagent'] != 1) || ($member['status'] != 1)) {
				header('location:' . mobileUrl('tyiot/register'));
				exit();
			}
		}
	}

	public function footerMenus()
	{
		global $_W;
		global $_GPC;
		include $this->template('tyiot/_menu');
	}
}

?>
