<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Index_YunphpShopPage extends MobilePage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		$allcategory = m('shop')->getCategory();
		$catlevel = intval($_W['shopset']['category']['level']);
		$opencategory = true;
		$plugin_tyiot = p('tyiot');
		$jqid = $_GPC['jqid'];
		if ($plugin_tyiot && (0 < intval($_W['shopset']['tyiot']['level']))) {
			$mid = intval($_GPC['mid']);

			if (!empty($mid)) {
				$shop = p('tyiot')->getShop($mid);

				if (empty($shop['selectcategory'])) {
					$opencategory = false;
				}
			}
		}
$goods['list']['jqid'] = $jqid;
		include $this->template();
	}

	public function get_list()
	{
		global $_GPC;
		global $_W;
		$jqid = $_GPC['jqid'];
		$args = array('pagesize' => 10, 'page' => intval($_GPC['page']), 'isnew' => trim($_GPC['isnew']), 'ishot' => trim($_GPC['ishot']), 'isrecommand' => trim($_GPC['isrecommand']), 'isdiscount' => trim($_GPC['isdiscount']), 'istime' => trim($_GPC['istime']), 'issendfree' => trim($_GPC['issendfree']), 'keywords' => trim($_GPC['keywords']), 'cate' => trim($_GPC['cate']), 'order' => trim($_GPC['order']), 'by' => trim($_GPC['by']), 'isiot' => 1);

		if (isset($_GPC['notyiot'])) {
			$args['notyiot'] = intval($_GPC['notyiot']);
		}

		$plugin_tyiot = p('tyiot');
		if ($plugin_tyiot && (0 < intval($_W['shopset']['tyiot']['level']))) {
			$mid = intval($_GPC['mid']);

			if (!empty($mid)) {
				$shop = p('tyiot')->getShop($mid);

				if (!empty($shop['selectgoods'])) {
					$args['ids'] = $shop['goodsids'];
				}
			}
		}

		$goods = m('goods')->getList($args);
		$goods['list']['jqid'] = $jqid;
		show_json(1, array('list' => $goods['list'], 'total' => $goods['total'], 'pagesize' => $args['pagesize'], 'jqid' =>$jqid));
	}

	public function query()
	{
		global $_GPC;
		global $_W;
		$jqid = $_GPC['jqid'];
		$args = array('pagesize' => 10, 'page' => intval($_GPC['page']), 'isnew' => trim($_GPC['isnew']), 'ishot' => trim($_GPC['ishot']), 'isrecommand' => trim($_GPC['isrecommand']), 'isdiscount' => trim($_GPC['isdiscount']), 'istime' => trim($_GPC['istime']), 'keywords' => trim($_GPC['keywords']), 'cate' => trim($_GPC['cate']), 'order' => trim($_GPC['order']), 'by' => trim($_GPC['by']), 'isiot' => 1);

		if (isset($_GPC['notyiot'])) {
			$args['notyiot'] = intval($_GPC['notyiot']);
		}

		$goods = m('goods')->getList($args);
		$goods['list']['jqid'] = $jqid;
		show_json(1, array('list' => $goods['list'], 'total' => $goods['total'], 'pagesize' => $args['pagesize'], 'jqid' =>$jqid));
	}
}

?>
