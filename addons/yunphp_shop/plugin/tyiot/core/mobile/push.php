<?php
if (!defined('IN_IA')) {
	exit('Access Denied');
}
require_once YUNPHP_SHOP_PLUGIN . 'tyiot/GatewayClient/Gateway.php';
use GatewayClient\Gateway;
Gateway::$registerAddress = '127.0.0.1:1238';
class Push_YunphpShopPage extends PluginMobilePage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		$uniacid = $_W['uniacid'];
		$jqid = $_GPC['jqid'];
		$message = $_GPC['message'];
		$client_id = $_GPC['client_id'];
		$biaoshi = substr($message,-8);
		
		$terminal = pdo_fetch('select * from ' . tablename('yunphp_iot_terminal') . ' where terminalid=:terminalid and uniacid=:uniacid limit 1', array(':terminalid' => $jqid, ':uniacid' => $_W['uniacid']));
		$short_url = $terminal['short_url'];
		$qrcode = m('qrcode')->createQrcode($short_url);
		
		include $this->template();
	}
}