<?php
if (!defined('IN_IA')) {
	exit('Access Denied');
}
require_once YUNPHP_SHOP_PLUGIN . 'tyiot/GatewayClient/Gateway.php';
use GatewayClient\Gateway;
Gateway::$registerAddress = '127.0.0.1:1238';
class Post_YunphpShopPage extends PluginMobilePage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		$uniacid = $_W['uniacid'];
		$message = $_GPC['message'];
		$client_id = $_GPC['client_id'];
		// pdo_insert('yunphp_iot_message', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>$status));
		if (!empty($message)) {
		$iswebpush = hex2bin($message);
		$iswebpushbiaoshi = substr($iswebpush,0,3);
		pdo_insert('yunphp_iot_message', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>$status, 'message_10' =>$iswebpush,'iswebpushbiaoshi'=>$iswebpushbiaoshi));
		if ($iswebpushbiaoshi == 'GET' || $iswebpushbiaoshi == '{"t)' ){
			
		} else {
		$biaoshi = substr($message,-8);
		if ($biaoshi == '0EA00BEA') {
			//将信息类型判定为心跳信息
			$status = 1;
			$jqid = substr($message,0,-8);
			//记录到心跳信息日志
			pdo_insert('yunphp_iot_heartbeat', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>$status));
			$heartbeatid = pdo_insertid();
			//向终端通讯模块回复心跳信息，以保证终端通讯模块链接。
			Gateway::sendToUid($jqid, 'ok');
			if (!empty($jqid)){
				//检查终端绑定表中是否存在此client_id绑定的机器ID
				$jqidver = pdo_fetch("SELECT * FROM ".tablename('yunphp_iot_registerok')." WHERE client_id LIKE :client_id order by id desc limit 1", array(':client_id' => $client_id));
				if (empty($jqidver)) {
					//如果注册绑定中没有当前client_id对应的机器ID，则将终端断开，重新链接
					Gateway::closeClient($client_id);
					//在机器故障日志中插入此错误“注册信息日志中没有找到当前链接对应的机器ID。如果此错误只出现一次，可能是注册信息日志被删除或者终端注册时网络故障，已经将终端重新连接，无需人工处理。如果此信息重复出现，则是由于终端通讯模块的注册码填写错误，请严格按照终端管理中提供的注册码修改终端通讯模块的注册码。产生此错误的心跳日志ID：”
					pdo_insert('yunphp_iot_fault', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>0, 'faulttype' => 0, 'faultdesc' => '注册信息日志中没有找到当前链接对应的机器ID。如果此错误只出现一次，可能是注册信息日志被删除或者终端注册时网络故障，已经将终端重新连接，无需人工处理。如果此信息重复出现，则是由于终端通讯模块的注册码填写错误，请严格按照终端管理中提供的注册码修改终端通讯模块的注册码。产生此错误的心跳日志ID：' . $heartbeatid . '。'));
				} elseif ($jqid != intval($jqidver['jqid'])){
					//如果注册信息日志中当前链接的绑定机器ID与心跳传输的机器ID不一致，则将终端断开，重新链接
					Gateway::closeClient($client_id);
					//将终端机器ID与心跳传输的id不一致的情况记录到机器故障表，“当前机器传输的心跳ID与注册日志中绑定的机器ID不一致，系统已经将终端重新链接，如果此信息重复出现，请检查终端通讯模块的注册信息码与心跳信息码是否严格按照终端管理中提供的信息码填写！如果仅出现一次则可能是服务器重启后，终端通讯模块提交的新注册信息服务器未正确接收，无需人工处理。产生此错误的心跳日志ID：”
					pdo_insert('yunphp_iot_fault', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>0, 'faulttype' => 0, 'faultdesc' => '当前机器传输的心跳ID与注册日志中绑定的机器ID不一致，系统已经将终端重新链接，如果此信息重复出现，请检查终端通讯模块的注册信息码与心跳信息码是否严格按照终端管理中提供的信息码填写！如果仅出现一次则可能是服务器重启后，终端通讯模块提交的新注册信息服务器未正确接收，无需人工处理。产生此错误的心跳日志ID：' . $heartbeatid . '。'));
				}
			}
		} elseif ($biaoshi == '0E0000E0') {
			//将信息类型判定为注册信息
			$status = 2;
			$jqid = substr($message,0,-8);
			//Gateway::sendToAll('regok1');
			//记录机器注册日志
			pdo_insert('yunphp_iot_register', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>$status));
			$registerid = pdo_insertid();
			//检查注册绑定表是否有此client_id
			$jqidregver = pdo_fetch("SELECT * FROM ".tablename('yunphp_iot_registerok')." WHERE client_id LIKE :client_id order by id desc limit 1", array(':client_id' => $client_id));
			if (empty($jqidregver)){
				//将当前$client_id与机器ID绑定，此绑定记录到内存。
				Gateway::bindUid($client_id, $jqid);
				
				//删除原绑定记录
				pdo_delete('yunphp_iot_registerok',array('jqid' => $jqid));
				//将信息记录到当前$client_id与机器ID绑定表，供后续使用
				pdo_insert('yunphp_iot_registerok', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>$status));
				//Gateway::sendToUid($jqid, 'regok');
			} elseif ($jqidregver['jqid'] == $jqid) {
				//如果注册绑定表中存在此client_id，则断开链接
				Gateway::closeClient($client_id);
				//向终端故障表写入错误信息，“当前链接client_id已经在注册绑定表存在，系统已经自动断开链接，终端将重新链接，这个问题可能是由于服务端重启造成的，此问题无需人工介入，系统会自行处理。”
				pdo_insert('yunphp_iot_fault', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>0, 'faulttype' => 0, 'faultdesc' => '当前链接client_id已经在注册绑定表存在，系统已经自动断开链接，终端将重新链接，这个问题可能是由于服务端重启造成的，此问题无需人工介入，系统会自行处理。产生此错误的注册日志ID：' . $registerid . '。'));
			}
		} else {
			//将信息类型判定为终端传输过来的上报信息
			$status = 3;
			//如果是通讯信息，则到register表取出对应的jqid
			$jqidcheck = pdo_fetch("SELECT * FROM ".tablename('yunphp_iot_registerok')." WHERE client_id LIKE :client_id order by id desc limit 1", array(':client_id' => $client_id));
			if (!empty($jqidcheck['jqid'])){
				$jqid = intval($jqidcheck['jqid']);
				pdo_insert('yunphp_iot_com', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>$status));
			} else {
				pdo_insert('yunphp_iot_com', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>$status));
				$comid = pdo_insertid();
				//如果注册绑定表中查询不到机器ID，则断开链接重新链接
				Gateway::closeClient($client_id);
				//向终端故障表写入错误信息：“服务端收到一条来自终端的指令，但是由于在注册绑定表中没有查询到终端ID，所以无法判断此信息的来源，服务端已经自动断开与此终端的链接，将重新链接。如果此故障重复出现，则是由于终端通讯模块中的注册码填写错误造成的，请严格按照终端列表中提供的注册码填写。”
				pdo_insert('yunphp_iot_fault', array('message'=>$message,'jqid'=>$jqid,'client_id'=>$client_id,'uniacid'=>$_W['uniacid'],'posttime'=>time(),'status'=>0, 'faulttype' => 0, 'faultdesc' => '服务端收到一条来自终端的指令，但是由于在注册绑定表中没有查询到终端ID，所以无法判断此信息的来源，服务端已经自动断开与此终端的链接，将重新链接。如果此故障重复出现，则是由于终端通讯模块中的注册码填写错误造成的，请严格按照终端列表中提供的注册码填写。产生此错误的注册日志ID：' . $comid . '。'));
			}
		}
		}
		}
	}
}