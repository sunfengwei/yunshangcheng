<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
class Taskuser_YunphpShopPage extends PluginWebPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$pindex = max(1, intval($_GPC['page']));
		$psize = 10;
		$params = array(':uniacid' => $_W['uniacid']);
		$condition = ' and j.uniacid=:uniacid and j.bonustask_type=' . $_GPC['type'] . ' and j.bonustask_id=' . intval($_GPC['id']);
		$searchfield = strtolower(trim($_GPC['searchfield']));
		$keyword = trim($_GPC['keyword']);
		if (!(empty($searchfield)) && !(empty($keyword))) 
		{
			if ($searchfield == 'rec') 
			{
				$condition .= ' AND ( m.nickname LIKE :keyword or m.realname LIKE :keyword or m.mobile LIKE :keyword ) ';
			}
			else if ($searchfield == 'sub') 
			{
				$condition .= ' AND ( m1.nickname LIKE :keyword or m1.realname LIKE :keyword or m1.mobile LIKE :keyword ) ';
			}
			$params[':keyword'] = '%' . $keyword . '%';
		}
		if (!(empty($_GPC['time']['start'])) && !(empty($_GPC['time']['end']))) 
		{
			$starttime = strtotime($_GPC['time']['start']);
			$endtime = strtotime($_GPC['time']['end']);
			$condition .= ' AND j.addtime >= :starttime AND j.addtime <= :endtime ';
			$params[':starttime'] = $starttime;
			$params[':endtime'] = $endtime;
		}
		$list = pdo_fetchall('SELECT j.*,m.avatar,m.nickname,m.realname,m.mobile FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' j ' . ' left join ' . tablename('yunphp_shop_member') . ' m on m.openid = j.join_user and m.uniacid = j.uniacid WHERE 1 ' . $condition . ' ORDER BY j.addtime desc ' . '  LIMIT ' . (($pindex - 1) * $psize) . ',' . $psize, $params);
		$total = pdo_fetchcolumn('SELECT count(*)  FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' j ' . ' left join ' . tablename('yunphp_shop_member') . ' m on m.openid = j.join_user and m.uniacid = j.uniacid where 1 ' . $condition . '  ', $params);
		$pager = pagination($total, $pindex, $psize);
		load()->func('tpl');
		include $this->template();
	}
}
?>