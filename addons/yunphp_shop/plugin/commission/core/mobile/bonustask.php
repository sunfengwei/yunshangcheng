<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
require YUNPHP_SHOP_PLUGIN . 'commission/core/page_login_mobile.php';
class Bonustask_YunphpShopPage extends CommissionMobileLoginPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$tabpage = $_GPC['tabpage'];
		$openid = trim($_W['openid']);

		$member = m('member')->getMember($openid);
		$now_time = time();
		$bonustask_sql = 'SELECT * FROM ' . tablename('yunphp_shop_commission_bonustask') . ' WHERE timestart<=' . $now_time . ' AND timeend>' . $now_time . ' AND uniacid=' . $_W['uniacid'] . ' AND `status`=1 AND `is_delete`=0 ORDER BY `createtime` DESC LIMIT 0,15';
		$bonustask_list = pdo_fetchall($bonustask_sql);
		foreach ($bonustask_list as $key => $val ) 
		{
			$val['reward_data'] = unserialize($val['reward_data']);
			$recward = $val['reward_data']['rec'];
			foreach ($recward as $k => $v ) 
			{
				if (isset($v['credit']) && (0 < $v['credit'])) 
				{
					$bonustask_list[$key]['is_credit'] = 1;
				}
				if (isset($v['money']['num']) && (0 < $v['money']['num'])) 
				{
					$bonustask_list[$key]['is_money'] = 1;
				}
				if (isset($v['bribery']) && (0 < $v['bribery'])) 
				{
					$bonustask_list[$key]['is_bribery'] = 1;
				}
				if (isset($v['goods']) && count($v['goods'])) 
				{
					$bonustask_list[$key]['is_goods'] = 1;
				}
				if (isset($v['coupon']['total']) && (0 < $v['coupon']['total'])) 
				{
					$bonustask_list[$key]['is_coupon'] = 1;
				}
				if (isset($v['shiwu']) && count($v['shiwu'])) 
				{
					$bonustask_list[$key]['is_shiwu'] = 1;
				}
				$bonustaskown = pdo_fetch('SELECT * FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' WHERE failtime>' . $now_time . ' AND is_reward= 0 AND uniacid=' . $_W['uniacid'] . ' AND bonustask_id= ' . $val['id'] . ' ');
				if (!empty($bonustaskown)) {
					$bonustask_list[$key]['bonustaskown'] = 1;
				} else {
					$bonustask_list[$key]['bonustaskown'] = 0;
				}
			}
		}
		$running_sql = 'SELECT `join`.*,`bonustask`.title,`bonustask`.bedown,`bonustask`.beown,`bonustask`.reward_data AS `poster_reward`,`bonustask`.titleicon,`bonustask`.poster_type FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' AS `join` LEFT JOIN ' . tablename('yunphp_shop_commission_bonustask') . ' AS `bonustask` ON `join`.bonustask_id=`bonustask`.`id` WHERE `join`.`failtime`>' . $now_time . ' AND `join`.`join_user`="' . $openid . '" AND `join`.uniacid=' . $_W['uniacid'] . ' AND `join`.`is_reward`=0 ORDER BY `join`.`addtime` DESC LIMIT 0,15';
		$bonustask_running = pdo_fetchall($running_sql);
		foreach ($bonustask_running as $key => $val ) 
		{
			$val['reward_data'] = unserialize($val['poster_reward']);
			$recward = $val['reward_data']['rec'];
			foreach ($recward as $k => $v ) 
			{
				if (isset($v['credit']) && (0 < $v['credit'])) 
				{
					$bonustask_running[$key]['is_credit'] = 1;
				}
				if (isset($v['money']['num']) && (0 < $v['money']['num'])) 
				{
					$bonustask_running[$key]['is_money'] = 1;
				}
				if (isset($v['bribery']) && (0 < $v['bribery'])) 
				{
					$bonustask_running[$key]['is_bribery'] = 1;
				}
				if (isset($v['goods']) && count($v['goods'])) 
				{
					$bonustask_running[$key]['is_goods'] = 1;
				}
				if (isset($v['coupon']['total']) && (0 < $v['coupon']['total'])) 
				{
					$bonustask_running[$key]['is_coupon'] = 1;
				}
				if (isset($v['shiwu']) && count($v['shiwu'])) 
				{
					$bonustask_running[$key]['is_shiwu'] = 1;
				}
			}
		}
		$complete_sql = 'SELECT `join`.*,`bonustask`.title,`bonustask`.titleicon,`bonustask`.bedown,`bonustask`.beown,`bonustask`.poster_type FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' AS `join` LEFT JOIN ' . tablename('yunphp_shop_commission_bonustask') . ' AS `bonustask` ON `join`.bonustask_id=`bonustask`.`id` WHERE `join`.uniacid=' . $_W['uniacid'] . ' AND `join`.`join_user`="' . $openid . '" AND `join`.`is_reward`=1 ORDER BY `join`.`addtime` DESC LIMIT 0,15';
		$bonustask_complete = pdo_fetchall($complete_sql);
		foreach ($bonustask_complete as $key => $val ) 
		{
			$val['reward_data'] = unserialize($val['reward_data']);
			$recward = $val['reward_data'];
			foreach ($recward as $k => $v ) 
			{
				if (isset($v['credit']) && (0 < $v['credit'])) 
				{
					$bonustask_complete[$key]['is_credit'] = 1;
				}
				if (isset($v['money']['num']) && (0 < $v['money']['num'])) 
				{
					$bonustask_complete[$key]['is_money'] = 1;
				}
				if (isset($v['bribery']) && (0 < $v['bribery'])) 
				{
					$bonustask_complete[$key]['is_bribery'] = 1;
				}
				if (isset($v['goods']) && count($v['goods'])) 
				{
					$bonustask_complete[$key]['is_goods'] = 1;
				}
				if (isset($v['coupon']['total']) && (0 < $v['coupon']['total'])) 
				{
					$bonustask_complete[$key]['is_coupon'] = 1;
				}
				if (isset($v['shiwu']) && count($v['shiwu'])) 
				{
					$bonustask_complete[$key]['is_shiwu'] = 1;
				}
			}
		}
		$faile_sql = 'SELECT `join`.*,`bonustask`.title,`bonustask`.reward_data AS `poster_reward`,`bonustask`.titleicon,`bonustask`.bedown,`bonustask`.beown,`bonustask`.poster_type FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' AS `join` LEFT JOIN ' . tablename('yunphp_shop_commission_bonustask') . ' AS `bonustask` ON `join`.bonustask_id=`bonustask`.`id` WHERE `join`.`failtime`<=' . $now_time . ' AND `join`.`join_user`="' . $openid . '" AND `join`.uniacid=' . $_W['uniacid'] . ' AND `join`.`is_reward`=0 ORDER BY `join`.`addtime` DESC LIMIT 0,15';
		$faile_complete = pdo_fetchall($faile_sql);
		foreach ($faile_complete as $key => $val ) 
		{
			$val['reward_data'] = unserialize($val['poster_reward']);
			$recward = $val['reward_data']['rec'];
			foreach ($recward as $k => $v ) 
			{
				if (isset($v['credit']) && (0 < $v['credit'])) 
				{
					$faile_complete[$key]['is_credit'] = 1;
				}
				if (isset($v['money']['num']) && (0 < $v['money']['num'])) 
				{
					$faile_complete[$key]['is_credit'] = 1;
				}
				if (isset($v['bribery']) && (0 < $v['bribery'])) 
				{
					$faile_complete[$key]['is_money'] = 1;
				}
				if (isset($v['goods']) && count($v['goods'])) 
				{
					$faile_complete[$key]['is_goods'] = 1;
				}
				if (isset($v['coupon']['total']) && (0 < $v['coupon']['total'])) 
				{
					$faile_complete[$key]['is_coupon'] = 1;
				}
				if (isset($v['shiwu']) && count($v['shiwu'])) 
				{
					$faile_complete[$key]['is_shiwu'] = 1;
				}
			}
		}
		$advs = pdo_fetchall('select id,advname,link,thumb from ' . tablename('yunphp_shop_commission_bonustaskadv') . ' where uniacid=:uniacid and enabled=1 order by displayorder desc', array(':uniacid' => $_W['uniacid']));
		$advs = set_medias($advs, 'thumb');
		include $this->template();
	}
	public function getbonustask() 
	{
		global $_W;
		global $_GPC;
		$openid = trim($_W['openid']);
		$content = trim(urldecode($_GPC['content']));
		$time = time();
		if (empty($openid)) {
			echo json_encode(array('status' => 0));
			return;
		}
		$member = m('member')->getMember($openid);
		if (empty($member)) 
		{
			echo json_encode(array('status' => 0));
			return;
		}
		$poster = pdo_fetch('select * from ' . tablename('yunphp_shop_commission_bonustask') . ' where id=:id and uniacid=:uniacid and `status`=1 and `is_delete`=0 limit 1', array(':id' => $content, ':uniacid' => $_W['uniacid']));
		if (empty($poster)) 
		{
			m('message')->sendCustomNotice($openid, '未找到任务!');
			echo json_encode(array('status' => 0));
			return;
		}
		
		$task_count = pdo_fetchcolumn('select COUNT(*) from ' . tablename('yunphp_shop_commission_bonustask_join') . ' where uniacid=:uniacid and join_user=:join_user and bonustask_type=' . $poster['poster_type'] . ' and is_reward=0 and failtime>' . time(), array(':uniacid' => $_W['uniacid'], ':join_user' => $member['openid']));
		if (empty($task_count)) {
			$rec_reward = unserialize($poster['reward_data']);
			$rec_reward = $rec_reward['rec'];
			$rec_reward = serialize($rec_reward);
			$bonustask_join = array('uniacid' => $_W['uniacid'], 'join_user' => $member['openid'], 'bonustask_id' => $poster['id'], 'bonustask_type' => 1, 'needcount' => $poster['needcount'], 'failtime' => $time + $poster['days'], 'addtime' => $time);
			if ($poster['poster_type'] == 2) 
			{
				$bonustask_join['bonustask_type'] = 2;
				$bonustask_join['reward_data'] = $rec_reward;
			}
			pdo_insert('yunphp_shop_commission_bonustask_join', $bonustask_join);
			$id = pdo_insertid();
			if (!empty($id)) {
				echo json_encode(array('status' => 1));
			}
			
		} else {
			m('message')->sendCustomNotice($openid, '您已经接了这个任务，努力协助您的团队完成任务吧!');
			echo json_encode(array('status' => 0));
			return;
			
		}
	}
	public function getbonustaskinfo() 
	{
		global $_W;
		global $_GPC;
		if (intval($_GPC['id'])) 
		{
			$param = array(':id' => intval($_GPC['id']), ':uniacid' => $_W['uniacid']);
			$now_time = time();
			$bonustask_sql = 'SELECT * FROM ' . tablename('yunphp_shop_commission_bonustask') . ' WHERE timestart<=' . $now_time . ' AND timeend>' . $now_time . ' AND uniacid=:uniacid AND id=:id AND `status`=1  ';
			$bonustaskinfo = pdo_fetch($bonustask_sql, $param);
			$bonustaskinfo['reward_data'] = unserialize($bonustaskinfo['reward_data']);
			$db_data = pdo_fetchcolumn('select `data` from ' . tablename('yunphp_shop_commission_bonustaskset') . ' where uniacid=:uniacid limit 1', array(':uniacid' => $_W['uniacid']));
				$bonustaskown = pdo_fetch('SELECT * FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' WHERE failtime>' . $now_time . ' AND is_reward= 0 AND uniacid=' . $_W['uniacid'] . ' AND bonustask_id= ' . $bonustaskinfo['id'] . ' ');
				if (!empty($bonustaskown)) {
					$bonustaskinfo['bonustaskown'] = 1;
				} else {
					$bonustaskinfo['bonustaskown'] = 0;
				}
			$res = '';
			if (!(empty($db_data))) 
			{
				$res = unserialize($db_data);
			}
			$is_get = 0;
			if ($res['is_posterall'] == 1) 
			{
				$bonustask_count = pdo_fetchcolumn('select COUNT(*) from ' . tablename('yunphp_shop_commission_bonustask_join') . ' where uniacid=:uniacid and join_user=:join_user and bonustask_type=' . $bonustaskinfo['poster_type'] . ' and is_reward=0 and failtime>' . time(), array(':uniacid' => $_W['uniacid'], ':join_user' => $_W['openid']));
				if (empty($bonustask_count)) 
				{
					$end_bonustask_count = pdo_fetchcolumn('select COUNT(*) from ' . tablename('yunphp_shop_commission_bonustask_join') . ' where uniacid=:uniacid and join_user=:join_user and bonustask_id=' . $bonustaskinfo['id'] . ' and (is_reward=1 or failtime<' . time() . ')', array(':uniacid' => $_W['uniacid'], ':join_user' => $_W['openid']));
					if ($end_bonustask_count) 
					{
						if ($bonustaskinfo['is_repeat']) 
						{
							$is_get = 1;
						}
					}
					else 
					{
						$is_get = 1;
					}
				}
			}
			else 
			{
				$poster_type = 1;
				if ($bonustaskinfo['poster_type'] == 1) 
				{
					$poster_type = 2;
				}
				$other_bonustask_count = pdo_fetchcolumn('select COUNT(*) from ' . tablename('yunphp_shop_commission_bonustask_join') . ' where uniacid=:uniacid and join_user=:join_user and bonustask_type=' . $poster_type . ' and failtime>' . time(), array(':uniacid' => $_W['uniacid'], ':join_user' => $_W['openid']));
				$bonustask_count = pdo_fetchcolumn('select bonustask_id from ' . tablename('yunphp_shop_commission_bonustask_join') . ' where uniacid=:uniacid and join_user=:join_user and bonustask_type=' . $bonustaskinfo['poster_type'] . ' and failtime>' . time(), array(':uniacid' => $_W['uniacid'], ':join_user' => $_W['openid']));
				if (empty($other_bonustask_count) && empty($bonustask_count)) 
				{
					$is_get = 1;
				}
				else if (empty($other_bonustask_count) && !(empty($bonustask_count)) && ($bonustask_count == intval($_GPC['id']))) 
				{
					$is_get = 1;
				}
			}
			if ($bonustaskinfo['poster_type'] == 1) 
			{
				include $this->template('commission/bonustask/bonustaskinfo');
			}
			else if ($bonustaskinfo['poster_type'] == 2) 
			{
				$rankinfo = array();
				$rankinfoone = array(1 => $res['bonustaskranktitle'] . '1', 2 => $res['bonustaskranktitle'] . '2', 3 => $res['bonustaskranktitle'] . '3', 4 => $res['bonustaskranktitle'] . '4', 5 => $res['bonustaskranktitle'] . '5', 6 => $res['bonustaskranktitle'] . '6', 7 => $res['bonustaskranktitle'] . '7', 8 => $res['bonustaskranktitle'] . '8', 9 => $res['bonustaskranktitle'] . '9', 10 => $res['bonustaskranktitle'] . '10');
				$rankinfotwo = array(1 => $res['bonustaskranktitle'] . 'Ⅰ', 2 => $res['bonustaskranktitle'] . 'Ⅱ', 3 => $res['bonustaskranktitle'] . 'Ⅲ', 4 => $res['bonustaskranktitle'] . 'Ⅳ', 5 => $res['bonustaskranktitle'] . 'Ⅴ', 6 => $res['bonustaskranktitle'] . 'VI', 7 => $res['bonustaskranktitle'] . 'VII', 8 => $res['bonustaskranktitle'] . 'VIII', 9 => $res['bonustaskranktitle'] . 'IX', 10 => $res['bonustaskranktitle'] . 'X');
				$rankinfothree = array(1 => $res['bonustaskranktitle'] . 'A', 2 => $res['bonustaskranktitle'] . 'B', 3 => $res['bonustaskranktitle'] . 'C', 4 => $res['bonustaskranktitle'] . 'D', 5 => $res['bonustaskranktitle'] . 'E', 6 => $res['bonustaskranktitle'] . 'F', 7 => $res['bonustaskranktitle'] . 'G', 8 => $res['bonustaskranktitle'] . 'H', 9 => $res['bonustaskranktitle'] . 'I', 10 => $res['bonustaskranktitle'] . 'J');
				if ($res['bonustaskranktype'] == 1) 
				{
					$rankinfo = $rankinfoone;
				}
				else if ($res['bonustaskranktype'] == 2) 
				{
					$rankinfo = $rankinfotwo;
				}
				else if ($res['bonustaskranktype'] == 3) 
				{
					$rankinfo = $rankinfothree;
				}
				include $this->template('commission/bonustask/bonustaskinfo');
			}
		}
		else 
		{
			$bonustaskinfo = '';
			include $this->template('commission/bonustask/bonustaskinfo');
		}
	}
	public function getcompleteinfo() 
	{
		global $_W;
		global $_GPC;
		if (intval($_GPC['id'])) 
		{
			$param = array(':join_user' => $_W['openid'], ':join_id' => intval($_GPC['id']));
			$bonustask_sql = 'SELECT `join`.*,`bonustask`.title,`bonustask`.titleicon,`bonustask`.poster_banner,`bonustask`.bedown FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' AS `join` LEFT JOIN ' . tablename('yunphp_shop_commission_bonustask') . ' AS `bonustask` ON `join`.bonustask_id=`bonustask`.`id` WHERE `join`.uniacid=' . $_W['uniacid'] . ' AND `join`.join_user=:join_user AND `join`.`join_id`=:join_id ';
			$bonustaskinfo = pdo_fetch($bonustask_sql, $param);
			if (!(empty($bonustaskinfo['reward_data']))) 
			{
				$bonustaskinfo['reward_data'] = unserialize($bonustaskinfo['reward_data']);
			}
			$db_data = pdo_fetchcolumn('select `data` from ' . tablename('yunphp_shop_commission_bonustaskset') . ' where uniacid=:uniacid limit 1', array(':uniacid' => $_W['uniacid']));
			$res = '';
			if (!(empty($db_data))) 
			{
				$res = unserialize($db_data);
			}
			if ($bonustaskinfo['bonustask_type'] == 1) 
			{
				include $this->template('commission/bonustask/complete');
			}
			else if ($bonustaskinfo['bonustask_type'] == 2) 
			{
				$rankinfo = array();
				$rankinfoone = array(1 => $res['bonustaskranktitle'] . '1', 2 => $res['bonustaskranktitle'] . '2', 3 => $res['bonustaskranktitle'] . '3', 4 => $res['bonustaskranktitle'] . '4', 5 => $res['bonustaskranktitle'] . '5', 6 => $res['bonustaskranktitle'] . '6', 7 => $res['bonustaskranktitle'] . '7', 8 => $res['bonustaskranktitle'] . '8', 9 => $res['bonustaskranktitle'] . '9', 10 => $res['bonustaskranktitle'] . '10');
				$rankinfotwo = array(1 => $res['bonustaskranktitle'] . 'Ⅰ', 2 => $res['bonustaskranktitle'] . 'Ⅱ', 3 => $res['bonustaskranktitle'] . 'Ⅲ', 4 => $res['bonustaskranktitle'] . 'Ⅳ', 5 => $res['bonustaskranktitle'] . 'Ⅴ', 6 => $res['bonustaskranktitle'] . 'VI', 7 => $res['bonustaskranktitle'] . 'VII', 8 => $res['bonustaskranktitle'] . 'VIII', 9 => $res['bonustaskranktitle'] . 'IX', 10 => $res['bonustaskranktitle'] . 'X');
				$rankinfothree = array(1 => $res['bonustaskranktitle'] . 'A', 2 => $res['bonustaskranktitle'] . 'B', 3 => $res['bonustaskranktitle'] . 'C', 4 => $res['bonustaskranktitle'] . 'D', 5 => $res['bonustaskranktitle'] . 'E', 6 => $res['bonustaskranktitle'] . 'F', 7 => $res['bonustaskranktitle'] . 'G', 8 => $res['bonustaskranktitle'] . 'H', 9 => $res['bonustaskranktitle'] . 'I', 10 => $res['bonustaskranktitle'] . 'J');
				if ($res['bonustaskranktype'] == 1) 
				{
					$rankinfo = $rankinfoone;
				}
				else if ($res['bonustaskranktype'] == 2) 
				{
					$rankinfo = $rankinfotwo;
				}
				else if ($res['bonustaskranktype'] == 3) 
				{
					$rankinfo = $rankinfothree;
				}
				else 
				{
					$rankinfo = $rankinfoone;
				}
				include $this->template('commission/bonustask/rankcomplete');
			}
		}
		else 
		{
			$bonustaskinfo = '';
			include $this->template('commission/bonustask/complete');
		}
	}
	public function goodslist() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		$bonustasksql = 'SELECT * FROM ' . tablename('yunphp_shop_commission_bonustask_join') . ' WHERE `join_id`=' . $id . ' AND `join_user`=:join_user';
		$bonustaskinfo = pdo_fetch($bonustasksql, array(':join_user' => $_W['openid']));
		$goodslist = array();
		if ($bonustaskinfo['bonustask_type'] == 1) 
		{
			$bonustaskinfo['reward_data'] = unserialize($bonustaskinfo['reward_data']);
			$goodslist = $bonustaskinfo['reward_data']['goods'];
			foreach ($bonustaskinfo['reward_data']['goods'] as $k => $v ) 
			{
				$searchsql = 'SELECT thumb,marketprice FROM ' . tablename('yunphp_shop_goods') . ' WHERE uniacid= ' . $_W['uniacid'] . ' and id=' . $k . ' and status=1 and deleted=0';
				$goodsinfo = pdo_fetch($searchsql);
				$thumb = tomedia($goodsinfo['thumb']);
				$goodslist[$k]['thumb'] = $thumb;
				$goodslist[$k]['oldprice'] = $goodsinfo['marketprice'];
				if (!(empty($goodslist[$k]['spec']))) 
				{
					$goodslist[$k]['total'] = 0;
					foreach ($goodslist[$k]['spec'] as $key => $val ) 
					{
						if ($val['marketprice'] < $goodslist[$k]['marketprice']) 
						{
							$goodslist[$k]['marketprice'] = $val['marketprice'];
						}
						$goodslist[$k]['total'] += $val['total'];
					}
				}
			}
			include $this->template('bonustask/goodslist');
		}
		else if ($bonustaskinfo['bonustask_type'] == 2) 
		{
			$bonustaskinfo['reward_data'] = unserialize($bonustaskinfo['reward_data']);
			foreach ($bonustaskinfo['reward_data'] as $key => $value ) 
			{
				if (isset($value['is_reward']) && ($value['is_reward'] == 1)) 
				{
					$goodslist[$key] = $value['goods'];
					foreach ($value['goods'] as $k => $val ) 
					{
						$searchsql = 'SELECT thumb,marketprice FROM ' . tablename('yunphp_shop_goods') . ' WHERE uniacid= ' . $_W['uniacid'] . ' and id=' . $k . ' and status=1 and deleted=0';
						$goodsinfo = pdo_fetch($searchsql);
						$thumb = tomedia($goodsinfo['thumb']);
						$goodslist[$key][$k]['thumb'] = $thumb;
						$goodslist[$key][$k]['oldprice'] = $goodsinfo['marketprice'];
						if (!(empty($goodslist[$key][$k]['spec']))) 
						{
							$goodslist[$key][$k]['total'] = 0;
							foreach ($goodslist[$k]['spec'] as $ke => $va ) 
							{
								if ($va['marketprice'] < $goodslist[$key][$k]['marketprice']) 
								{
									$goodslist[$key][$k]['marketprice'] = $va['marketprice'];
								}
								$goodslist[$key][$k]['total'] += $va['total'];
							}
						}
					}
				}
			}
			include $this->template('bonustask/goodslist');
		}
	}
	public function getMoreBonustask() 
	{
	}
}
?>