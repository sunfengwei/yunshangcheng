<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
require YUNPHP_SHOP_PLUGIN . 'openapi/core/page_login_mobile.php';
class Index_YunphpShopPage extends OpenapiMobileLoginPage
{
	
	public function main()
	{
		global $_W;
		global $_GPC;
		$member = m('member')->getInfo($_W['openid']);
		$wapset = m('common')->getSysset('wap');
		include $this->template();
	}
	public function trademark()
	{
		global $_W;
		global $_GPC;
		$url = mobileUrl('openapi/trademark');
		$backurl = mobileUrl('member/bind', array('backurl' => base64_encode($url)));
		$member = m('member')->getInfo($_W['openid']);
		$wapset = m('common')->getSysset('wap');
		$area_set = m('util')->get_area_config_set();
		$new_area = intval($area_set['new_area']);
		$set = $_W['shopset'];
		$_W['shopshare'] = array('title' => '免费商标查询', 'imgUrl' => (empty($set['share']['icon']) ? tomedia($set['shop']['logo']) : tomedia($set['share']['icon'])), 'desc' => '与国家商标局数据库同步，免费查询商标，三秒得知商标注册信息', 'link' => mobileUrl('openapi/trademark', NULL, true));
		if (p('commission')) 
		{
			$pset = p('commission')->getSet();
			if (!(empty($pset['level']))) 
			{
				if (!(empty($member)) && ($member['status'] == 1) && ($member['isagent'] == 1)) 
				{
					$_W['shopshare']['link'] = $_W['shopshare']['link'] . '&mid=' . $member['id'];
					if (empty($pset['become_reg']) && (empty($member['realname']) || empty($member['mobile']))) 
					{
						$trigger = true;
					}
				}
				else if (!(empty($_GPC['mid']))) 
				{
					$_W['shopshare']['link'] = $_W['shopshare']['link'] . '&mid=' . $_GPC['id'];
				}
			}
		}
		include $this->template();
	}
	
	public function search()
	{
		global $_W;
		global $_GPC;
		//----------------------------------
		// 商标信息查询调用示例代码 － 聚合数据
		// 在线接口文档：http://www.juhe.cn/docs/178
		//----------------------------------
		 
		header('Content-type:text/html;charset=utf-8');
		 
		 
		//配置您申请的appkey
		$appkeys = pdo_fetch('SELECT appKey FROM ' . tablename('yunphp_shop_openapi_apiset') . ' WHERE apitype=1 AND status=1 AND uniacid=' . $_W['uniacid'] . ' limit 1 ');
		$appkey = $appkeys['appKey'];
		if (empty($appkey)) 
		{
			show_json(0, '1');
		}
		$keyword = trim($_GPC['keyword']);
		if (empty($keyword)) 
		{
			show_json(0, '2');
		}
		$searchType = trim($_GPC['searchType']);
		if (empty($searchType)) 
		{
			show_json(0, '3');
		}
		//************1.模糊查询商标列表************
		$url = "http://japi.juhe.cn/trademark/search";
		$params = array(
			  "key" => $appkey,//您申请的appKey
			  "keyword" => $keyword,//待搜索的关键词
			  "pageSize" => "50",//    页面大小，即一次api调用最大获取多少条记录，取值范围：[1-50]
			  "pageNo" => "1",//当前页码数，即本次api调用是获得结果的第几页，从1开始计数
			  "searchType" => $searchType,//默认=4,按什么来查，1: 商标名， 2：注册号， 3：申请人 4：商标名/注册号/申请人只要模糊匹配
			  "intCls   " => "0",//默认 =0,0：全部国际分类,非0：限定在指定类别，类别间用分号分割。如：4;12;34  表示在第4、12、34类内查询
		);
		
		$data = $params;
		$data['uniacid'] = $_W['uniacid'];
		$data['openid'] = $_W['openid'];
		$data['createtime'] = time();
		pdo_insert('yunphp_shop_openapi_record', $data);
		$id = pdo_insertid();
		$paramstring = http_build_query($params);
		$content = $this->model->juhecurl($url,$paramstring);
		$result = json_decode($content,true);
		if($result){
			if($result['error_code']=='0'){
				$result_data = $result['result']['data'];
				
			}else{
				$error_code = $result['error_code'];
			}
		}else{
			$error_code = '商标注册局接口繁忙，请稍后再试！';
		}
		pdo_update('yunphp_shop_openapi_record', array('data' => $content, 'status' => 0, 'apitype' => 1, 'results' => $error_code), array('id' => $id, 'uniacid' => $_W['uniacid'], 'openid' => $_W['openid']));
		//**************************************************
		include $this->template();
	}
	public function main1()
	{
		global $_W;
		global $_GPC;
		//************2.查询申请人列表************
		$url = "http://japi.juhe.cn/trademark/applist";
		$params = array(
			  "key" => $appkey,//您申请的appKey
			  "keyword" => "",//待搜索的关键词
			  "pageSize" => "",//    页面大小，即一次api调用最大获取多少条记录，取值范围：[1-50]
			  "pageNo" => "",//当前页码数，即本次api调用是获得结果的第几页，从1开始计数
			  "pattern" => "",//匹配方式：0-默认，模糊匹配；1-完全相同，2-前包含
		);
		$paramstring = http_build_query($params);
		$content = juhecurl($url,$paramstring);
		$result = json_decode($content,true);
		if($result){
			if($result['error_code']=='0'){
				print_r($result);
			}else{
				echo $result['error_code'].":".$result['reason'];
			}
		}else{
			echo "请求失败";
		}
		//**************************************************
	}
	public function main2()
	{
		global $_W;
		global $_GPC;
		//************3.查询指定申请人的全部商标列表************
		$url = "http://japi.juhe.cn/trademark/marklist";
		$params = array(
			  "key" => $appkey,//您申请的appKey
			  "applicantCn" => "",//申请人中文名
			  "idCardNo" => "",//申请人身份证号。如果申请人为公司，则无需传入；如果申请人为个人，推荐传入身份证号，如果不知道身份证号，则可以不传入，此时，此接口返回您所查询的申请人名的商标（理论上是存在申请人同名的情况，可能有些商标并不是您所需要的）。
		);
		$paramstring = http_build_query($params);
		$content = juhecurl($url,$paramstring);
		$result = json_decode($content,true);
		if($result){
			if($result['error_code']=='0'){
				print_r($result);
			}else{
				echo $result['error_code'].":".$result['reason'];
			}
		}else{
			echo "请求失败";
		}
//**************************************************
	}
	public function main3()
	{
		global $_W;
		global $_GPC;
		//************4.查询指定商标的详细信息************
		$url = "http://japi.juhe.cn/trademark/detail";
		$params = array(
			  "key" => $appkey,//你申请的appKey
			  "regNo" => "",//注册号/申请号
			  "intCls   " => "",//国际分类,可&lt;a href=&quot;http://www.juhe.cn/docs/api/id/178/aid/605&quot;&gt;模糊查询商标列表&lt;/a&gt;获得
		);
		$paramstring = http_build_query($params);
		$content = juhecurl($url,$paramstring);
		$result = json_decode($content,true);
		if($result){
			if($result['error_code']=='0'){
				print_r($result);
			}else{
				echo $result['error_code'].":".$result['reason'];
			}
		}else{
			echo "请求失败";
		}
	}
	

}


?>