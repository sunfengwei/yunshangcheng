<?php
if (!defined("IN_IA")) {
	exit("Access Denied");
}

class OpenapiModel extends PluginModel
{
	//**************************************************
	/**
	 * 请求接口返回内容
	 * @param  string $url [请求的URL地址]
	 * @param  string $params [请求的参数]
	 * @param  int $ipost [是否采用POST形式]
	 * @return  string
	 */
	function juhecurl($url,$params=false,$ispost=0){
		$httpInfo = array();
		$ch = curl_init();
	 
		curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
		curl_setopt( $ch, CURLOPT_USERAGENT , 'JuheData' );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
		curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if( $ispost )
		{
			curl_setopt( $ch , CURLOPT_POST , true );
			curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
			curl_setopt( $ch , CURLOPT_URL , $url );
		}
		else
		{
			if($params){
				curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
			}else{
				curl_setopt( $ch , CURLOPT_URL , $url);
			}
		}
		$response = curl_exec( $ch );
		if ($response === FALSE) {
			//echo "cURL Error: " . curl_error($ch);
			return false;
		}
		$httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
		$httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
		curl_close( $ch );
		return $response;
	}
	public function getLevels($all = true, $default = false) 
	{
		global $_W;
		if ($all) 
		{
			$levels = pdo_fetchall('select * from ' . tablename('yunphp_shop_events_level') . ' where uniacid=:uniacid order by bonus asc', array(':uniacid' => $_W['uniacid']));
		}
		else 
		{
			$levels = pdo_fetchall('select * from ' . tablename('yunphp_shop_events_level') . ' where uniacid=:uniacid and (ordermoney>0 or commissionmoney>0 or bonusmoney>0) order by bonus asc', array(':uniacid' => $_W['uniacid']));
		}
		if ($default) 
		{
			$default = array('id' => '0', 'levelname' => (empty($_S['events']['levelname']) ? '默认等级' : $_S['events']['levelname']), 'bonus' => $_W['shopset']['events']['bonus']);
			$levels = array_merge(array($default), $levels);
		}
		return $levels;
	}
	public function checkMember() 
	{
		global $_W;
		global $_GPC;
		if (!(empty($_W['openid']))) 
		{
			$member = pdo_fetch('select * from ' . tablename('yunphp_shop_openapi_member') . ' where uniacid=:uniacid and openid=:openid limit 1', array(':uniacid' => $_W['uniacid'], ':openid' => $_W['openid']));
			if (empty($member)) 
			{
				$member = array('uniacid' => $_W['uniacid'], 'openid' => $_W['openid'], 'createtime' => time());
				pdo_insert('yunphp_shop_openapi_member', $member);
			}
			else if (!(empty($member['isblack']))) 
			{
				show_message('禁止访问，请联系客服!');
			}
		}
	}
	public function getMember($openid) 
	{
		global $_W;
		global $_GPC;
		$member = m('member')->getMember($openid);
		$sns_member = pdo_fetch('select * from ' . tablename('yunphp_shop_openapi_member') . ' where uniacid=:uniacid and openid=:openid limit 1', array(':uniacid' => $_W['uniacid'], ':openid' => $member['openid']));
		if (empty($sns_member)) 
		{
			$member['sns_credit'] = 0;
			$member['sns_level'] = 0;
			$member['notupgrade'] = 0;
		}
		else 
		{
			$member['sns_id'] = $sns_member['id'];
			$member['sns_credit'] = $sns_member['credit'];
			$member['sns_level'] = $sns_member['level'];
			$member['sns_sign'] = $sns_member['sign'];
			$member['sns_notupgrade'] = $sns_member['notupgrade'];
			$member['eventsuserid'] = $sns_member['id'];
		}
		return $member;
	}
	
	
	
}


?>