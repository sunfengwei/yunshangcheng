<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
class Record_YunphpShopPage extends PluginWebPage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$isManager = false;
		if (($_W['role'] == 'manager') || ($_W['role'] == 'founder')) 
		{
			$isManager = true;
		}
		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$condition = ' and p.uniacid = :uniacid ';
		$params = array(':uniacid' => $_W['uniacid']);
		
		$permuid = intval($_GPC['permuid']);
		if ($permuid == 1) 
		{
			$condition .= ' and om.permuid = :permuid';
			$params[':permuid'] = trim($_W['uid']);
			
		} elseif ($permuid == 2) {
			$condition .= ' and om.permuid = 0';
		}
		$deleted = intval($_GPC['deleted']);
		if (!(empty($deleted))) 
		{
			$condition .= ' and p.deleted = 1';
		} else {
			$condition .= ' and p.deleted = 0';
		}
		if ($_GPC['deleted'] != '') 
		{
			$condition .= ' and deleted=' . intval($_GPC['deleted']);
		}
		if ($_GPC['checked'] != '') 
		{
			$condition .= ' and checked=' . intval($_GPC['checked']);
		}
		$uid = intval($_GPC['uid']);
		if (!(empty($uid))) 
		{
			$m = m('member')->getMember($uid);
			if (!(empty($m))) 
			{
				$condition .= ' and p.openid=:openid';
				$params[':openid'] = $m['openid'];
			}
		}
		if (!(empty($_GPC['keyword']))) 
		{
			$_GPC['keyword'] = trim($_GPC['keyword']);
			$condition .= ' and ( p.title  like :keyword or p.avatar like :keyword )';
			$params[':keyword'] = '%' . $_GPC['keyword'] . '%';
		}
		$sql = 'select p.*, m.avatar, m.mobile, m.realname, m.nickname' . '  from ' . tablename('yunphp_shop_openapi_record') . ' p ' . ' left join ' . tablename('yunphp_shop_member') . ' m on m.openid = p.openid and m.uniacid = p.uniacid left join ' . tablename('yunphp_shop_openapi_member') . ' om on om.openid = m.openid and om.uniacid = m.uniacid where 1 ' . $condition . ' ORDER BY p.createtime DESC LIMIT ' . (($pindex - 1) * $psize) . ',' . $psize;
		$list = pdo_fetchall($sql, $params);
		$total = pdo_fetchcolumn('select count(*) from ' . tablename('yunphp_shop_openapi_record') . ' p ' . ' left join ' . tablename('yunphp_shop_member') . ' m on m.openid = p.openid and m.uniacid = p.uniacid left join ' . tablename('yunphp_shop_openapi_member') . ' om on om.openid = m.openid and om.uniacid = m.uniacid where 1 ' . $condition, $params);
		foreach ($list as $key => &$row ) {
			$result = json_decode($row['data'],true);
			if (count($result['result']['data']) > 0)
			{
				$row['isempty'] = 0;
				$row['resultdata'] = $result['result']['data'];
			} else {
				$row['isempty'] = 1;
			}
			
		}
		unset($row);
		$pager = pagination($total, $pindex, $psize);
		
		$opencommission = false;
		$plug_commission = p('commission');

		if ($plug_commission) {
			$comset = $plug_commission->getSet();

			if (!empty($comset)) {
				$opencommission = true;
			}

		}
		
		include $this->template();
	}
	public function delete() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		if (empty($id)) 
		{
			$id = ((is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0));
		}
		$deleted = intval($_GPC['deleted']);
		$items = pdo_fetchall('SELECT id,keyword,openid FROM ' . tablename('yunphp_shop_openapi_record') . ' WHERE id in( ' . $id . ' ) AND uniacid=' . $_W['uniacid']);
		foreach ($items as $item ) 
		{
			if ($deleted) 
			{
				plog('openapi.record.delete', '删除查询记录 ID: ' . $item['id'] . ' 关键字: ' . $item['keyword'] . ' openid: ' . $item['openid']);
				pdo_update('yunphp_shop_openapi_record', array('deleted' => 1, 'deletedtime' => time()), array('id' => $item['id']));
			}
			else 
			{
				plog('openapi.record.delete', '删除查询记录 ID: ' . $item['id'] . ' 关键字: ' . $item['keyword'] . ' openid: ' . $item['openid']);
				pdo_update('yunphp_shop_openapi_record', array('deleted' => 0, 'deletedtime' => 0), array('id' => $item['id']));
			}
		}
		show_json(1, array('url' => referer()));
	}
	public function rob() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		$item = pdo_fetch('SELECT id,keyword,openid FROM ' . tablename('yunphp_shop_openapi_record') . ' WHERE id=' . $id . ' AND uniacid=' . $_W['uniacid']);
		pdo_update('yunphp_shop_openapi_member', array('permuid' => trim($_W['uid']), 'permtime' => time()), array('openid' => $item['openid']));
		show_json(1, array('url' => referer()));
	}
}