<?php
if (!defined("IN_IA")) {
	exit("Access Denied");
}

class Index_YunphpShopPage extends PluginWebPage
{
	public function main()
	{
		header("location: " . webUrl("openapi/member"));
		exit();
	}

	public function index()
	{
		global $_W;
		include $this->template();
	}
}


?>