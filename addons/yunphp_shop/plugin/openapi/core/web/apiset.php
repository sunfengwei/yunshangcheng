<?php
if (!defined("IN_IA")) {
	exit("Access Denied");
}

class Apiset_YunphpShopPage extends PluginWebPage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$condition = ' and uniacid=:uniacid';
		$params = array(':uniacid' => $_W['uniacid']);
		if ($_GPC['status'] != '') 
		{
			$condition .= ' and status=' . intval($_GPC['status']);
		}
		if (!(empty($_GPC['keyword']))) 
		{
			$_GPC['keyword'] = trim($_GPC['keyword']);
			$condition .= ' and catename  like :keyword';
			$params[':keyword'] = '%' . $_GPC['keyword'] . '%';
		}
		$list = pdo_fetchall('SELECT * FROM ' . tablename('yunphp_shop_openapi_apiset') . ' WHERE 1 ' . $condition . '  ORDER BY displayorder desc, id DESC limit ' . (($pindex - 1) * $psize) . ',' . $psize, $params);
		$total = pdo_fetchcolumn('SELECT count(*) FROM ' . tablename('yunphp_shop_openapi_apiset') . ' WHERE 1 ' . $condition, $params);
		
		$pager = pagination($total, $pindex, $psize);
		include $this->template();
	}
	public function add() 
	{
		$this->post();
	}
	public function edit() 
	{
		$this->post();
	}
	protected function post() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		if ($_W['ispost']) 
		{
			$data = array('uniacid' => $_W['uniacid'], 'catename' => str_replace(' ','',$_GPC['catename']), 'status' => intval($_GPC['status']), 'displayorder' => intval($_GPC['displayorder']), 'isrecommand' => intval($_GPC['isrecommand']),'apitype' => intval($_GPC['apitype']),'appKey' => trim($_GPC['appKey']));
			if (empty($_GPC['apitype'])) 
			{
				show_json(0, '请选择API类型!');
			}
			
			if (!(empty($id))) 
			{
				pdo_update('yunphp_shop_openapi_apiset', $data, array('id' => $id));
				plog('openapi.apiset.edit', '修改API ID: ' . $id);
			}
			else 
			{
				$data['createtime'] = time();
				pdo_insert('yunphp_shop_openapi_apiset', $data);
				$id = pdo_insertid();
				plog('openapi.apiset.add', '添加API ID: ' . $id);
			}
			show_json(1, array('url' => webUrl('openapi/apiset')));
		}
		$item = pdo_fetch('select * from ' . tablename('yunphp_shop_openapi_apiset') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
		
		include $this->template();
	}
	public function delete() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		if (empty($id)) 
		{
			$id = ((is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0));
		}
		$items = pdo_fetchall('SELECT id,catename FROM ' . tablename('yunphp_shop_openapi_apiset') . ' WHERE id in( ' . $id . ' ) AND uniacid=' . $_W['uniacid']);
		foreach ($items as $item ) 
		{
			pdo_delete('yunphp_iot_tertype', array('id' => $item['id']));
			plog('tyiot_tertype.delete', '删除商户分类 ID: ' . $item['id'] . ' 标题: ' . $item['catename'] . ' ');
		}
		show_json(1, array('url' => referer()));
	}
	public function status() 
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		if (empty($id)) 
		{
			$id = ((is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0));
		}
		$items = pdo_fetchall('SELECT id,catename FROM ' . tablename('yunphp_shop_openapi_apiset') . ' WHERE id in( ' . $id . ' ) AND uniacid=' . $_W['uniacid']);
		foreach ($items as $item ) 
		{
			pdo_update('yunphp_shop_openapi_apiset', array('status' => intval($_GPC['status'])), array('id' => $item['id']));
			plog('tyiot_tertype.edit', (('修改商户分类状态<br/>ID: ' . $item['id'] . '<br/>分类名称: ' . $item['catename'] . '<br/>状态: ' . $_GPC['status']) == 1 ? '启用' : '禁用'));
		}
		show_json(1, array('url' => referer()));
	}
}