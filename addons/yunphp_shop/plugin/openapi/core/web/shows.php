<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Shows_YunphpShopPage extends PluginWebPage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$condition = ' and p.uniacid = :uniacid';
		$params = array(':uniacid' => $_W['uniacid']);
		$pid = intval($_GPC['id']);
		

		if (!empty($pid)) {
			$post = pdo_fetch('select * from ' . tablename('yunphp_shop_openapi_record') . ' where uniacid=:uniacid and id=:id  limit 1', array(':uniacid' => $_W['uniacid'], ':id' => $pid));
			$member = $this->model->getMember($post['openid']);
			$member['postcount'] = pdo_fetchcolumn('select count(*) from ' . tablename('yunphp_shop_openapi_record') . ' where uniacid=:uniacid and openid=:openid limit 1', array(':uniacid' => $_W['uniacid'], ':openid' => $member['openid']));
			$member['replycount'] = pdo_fetchcolumn('select count(*) from ' . tablename('yunphp_shop_openapi_record') . ' where uniacid=:uniacid and openid=:openid  limit 1', array(':uniacid' => $_W['uniacid'], ':openid' => $member['openid']));
			$level = array('levelname' => (empty($set['levelname']) ? '社区粉丝' : $set['levelname']), 'color' => (empty($set['levelcolor']) ? '#333' : $set['levelcolor']), 'bg' => (empty($set['levelbg']) ? '#eee' : $set['levelbg']));

			if (!empty($member['sns_level'])) {
				$level = pdo_fetch('select * from ' . tablename('yunphp_shop_openapi_level') . ' where id=:id  limit 1', array(':id' => $member['sns_level']));
			}
			$allresult = pdo_fetchall('select * from ' . tablename('yunphp_shop_openapi_record') . ' where uniacid=:uniacid and openid=:openid  ORDER BY createtime desc', array(':uniacid' => $_W['uniacid'], ':openid' => $member['openid']));
			foreach ($allresult as $key => &$row ) {
				$result = json_decode($row['data'],true);
				if (count($result['result']['data']) > 0)
				{
					$row['isempty'] = 0;
					$row['resultdata'] = $result['result']['data'];
				} else {
					$row['isempty'] = 1;
				}
				
			}
			unset($row);
		}


		


		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$condition = ' and `uniacid` = :uniacid ';
		$params = array(':uniacid' => $_W['uniacid']);

		if (!empty($post)) {
			$condition .= ' and openid=:openid ';
			$params[':openid'] = $member['openid'];
		}


		if ($_GPC['deleted'] != '') {
			$condition .= ' and deleted=' . intval($_GPC['deleted']);
		}


		if ($_GPC['checked'] != '') {
			$condition .= ' and checked=' . intval($_GPC['checked']);
		}


		if (!empty($_GPC['keyword'])) {
			$_GPC['keyword'] = trim($_GPC['keyword']);
			$condition .= ' and p.title  like :keyword';
			$params[':keyword'] = '%' . $_GPC['keyword'] . '%';
		}


		$sql = 'select * from ' . tablename('yunphp_shop_openapi_shows') . '  where 1 ' . $condition . ' ORDER BY createtime asc LIMIT ' . (($pindex - 1) * $psize) . ',' . $psize;
		$list = pdo_fetchall($sql, $params);
		$total = pdo_fetchcolumn('select count(*) from ' . tablename('yunphp_shop_openapi_shows') . ' where 1 ' . $condition, $params);

		$pager = pagination($total, $pindex, $psize);
		include $this->template();
	}
	
	public function add() 
	{
		$this->post();
	}
	public function edit() 
	{
		$this->post();
	}
	protected function post() 
	{
		global $_W;
		global $_GPC;
		global $_S;
		$openid = $_GPC['openid'];
		$recordid = $_GPC['recordid'];
		$id = trim($_GPC['id']);
		if (!empty($id)) {
			$shows = pdo_fetch('SELECT * FROM ' . tablename('yunphp_shop_openapi_shows') . ' WHERE id=:id and uniacid=:uniacid limit 1', array(':id' => intval($id), ':uniacid' => $_W['uniacid']));
		}
		$member = $this->model->getMember($openid);
		if ($_W['ispost']) 
		{
			$data = array('uniacid' => $_W['uniacid'], 'permid' => trim($_W['uid']), 'openid' => trim($openid), 'severtype' => intval($_GPC['severtype']), 'tel' => trim($_GPC['tel']), 'content' => trim($_GPC['content']), 'note' => trim($_GPC['note']), 'userid' => $member['id'], 'name' => trim($_GPC['name']), 'status' => intval($_GPC['status']));
			if (empty($data['severtype']))
			{
				show_json(0, "请选择联系方式");
			}
			if (empty($data['tel']))
			{
				show_json(0, "请填写联系号码");
			}
			if (empty($data['content']))
			{
				show_json(0, "请填写内容概要");
			}
			if (empty($data['status']))
			{
				show_json(0, "请填写联系结果");
			}
			if (!(empty($id))) 
			{
				$updatecontent = '<br/>等级名称: ' . $level['levelname'] . '->' . $data['levelname'] . '<br/>一级佣金比例: ' . $level['commission1'] . '->' . $data['commission1'] . '<br/>二级佣金比例: ' . $level['commission2'] . '->' . $data['commission2'] . '<br/>三级佣金比例: ' . $level['commission3'] . '->' . $data['commission3'];
				pdo_update('yunphp_shop_openapi_shows', $data, array('id' => $id, 'uniacid' => $_W['uniacid']));
				plog('openapi.show.edit', '修改客户联络记录 ID: ' . $id . $updatecontent);
			}
			else 
			{
				$data['createtime'] = time();
				$data['permname'] = trim($_W['username']);
				pdo_insert('yunphp_shop_openapi_shows', $data);
				$id = pdo_insertid();
				plog('commission.level.add', '添加客户联络记录 ID: ' . $id);
			}
			show_json(1, array('url' => webUrl('openapi/shows', array('id' => $recordid))));
		}
		include $this->template();
	}
}