<?php
if (!(defined('IN_IA'))) 
{
	exit('Access Denied');
}
require YUNPHP_SHOP_PLUGIN . 'clubs/core/page_amobile.php';
class Detail_YunphpShopPage extends ClubsAmobilePage 
{
	public function main() 
	{
		global $_W;
		global $_GPC;
		$openid = $_W['openid'];
		$uniacid = $_W['uniacid'];
		$id = intval($_GPC['id']);
		$activities = pdo_fetch('select * from ' . tablename('yunphp_shop_clubs_activities') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
		$thumbs = iunserializer($activities['thumb_url']);
		if (empty($thumbs)) 
		{
			$thumbs = array($activities['thumb']);
		}
		if (!(empty($activities['thumb_first'])) && !(empty($activities['thumb']))) 
		{
			$thumbs = array_merge(array($activities['thumb']), $thumbs);
		}
		$member = m('member')->getMember($openid);
		$share = m('common')->getSysset('share');
		$share['goods_detail_text'] = nl2br($share['goods_detail_text']);
		$activitiesdesc = ((!(empty($activities['description'])) ? $activities['description'] : $activities['subtitle']));
		$_W['shopshare'] = array('title' => (!(empty($activities['share_title'])) ? $activities['share_title'] : $activities['title']), 'imgUrl' => (!(empty($activities['share_icon'])) ? tomedia($activities['share_icon']) : tomedia($activities['thumb'])), 'desc' => (!(empty($activitiesdesc)) ? $activitiesdesc : $_W['shopset']['shop']['name']), 'link' => mobileUrl('clubs/activities/detail', array('id' => $activities['id']), true));
		$com = p('commission');
		if ($com) 
		{
			$cset = $_W['shopset']['commission'];
			if (!(empty($cset))) 
			{
				if (($member['isagent'] == 1) && ($member['status'] == 1)) 
				{
					$_W['shopshare']['link'] = mobileUrl('clubs/activities/detail', array('id' => $goods['id'], 'mid' => $member['id']), true);
				}
				else if (!(empty($_GPC['mid']))) 
				{
					$_W['shopshare']['link'] = mobileUrl('clubs/activities/detail', array('id' => $goods['id'], 'mid' => $_GPC['mid']), true);
				}
			}
		}
		include $this->template();
	}
}