<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}


require YUNPHP_SHOP_PLUGIN . 'events/core/page_amobile.php';
class Index_YunphpShopPage extends EventsAmobilePage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		$cateid = intval($_GPC['cate']);
		
		$advs = pdo_fetchall('select * from ' . tablename('yunphp_shop_events_adv') . ' where uniacid=:uniacid and enabled=1 order by displayorder asc', array(':uniacid' => $_W['uniacid']));
		$advs = set_medias($advs, 'thumb');
		$categorys = pdo_fetchall('select * from ' . tablename('yunphp_shop_category') . ' where uniacid=:uniacid and enabled=1 and level=1 and type=2 order by displayorder asc', array(':uniacid' => $_W['uniacid']));
		if (!empty($cateid)) {
			$category = pdo_fetch('select * from ' . tablename('yunphp_shop_category') . ' where uniacid=:uniacid and enabled=1 and id=:id and type=2 ', array(':uniacid' => $_W['uniacid'], ':id' => $cateid));
		}
		if (!empty($cateid)) {
			$recommand_events = pdo_fetchall('select * from ' . tablename('yunphp_shop_goods') . ' where uniacid=:uniacid and status=1 and type>20 and type<30 and isrecommand=1 and pcate=:pcate order by displayorder asc ', array(':uniacid' => $_W['uniacid'], ':pcate' => $cateid));
		} else {
			$recommand_events = pdo_fetchall('select * from ' . tablename('yunphp_shop_goods') . ' where uniacid=:uniacid and status=1 and type>20 and type<30 and isrecommand=1 order by displayorder asc ', array(':uniacid' => $_W['uniacid']));
		}
		include $this->template();
	}
	
	public function get_list()
	{
		global $_W;
		global $_GPC;
		$advs = pdo_fetchall('select * from ' . tablename('yunphp_shop_events_adv') . ' where uniacid=:uniacid and enabled=1 order by displayorder asc', array(':uniacid' => $_W['uniacid']));
		$advs = set_medias($advs, 'thumb');
		$category2 = pdo_fetchall('select * from ' . tablename('yunphp_shop_category') . ' where uniacid=:uniacid and enabled=1 and ishome=1 and level=2 order by displayorder asc', array(':uniacid' => $_W['uniacid']));
		$recommand_events = pdo_fetchall('select * from ' . tablename('yunphp_shop_goods') . ' where uniacid=:uniacid and status=1 and type>20 and type<30 and isrecommand=1 order by displayorder asc limit 30', array(':uniacid' => $_W['uniacid']));
		include $this->template();
	}
}


?>