DROP TABLE IF EXISTS ims_yunphp_shop_plugin;
CREATE TABLE `ims_yunphp_shop_plugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `displayorder` int(11) DEFAULT '0',
  `identity` varchar(50) DEFAULT '',
  `name` varchar(50) DEFAULT '',
  `version` varchar(10) DEFAULT '',
  `author` varchar(20) DEFAULT '',
  `status` int(11) DEFAULT '0',
  `category` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `desc` text,
  `iscom` tinyint(3) DEFAULT '0',
  `deprecated` tinyint(3) DEFAULT '0',
  `isv2` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_displayorder` (`displayorder`),
  KEY `idx_identity` (`identity`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

INSERT INTO ims_yunphp_shop_plugin VALUES 
('1','1','qiniu','七牛存储','1.0','官方','1','tool','../addons/yunphp_shop/static/images/qiniu.jpg','','1','0','0'),
('2','2','taobao','商品助手','1.0','官方','1','tool','../addons/yunphp_shop/static/images/taobao.jpg','','0','0','0'),
('3','3','commission','分销系统','1.0','官方','1','biz','../addons/yunphp_shop/static/images/commission.jpg','','0','0','0'),
('4','4','poster','超级海报','1.2','官方','1','sale','../addons/yunphp_shop/static/images/poster.jpg','','0','0','0'),
('5','5','verify','O2O核销','1.0','官方','1','biz','../addons/yunphp_shop/static/images/verify.jpg','','1','0','0'),
('6','6','tmessage','会员群发','1.0','官方','1','tool','../addons/yunphp_shop/static/images/tmessage.jpg','','1','0','0'),
('7','7','perm','分权系统','1.0','官方','1','help','../addons/yunphp_shop/static/images/perm.jpg','','1','0','0'),
('8','8','sale','营销宝','1.0','官方','1','sale','../addons/yunphp_shop/static/images/sale.jpg','','1','0','0'),
('9','9','designer','店铺装修V1','1.0','官方','1','help','../addons/yunphp_shop/static/images/designer.jpg','','0','1','0'),
('10','10','creditshop','积分商城','1.0','官方','1','biz','../addons/yunphp_shop/static/images/creditshop.jpg','','0','0','0'),
('11','11','virtual','虚拟物品','1.0','官方','1','biz','../addons/yunphp_shop/static/images/virtual.jpg','','1','0','0'),
('12','11','article','文章营销','1.0','官方','1','help','../addons/yunphp_shop/static/images/article.jpg','','0','0','0'),
('13','13','coupon','超级券','1.0','官方','1','sale','../addons/yunphp_shop/static/images/coupon.jpg','','1','0','0'),
('14','14','postera','活动海报','1.0','官方','1','sale','../addons/yunphp_shop/static/images/postera.jpg','','0','0','0'),
('15','16','system','系统工具','1.0','官方','0','help','../addons/yunphp_shop/static/images/system.jpg','','0','1','0'),
('16','15','diyform','自定表单','1.0','官方','1','help','../addons/yunphp_shop/static/images/diyform.jpg','','0','0','0'),
('17','16','exhelper','快递助手','1.0','官方','1','help','../addons/yunphp_shop/static/images/exhelper.jpg','','0','0','0'),
('18','19','groups','拼团系统','1.0','官方','1','biz','../addons/yunphp_shop/static/images/groups.jpg','','0','0','0'),
('19','20','diypage','店铺装修','2.0','官方','1','help','../addons/yunphp_shop/static/images/designer.jpg','','0','0','0'),
('20','22','globonus','全民股东','1.0','官方','1','biz','../addons/yunphp_shop/static/images/globonus.jpg','','0','0','0'),
('21','23','merch','多商户','1.0','官方','1','biz','../addons/yunphp_shop/static/images/merch.jpg','','0','0','1'),
('22','26','qa','帮助中心','1.0','官方','1','help','../addons/yunphp_shop/static/images/qa.jpg','','0','0','1'),
('24','27','sms','短信提醒','1.0','官方','1','tool','../addons/yunphp_shop/static/images/sms.jpg','','1','0','1'),
('25','29','sign','积分签到','1.0','官方','1','tool','../addons/yunphp_shop/static/images/sign.jpg','','0','0','1'),
('26','30','sns','SNS社区','1.0','官方','1','sale','../addons/yunphp_shop/static/images/sns.jpg','','0','0','1'),
('27','33','wap','全网通','1.0','官方','1','tool','','','1','0','1'),
('28','34','h5app','H5APP','1.0','官方','1','tool','','','1','0','1'),
('29','26','abonus','区域代理','1.0','官方','1','biz','../addons/yunphp_shop/static/images/abonus.jpg','','0','0','1'),
('30','33','printer','小票打印机','1.0','官方','1','tool','','','1','0','1'),
('31','34','bargain','砍价活动','1.0','官方','1','biz','../addons/yunphp_shop/static/images/bargain.jpg','','0','0','1'),
('32','35','task','任务中心','1.0','官方','1','sale','../addons/yunphp_shop/static/images/task.jpg','','0','0','1'),
('33','36','direct','直销系统','1.0','官方','0','biz','../addons/yunphp_shop/static/images/direct.jpg','','0','0','0'),
('34','37','tyiot','物联网系统','1.0','官方','1','biz','../addons/yunphp_shop/static/images/tyiot.jpg','','0','0','0'),
('35','36','cashier','收银台','1.0','官方','1','biz','../addons/yunphp_shop/static/images/cashier.jpg','','0','0','1'),
('36','37','messages','消息群发','1.0','官方','1','tool','../addons/yunphp_shop/static/images/messages.jpg','','0','0','1'),
('37','38','seckill','整点秒杀','1.0','官方','1','biz','../addons/yunphp_shop/static/images/seckill.jpg','','0','0','1'),
('38','38','exchange','兑换中心','1.0','官方','1','biz','../addons/yunphp_shop/static/images/exchange.jpg','','0','0','0'),
('39','37','assistant','微商助手','1.0','官方','1','tool','../addons/yunphp_shop/static/images/assistant.jpg','','0','0','1'),
('40','40','lottery','游戏营销','1.0','官方','1','sale','../addons/yunphp_shop/static/images/lottery.jpg','','0','0','1'),
('41','41','wxcard','微信卡券','1.0','官方','1','sale','','','1','0','1'),
('42','38','quick','快速购买','1.0','官方','1','biz','../addons/yunphp_shop/static/images/quick.jpg','','0','0','0'),
('43','43','mmanage','手机端商家管理中心','1.0','官方','1','tool','../addons/yunphp_shop/static/images/mmanage.jpg','','0','0','1'),
('44','44','yunim','云通信系统','1.0','官方','1','tool','../addons/yunphp_shop/static/images/yunim.jpg','','0','0','1'),
('45','45','events','活动报名','1.0','官方','1','biz','../addons/yunphp_shop/static/images/events.jpg','','0','0','0'),
('46','45','business','企业服务','1.0','官方','1','biz','../addons/yunphp_shop/static/images/business.jpg','','0','0','0');


DROP TABLE IF EXISTS ims_yunphp_shop_polyapi_key;
CREATE TABLE `ims_yunphp_shop_polyapi_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `merchid` int(11) NOT NULL DEFAULT '0',
  `appkey` varchar(200) NOT NULL DEFAULT '',
  `token` varchar(200) NOT NULL DEFAULT '',
  `appsecret` varchar(200) NOT NULL DEFAULT '',
  `createtime` int(11) NOT NULL DEFAULT '0',
  `updatetime` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`) USING BTREE,
  KEY `idx_appkey` (`appkey`) USING BTREE,
  KEY `idx_token` (`token`) USING BTREE,
  KEY `idx_appsecret` (`appsecret`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_poster;
CREATE TABLE `ims_yunphp_shop_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `type` tinyint(3) DEFAULT '0' COMMENT '1 首页 2 小店 3 商城 4 自定义',
  `title` varchar(255) DEFAULT '',
  `bg` varchar(255) DEFAULT '',
  `data` text,
  `keyword` varchar(255) DEFAULT '',
  `times` int(11) DEFAULT '0',
  `follows` int(11) DEFAULT '0',
  `isdefault` tinyint(3) DEFAULT '0',
  `resptitle` varchar(255) DEFAULT '',
  `respthumb` varchar(255) DEFAULT '',
  `createtime` int(11) DEFAULT '0',
  `respdesc` varchar(255) DEFAULT '',
  `respurl` varchar(255) DEFAULT '',
  `waittext` varchar(255) DEFAULT '',
  `oktext` varchar(255) DEFAULT '',
  `subcredit` int(11) DEFAULT '0',
  `submoney` decimal(10,2) DEFAULT '0.00',
  `reccredit` int(11) DEFAULT '0',
  `recmoney` decimal(10,2) DEFAULT '0.00',
  `paytype` tinyint(1) NOT NULL DEFAULT '0',
  `scantext` varchar(255) DEFAULT '',
  `subtext` varchar(255) DEFAULT '',
  `beagent` tinyint(3) DEFAULT '0',
  `bedown` tinyint(3) DEFAULT '0',
  `isopen` tinyint(3) DEFAULT '0',
  `opentext` varchar(255) DEFAULT '',
  `openurl` varchar(255) DEFAULT '',
  `templateid` varchar(255) DEFAULT '',
  `subpaycontent` text,
  `recpaycontent` varchar(255) DEFAULT '',
  `entrytext` varchar(255) DEFAULT '',
  `reccouponid` int(11) DEFAULT '0',
  `reccouponnum` int(11) DEFAULT '0',
  `subcouponid` int(11) DEFAULT '0',
  `subcouponnum` int(11) DEFAULT '0',
  `resptype` tinyint(3) DEFAULT '0',
  `resptext` text,
  `keyword2` varchar(255) DEFAULT '',
  `resptext11` text,
  `reward_totle` varchar(500) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_type` (`type`),
  KEY `idx_times` (`times`),
  KEY `idx_isdefault` (`isdefault`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO ims_yunphp_shop_poster VALUES 
('5','1','1','六点创服','','[]','','0','0','1','','','1494727815','','','','','0','0.00','0','0.00','0','','','0','0','0','','','','','','','0','0','0','0','0','','中国新一代云服务平台','','{\"reccredit_totle\":0,\"recmoney_totle\":0,\"reccouponnum_totle\":0}');


DROP TABLE IF EXISTS ims_yunphp_shop_poster_log;
CREATE TABLE `ims_yunphp_shop_poster_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `posterid` int(11) DEFAULT '0',
  `from_openid` varchar(255) DEFAULT '',
  `subcredit` int(11) DEFAULT '0',
  `submoney` decimal(10,2) DEFAULT '0.00',
  `reccredit` int(11) DEFAULT '0',
  `recmoney` decimal(10,2) DEFAULT '0.00',
  `createtime` int(11) DEFAULT '0',
  `reccouponid` int(11) DEFAULT '0',
  `reccouponnum` int(11) DEFAULT '0',
  `subcouponid` int(11) DEFAULT '0',
  `subcouponnum` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_openid` (`openid`),
  KEY `idx_createtime` (`createtime`),
  KEY `idx_posterid` (`posterid`),
  KEY `idx_from_openid` (`from_openid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_poster_qr;
CREATE TABLE `ims_yunphp_shop_poster_qr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acid` int(10) unsigned NOT NULL,
  `openid` varchar(100) NOT NULL DEFAULT '',
  `type` tinyint(3) DEFAULT '0',
  `sceneid` int(11) DEFAULT '0',
  `mediaid` varchar(255) DEFAULT '',
  `ticket` varchar(250) NOT NULL,
  `url` varchar(80) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `goodsid` int(11) DEFAULT '0',
  `qrimg` varchar(1000) DEFAULT '',
  `scenestr` varchar(255) DEFAULT '',
  `posterid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acid` (`acid`),
  KEY `idx_sceneid` (`sceneid`),
  KEY `idx_type` (`type`),
  KEY `idx_openid` (`openid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_poster_scan;
CREATE TABLE `ims_yunphp_shop_poster_scan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `posterid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `from_openid` varchar(255) DEFAULT '',
  `scantime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_posterid` (`posterid`),
  KEY `idx_scantime` (`scantime`),
  KEY `idx_openid` (`openid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_postera;
CREATE TABLE `ims_yunphp_shop_postera` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `type` tinyint(3) DEFAULT '0' COMMENT '1 首页 2 小店 3 商城 4 自定义',
  `days` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `bg` varchar(255) DEFAULT '',
  `data` text,
  `keyword` varchar(255) DEFAULT '',
  `isdefault` tinyint(3) DEFAULT '0',
  `resptitle` varchar(255) DEFAULT '',
  `respthumb` varchar(255) DEFAULT '',
  `createtime` int(11) DEFAULT '0',
  `respdesc` varchar(255) DEFAULT '',
  `respurl` varchar(255) DEFAULT '',
  `waittext` varchar(255) DEFAULT '',
  `oktext` varchar(255) DEFAULT '',
  `subcredit` int(11) DEFAULT '0',
  `submoney` decimal(10,2) DEFAULT '0.00',
  `reccredit` int(11) DEFAULT '0',
  `recmoney` decimal(10,2) DEFAULT '0.00',
  `scantext` varchar(255) DEFAULT '',
  `subtext` varchar(255) DEFAULT '',
  `beagent` tinyint(3) DEFAULT '0',
  `bedown` tinyint(3) DEFAULT '0',
  `isopen` tinyint(3) DEFAULT '0',
  `opentext` varchar(255) DEFAULT '',
  `openurl` varchar(255) DEFAULT '',
  `paytype` tinyint(1) NOT NULL DEFAULT '0',
  `subpaycontent` text,
  `recpaycontent` varchar(255) DEFAULT '',
  `templateid` varchar(255) DEFAULT '',
  `entrytext` varchar(255) DEFAULT '',
  `reccouponid` int(11) DEFAULT '0',
  `reccouponnum` int(11) DEFAULT '0',
  `subcouponid` int(11) DEFAULT '0',
  `subcouponnum` int(11) DEFAULT '0',
  `timestart` int(11) DEFAULT '0',
  `timeend` int(11) DEFAULT '0',
  `status` tinyint(3) DEFAULT '0',
  `goodsid` int(11) DEFAULT '0',
  `starttext` varchar(255) DEFAULT '',
  `endtext` varchar(255) DEFAULT NULL,
  `resptype` tinyint(3) DEFAULT '0',
  `resptext` text,
  `testflag` tinyint(1) DEFAULT '0',
  `keyword2` varchar(255) DEFAULT '',
  `reward_totle` varchar(500) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_type` (`type`),
  KEY `idx_isdefault` (`isdefault`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_postera_log;
CREATE TABLE `ims_yunphp_shop_postera_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `posterid` int(11) DEFAULT '0',
  `from_openid` varchar(255) DEFAULT '',
  `subcredit` int(11) DEFAULT '0',
  `submoney` decimal(10,2) DEFAULT '0.00',
  `reccredit` int(11) DEFAULT '0',
  `recmoney` decimal(10,2) DEFAULT '0.00',
  `createtime` int(11) DEFAULT '0',
  `reccouponid` int(11) DEFAULT '0',
  `reccouponnum` int(11) DEFAULT '0',
  `subcouponid` int(11) DEFAULT '0',
  `subcouponnum` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_openid` (`openid`),
  KEY `idx_createtime` (`createtime`),
  KEY `idx_posteraid` (`posterid`),
  KEY `idx_from_openid` (`from_openid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_postera_qr;
CREATE TABLE `ims_yunphp_shop_postera_qr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acid` int(10) unsigned NOT NULL,
  `openid` varchar(100) NOT NULL DEFAULT '',
  `posterid` int(11) DEFAULT '0',
  `type` tinyint(3) DEFAULT '0',
  `sceneid` int(11) DEFAULT '0',
  `mediaid` varchar(255) DEFAULT '',
  `ticket` varchar(250) NOT NULL,
  `url` varchar(80) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `goodsid` int(11) DEFAULT '0',
  `qrimg` varchar(1000) DEFAULT '',
  `expire` int(11) DEFAULT '0',
  `endtime` int(11) DEFAULT '0',
  `qrtime` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acid` (`acid`),
  KEY `idx_sceneid` (`sceneid`),
  KEY `idx_type` (`type`),
  KEY `idx_posterid` (`posterid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_qa_adv;
CREATE TABLE `ims_yunphp_shop_qa_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `advname` varchar(50) DEFAULT '',
  `link` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_enabled` (`enabled`),
  KEY `idx_displayorder` (`displayorder`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_qa_category;
CREATE TABLE `ims_yunphp_shop_qa_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `displayorder` tinyint(3) unsigned DEFAULT '0',
  `enabled` tinyint(1) DEFAULT '1',
  `isrecommand` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_displayorder` (`displayorder`),
  KEY `idx_enabled` (`enabled`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_qa_question;
CREATE TABLE `ims_yunphp_shop_qa_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `cate` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `isrecommand` tinyint(3) NOT NULL DEFAULT '0',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `createtime` int(11) NOT NULL DEFAULT '0',
  `lastedittime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_qa_set;
CREATE TABLE `ims_yunphp_shop_qa_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `showmember` tinyint(3) NOT NULL DEFAULT '0',
  `showtype` tinyint(3) NOT NULL DEFAULT '0',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `enter_title` varchar(255) NOT NULL DEFAULT '',
  `enter_img` varchar(255) NOT NULL DEFAULT '',
  `enter_desc` varchar(255) NOT NULL DEFAULT '',
  `share` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_unaicid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_quick;
CREATE TABLE `ims_yunphp_shop_quick` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `datas` mediumtext,
  `cart` tinyint(3) NOT NULL DEFAULT '0',
  `createtime` int(11) DEFAULT NULL,
  `lasttime` int(11) DEFAULT NULL,
  `share_title` varchar(255) DEFAULT NULL,
  `share_desc` varchar(255) DEFAULT NULL,
  `share_icon` varchar(255) DEFAULT NULL,
  `enter_title` varchar(255) DEFAULT NULL,
  `enter_desc` varchar(255) DEFAULT NULL,
  `enter_icon` varchar(255) DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

INSERT INTO ims_yunphp_shop_quick VALUES 
('1','1','外卖模式测试页面','','eyZxdW90O3RlbXBsYXRlJnF1b3Q7OiZxdW90OzAmcXVvdDssJnF1b3Q7c3R5bGUmcXVvdDs6W3smcXVvdDtjYXRlYmcmcXVvdDs6JnF1b3Q7I2Y4ZjhmOCZxdW90OywmcXVvdDtjYXRlY29sb3ImcXVvdDs6JnF1b3Q7IzY2NjY2NiZxdW90OywmcXVvdDtjYXRlYWN0aXZlYmcmcXVvdDs6JnF1b3Q7I2ZmZmZmZiZxdW90OywmcXVvdDtjYXRlYWN0aXZlY29sb3ImcXVvdDs6JnF1b3Q7I2ZmNTU1NSZxdW90OywmcXVvdDtnb29kc2JnJnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7Z29vZHN0aXRsZSZxdW90OzomcXVvdDsjMzMzMzMzJnF1b3Q7LCZxdW90O2dvb2Rzc3VidGl0bGUmcXVvdDs6JnF1b3Q7IzlmOWY5ZiZxdW90OywmcXVvdDtnb29kc3ByaWNlJnF1b3Q7OiZxdW90OyNmZjY1MDAmcXVvdDssJnF1b3Q7Z29vZHNzYWxlcyZxdW90OzomcXVvdDsjODg4ODg4JnF1b3Q7LCZxdW90O2dvb2RzY2FydCZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O3JpZ2h0dGl0bGUmcXVvdDs6JnF1b3Q7IzY2NjY2NiZxdW90OywmcXVvdDtyaWdodHRpdGxlYmcmcXVvdDs6JnF1b3Q7I2Y4ZjhmOCZxdW90OywmcXVvdDtyaWdodHRpdGxlYm9yZGVyJnF1b3Q7OiZxdW90OyNlZmVmZWYmcXVvdDssJnF1b3Q7Zm9vdGVyYmcmcXVvdDs6JnF1b3Q7IzQ3NDc0OSZxdW90OywmcXVvdDtmb290ZXJ0ZXh0JnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7Zm9vdGVyY2FydCZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O2Zvb3RlcmNhcnRpY29uJnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7Zm9vdGVyYnRuJnF1b3Q7OiZxdW90OyNmZjlkNTUmcXVvdDssJnF1b3Q7Zm9vdGVyYnRudGV4dCZxdW90OzomcXVvdDsjZmZmZmZmJnF1b3Q7fSx7JnF1b3Q7c2hvcHN0eWxlJnF1b3Q7OiZxdW90OzEmcXVvdDssJnF1b3Q7bG9nb3N0eWxlJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtub3RpY2UmcXVvdDs6JnF1b3Q7MSZxdW90OywmcXVvdDtub3RpY2VudW0mcXVvdDs6JnF1b3Q7NSZxdW90OywmcXVvdDtzaG9wYmcmcXVvdDs6JnF1b3Q7Li4vYWRkb25zL3l1bnBocF9zaG9wL3BsdWdpbi9xdWljay9zdGF0aWMvaW1hZ2VzL3Nob3AtMS5qcGcmcXVvdDssJnF1b3Q7bmFtZWNvbG9yJnF1b3Q7OiZxdW90OyMwMDAwMDAmcXVvdDssJnF1b3Q7bWVudWJnJnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7bWVudWljb24mcXVvdDs6JnF1b3Q7I2ZmNTU1NSZxdW90OywmcXVvdDttZW51dGV4dCZxdW90OzomcXVvdDsjMTQxNDE0JnF1b3Q7LCZxdW90O25vdGljZWljb24mcXVvdDs6JnF1b3Q7I2YxOWI1OSZxdW90OywmcXVvdDtub3RpY2Vjb2xvciZxdW90OzomcXVvdDsjNjc2YTZjJnF1b3Q7LCZxdW90O2NhdGViZyZxdW90OzomcXVvdDsjZTZlNmU2JnF1b3Q7LCZxdW90O2NhdGVjb2xvciZxdW90OzomcXVvdDsjNjc2YTZjJnF1b3Q7LCZxdW90O2NhdGVhY3RpdmViZyZxdW90OzomcXVvdDsjZmZmZmZmJnF1b3Q7LCZxdW90O2NhdGVhY3RpdmVjb2xvciZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O2dvb2RzYmcmcXVvdDs6JnF1b3Q7I2ZmZmZmZiZxdW90OywmcXVvdDtnb29kc3RpdGlsZSZxdW90OzomcXVvdDsjMzMzMzMzJnF1b3Q7LCZxdW90O2dvb2Rzc3VidGl0aWxlJnF1b3Q7OiZxdW90OyM5ZjlmOWYmcXVvdDssJnF1b3Q7Z29vZHNwcmljZSZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O2dvb2Rzc2FsZXMmcXVvdDs6JnF1b3Q7I2NiY2JjYiZxdW90OywmcXVvdDtnb29kc2NhcnQmcXVvdDs6JnF1b3Q7I2ZmNTU1NSZxdW90OywmcXVvdDtyaWdodHRpdGxlJnF1b3Q7OiZxdW90OyM2NjY2NjYmcXVvdDt9XSwmcXVvdDtkYXRhcyZxdW90OzpbeyZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+WIhue7hOWQjeensCZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtkZXNjJnF1b3Q7OiZxdW90O+WIhue7hOS4gOW+l+S7i+e7jSZxdW90OywmcXVvdDtkYXRhdHlwZSZxdW90OzowLCZxdW90O2NhdGVuYW1lJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtjYXRlaWQmcXVvdDs6MCwmcXVvdDtncm91cG5hbWUmcXVvdDs6JnF1b3Q7JnF1b3Q7LCZxdW90O2dyb3VwaWQmcXVvdDs6MCwmcXVvdDtkYXRhJnF1b3Q7Olt7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fSx7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fSx7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fSx7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fV0sJnF1b3Q7Z29vZHNpZHMmcXVvdDs6WzE5OF0sJnF1b3Q7Z29vZHNzb3J0JnF1b3Q7OjAsJnF1b3Q7Z29vZHNudW0mcXVvdDs6NX0seyZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+WIhue7hOWQjeensOS6jCZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtkZXNjJnF1b3Q7OiZxdW90O+WIhue7hOS6jOeahOS7i+e7jSZxdW90OywmcXVvdDtkYXRhdHlwZSZxdW90OzowLCZxdW90O2NhdGVuYW1lJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtjYXRlaWQmcXVvdDs6MCwmcXVvdDtncm91cG5hbWUmcXVvdDs6JnF1b3Q7JnF1b3Q7LCZxdW90O2dyb3VwaWQmcXVvdDs6MCwmcXVvdDtkYXRhJnF1b3Q7Olt7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fSx7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fSx7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fV0sJnF1b3Q7Z29vZHNpZHMmcXVvdDs6WzE5OF0sJnF1b3Q7Z29vZHNzb3J0JnF1b3Q7OjAsJnF1b3Q7Z29vZHNudW0mcXVvdDs6NX0seyZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+WIhue7hOWQjeensOS4iSZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtkZXNjJnF1b3Q7OiZxdW90O+WIhue7hOS4ieeahOS7i+e7jSZxdW90OywmcXVvdDtkYXRhdHlwZSZxdW90OzowLCZxdW90O2NhdGVuYW1lJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtjYXRlaWQmcXVvdDs6MCwmcXVvdDtncm91cG5hbWUmcXVvdDs6JnF1b3Q7JnF1b3Q7LCZxdW90O2dyb3VwaWQmcXVvdDs6MCwmcXVvdDtkYXRhJnF1b3Q7Olt7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fSx7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fSx7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7fV0sJnF1b3Q7Z29vZHNpZHMmcXVvdDs6WzE5OF0sJnF1b3Q7Z29vZHNzb3J0JnF1b3Q7OjAsJnF1b3Q7Z29vZHNudW0mcXVvdDs6NX1dLCZxdW90O2NhcnRkYXRhJnF1b3Q7OjAsJnF1b3Q7c2hvd2FkdiZxdW90OzomcXVvdDsyJnF1b3Q7LCZxdW90O2FkdnMmcXVvdDs6W3smcXVvdDtpbWd1cmwmcXVvdDs6JnF1b3Q7Li4vYWRkb25zL3l1bnBocF9zaG9wL3BsdWdpbi9xdWljay9zdGF0aWMvaW1hZ2VzL2Jhbm5lci0xLmpwZyZxdW90OywmcXVvdDtsaW5rdXJsJnF1b3Q7OiZxdW90OyZxdW90O30seyZxdW90O2ltZ3VybCZxdW90OzomcXVvdDsuLi9hZGRvbnMveXVucGhwX3Nob3AvcGx1Z2luL3F1aWNrL3N0YXRpYy9pbWFnZXMvYmFubmVyLTIuanBnJnF1b3Q7LCZxdW90O2xpbmt1cmwmcXVvdDs6JnF1b3Q7JnF1b3Q7fV0sJnF1b3Q7bm90aWNlcyZxdW90OzpbeyZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+WFrOWRiuS4gOagh+mimCZxdW90OywmcXVvdDtsaW5rdXJsJnF1b3Q7OiZxdW90OyZxdW90O30seyZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+WFrOWRiuS6jOagh+mimCZxdW90OywmcXVvdDtsaW5rdXJsJnF1b3Q7OiZxdW90OyZxdW90O31dLCZxdW90O3Nob3BtZW51JnF1b3Q7Olt7JnF1b3Q7dGV4dCZxdW90OzomcXVvdDvmjInpkq7lkI3np7AmcXVvdDssJnF1b3Q7aWNvbiZxdW90OzomcXVvdDtpY29uLXNob3AmcXVvdDssJnF1b3Q7bGlua3VybCZxdW90OzomcXVvdDsmcXVvdDt9LHsmcXVvdDt0ZXh0JnF1b3Q7OiZxdW90O+aMiemSruWQjeensCZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90O2ljb24tc2hvcCZxdW90OywmcXVvdDtsaW5rdXJsJnF1b3Q7OiZxdW90OyZxdW90O30seyZxdW90O3RleHQmcXVvdDs6JnF1b3Q75oyJ6ZKu5ZCN56ewJnF1b3Q7LCZxdW90O2ljb24mcXVvdDs6JnF1b3Q7aWNvbi1zaG9wJnF1b3Q7LCZxdW90O2xpbmt1cmwmcXVvdDs6JnF1b3Q7JnF1b3Q7fSx7JnF1b3Q7dGV4dCZxdW90OzomcXVvdDvmjInpkq7lkI3np7AmcXVvdDssJnF1b3Q7aWNvbiZxdW90OzomcXVvdDtpY29uLXNob3AmcXVvdDssJnF1b3Q7bGlua3VybCZxdW90OzomcXVvdDsmcXVvdDt9XSwmcXVvdDtkYXRhSW5kZXgmcXVvdDs6MiwmcXVvdDtzZWxlY3RlZCZxdW90Ozo0fQ==','0','1494218584','1494218584','','','','','','','1'),
('2','1','商城模式演示','','eyZxdW90O3RlbXBsYXRlJnF1b3Q7OjEsJnF1b3Q7c3R5bGUmcXVvdDs6W3smcXVvdDtjYXRlYmcmcXVvdDs6JnF1b3Q7I2Y4ZjhmOCZxdW90OywmcXVvdDtjYXRlY29sb3ImcXVvdDs6JnF1b3Q7IzY2NjY2NiZxdW90OywmcXVvdDtjYXRlYWN0aXZlYmcmcXVvdDs6JnF1b3Q7I2ZmZmZmZiZxdW90OywmcXVvdDtjYXRlYWN0aXZlY29sb3ImcXVvdDs6JnF1b3Q7I2ZmNTU1NSZxdW90OywmcXVvdDtnb29kc2JnJnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7Z29vZHN0aXRsZSZxdW90OzomcXVvdDsjMzMzMzMzJnF1b3Q7LCZxdW90O2dvb2Rzc3VidGl0bGUmcXVvdDs6JnF1b3Q7IzlmOWY5ZiZxdW90OywmcXVvdDtnb29kc3ByaWNlJnF1b3Q7OiZxdW90OyNmZjY1MDAmcXVvdDssJnF1b3Q7Z29vZHNzYWxlcyZxdW90OzomcXVvdDsjODg4ODg4JnF1b3Q7LCZxdW90O2dvb2RzY2FydCZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O3JpZ2h0dGl0bGUmcXVvdDs6JnF1b3Q7IzY2NjY2NiZxdW90OywmcXVvdDtyaWdodHRpdGxlYmcmcXVvdDs6JnF1b3Q7I2Y4ZjhmOCZxdW90OywmcXVvdDtyaWdodHRpdGxlYm9yZGVyJnF1b3Q7OiZxdW90OyNlZmVmZWYmcXVvdDssJnF1b3Q7Zm9vdGVyYmcmcXVvdDs6JnF1b3Q7IzQ3NDc0OSZxdW90OywmcXVvdDtmb290ZXJ0ZXh0JnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7Zm9vdGVyY2FydCZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O2Zvb3RlcmNhcnRpY29uJnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7Zm9vdGVyYnRuJnF1b3Q7OiZxdW90OyNmZjlkNTUmcXVvdDssJnF1b3Q7Zm9vdGVyYnRudGV4dCZxdW90OzomcXVvdDsjZmZmZmZmJnF1b3Q7fSx7JnF1b3Q7c2hvcHN0eWxlJnF1b3Q7OiZxdW90OzEmcXVvdDssJnF1b3Q7bG9nb3N0eWxlJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtub3RpY2UmcXVvdDs6JnF1b3Q7MiZxdW90OywmcXVvdDtub3RpY2VudW0mcXVvdDs6JnF1b3Q7NSZxdW90OywmcXVvdDtzaG9wYmcmcXVvdDs6JnF1b3Q7Li4vYWRkb25zL3l1bnBocF9zaG9wL3BsdWdpbi9xdWljay9zdGF0aWMvaW1hZ2VzL3Nob3AtMS5qcGcmcXVvdDssJnF1b3Q7bmFtZWNvbG9yJnF1b3Q7OiZxdW90OyMwMDAwMDAmcXVvdDssJnF1b3Q7bWVudWJnJnF1b3Q7OiZxdW90OyNmZmZmZmYmcXVvdDssJnF1b3Q7bWVudWljb24mcXVvdDs6JnF1b3Q7I2ZmNTU1NSZxdW90OywmcXVvdDttZW51dGV4dCZxdW90OzomcXVvdDsjMTQxNDE0JnF1b3Q7LCZxdW90O25vdGljZWljb24mcXVvdDs6JnF1b3Q7I2YxOWI1OSZxdW90OywmcXVvdDtub3RpY2Vjb2xvciZxdW90OzomcXVvdDsjNjc2YTZjJnF1b3Q7LCZxdW90O2NhdGViZyZxdW90OzomcXVvdDsjZTZlNmU2JnF1b3Q7LCZxdW90O2NhdGVjb2xvciZxdW90OzomcXVvdDsjNjc2YTZjJnF1b3Q7LCZxdW90O2NhdGVhY3RpdmViZyZxdW90OzomcXVvdDsjZmZmZmZmJnF1b3Q7LCZxdW90O2NhdGVhY3RpdmVjb2xvciZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O2dvb2RzYmcmcXVvdDs6JnF1b3Q7I2ZmZmZmZiZxdW90OywmcXVvdDtnb29kc3RpdGlsZSZxdW90OzomcXVvdDsjMzMzMzMzJnF1b3Q7LCZxdW90O2dvb2Rzc3VidGl0aWxlJnF1b3Q7OiZxdW90OyM5ZjlmOWYmcXVvdDssJnF1b3Q7Z29vZHNwcmljZSZxdW90OzomcXVvdDsjZmY1NTU1JnF1b3Q7LCZxdW90O2dvb2Rzc2FsZXMmcXVvdDs6JnF1b3Q7I2NiY2JjYiZxdW90OywmcXVvdDtnb29kc2NhcnQmcXVvdDs6JnF1b3Q7I2ZmNTU1NSZxdW90OywmcXVvdDtyaWdodHRpdGxlJnF1b3Q7OiZxdW90OyM2NjY2NjYmcXVvdDt9XSwmcXVvdDtkYXRhcyZxdW90OzpbeyZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+WIhue7hOWQjeensCZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtkZXNjJnF1b3Q7OiZxdW90O+i/meaYr+S7i+e7jSZxdW90OywmcXVvdDtkYXRhdHlwZSZxdW90OzowLCZxdW90O2NhdGVuYW1lJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtjYXRlaWQmcXVvdDs6MCwmcXVvdDtncm91cG5hbWUmcXVvdDs6JnF1b3Q7JnF1b3Q7LCZxdW90O2dyb3VwaWQmcXVvdDs6MCwmcXVvdDtkYXRhJnF1b3Q7Olt7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7LCZxdW90O3N1YnRpdGxlJnF1b3Q7OiZxdW90OyZxdW90O31dLCZxdW90O2dvb2RzaWRzJnF1b3Q7OlsxOThdLCZxdW90O2dvb2Rzc29ydCZxdW90OzowLCZxdW90O2dvb2RzbnVtJnF1b3Q7OjV9LHsmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvliIbnu4TlkI3np7AmcXVvdDssJnF1b3Q7aWNvbiZxdW90OzomcXVvdDtpY29uLWVtb2ppJnF1b3Q7LCZxdW90O2Rlc2MmcXVvdDs6JnF1b3Q76L+Z5piv5LuL57uNJnF1b3Q7LCZxdW90O2RhdGF0eXBlJnF1b3Q7OjAsJnF1b3Q7Y2F0ZW5hbWUmcXVvdDs6JnF1b3Q7JnF1b3Q7LCZxdW90O2NhdGVpZCZxdW90OzowLCZxdW90O2dyb3VwbmFtZSZxdW90OzomcXVvdDsmcXVvdDssJnF1b3Q7Z3JvdXBpZCZxdW90OzowLCZxdW90O2RhdGEmcXVvdDs6W3smcXVvdDt0aHVtYiZxdW90OzomcXVvdDtodHRwOi8vbGl1ZGlhbmNodWFuZ2Z1Lm9zcy1jbi1oYW5nemhvdS5hbGl5dW5jcy5jb20vaW1hZ2VzLzEvMjAxNy8wNS9WNVlLSnFJTjUzTGhIR2owdHBIWWx5aFRHNVFxUUwuanBnJnF1b3Q7LCZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+a1i+ivleWVhuWTgSZxdW90OywmcXVvdDtnaWQmcXVvdDs6JnF1b3Q7MTk4JnF1b3Q7LCZxdW90O3ByaWNlJnF1b3Q7OiZxdW90OzEwLjAwJnF1b3Q7LCZxdW90O3RvdGFsJnF1b3Q7OiZxdW90OzEwJnF1b3Q7LCZxdW90O3NhbGVzJnF1b3Q7OiZxdW90OzAmcXVvdDssJnF1b3Q7c3VidGl0bGUmcXVvdDs6JnF1b3Q7JnF1b3Q7fV0sJnF1b3Q7Z29vZHNpZHMmcXVvdDs6WzE5OF0sJnF1b3Q7Z29vZHNzb3J0JnF1b3Q7OjAsJnF1b3Q7Z29vZHNudW0mcXVvdDs6NX0seyZxdW90O3RpdGxlJnF1b3Q7OiZxdW90O+WIhue7hOWQjeensCZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90O2ljb24tbGlrZSZxdW90OywmcXVvdDtkZXNjJnF1b3Q7OiZxdW90O+i/meaYr+S7i+e7jSZxdW90OywmcXVvdDtkYXRhdHlwZSZxdW90OzowLCZxdW90O2NhdGVuYW1lJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtjYXRlaWQmcXVvdDs6MCwmcXVvdDtncm91cG5hbWUmcXVvdDs6JnF1b3Q7JnF1b3Q7LCZxdW90O2dyb3VwaWQmcXVvdDs6MCwmcXVvdDtkYXRhJnF1b3Q7Olt7JnF1b3Q7dGh1bWImcXVvdDs6JnF1b3Q7aHR0cDovL2xpdWRpYW5jaHVhbmdmdS5vc3MtY24taGFuZ3pob3UuYWxpeXVuY3MuY29tL2ltYWdlcy8xLzIwMTcvMDUvVjVZS0pxSU41M0xoSEdqMHRwSFlseWhURzVRcVFMLmpwZyZxdW90OywmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvmtYvor5XllYblk4EmcXVvdDssJnF1b3Q7Z2lkJnF1b3Q7OiZxdW90OzE5OCZxdW90OywmcXVvdDtwcmljZSZxdW90OzomcXVvdDsxMC4wMCZxdW90OywmcXVvdDt0b3RhbCZxdW90OzomcXVvdDsxMCZxdW90OywmcXVvdDtzYWxlcyZxdW90OzomcXVvdDswJnF1b3Q7LCZxdW90O3N1YnRpdGxlJnF1b3Q7OiZxdW90OyZxdW90O31dLCZxdW90O2dvb2RzaWRzJnF1b3Q7OlsxOThdLCZxdW90O2dvb2Rzc29ydCZxdW90OzowLCZxdW90O2dvb2RzbnVtJnF1b3Q7OjV9LHsmcXVvdDt0aXRsZSZxdW90OzomcXVvdDvliIbnu4TlkI3np7AmcXVvdDssJnF1b3Q7aWNvbiZxdW90OzomcXVvdDsmcXVvdDssJnF1b3Q7ZGVzYyZxdW90OzomcXVvdDvov5nmmK/ku4vnu40mcXVvdDssJnF1b3Q7ZGF0YXR5cGUmcXVvdDs6MCwmcXVvdDtjYXRlbmFtZSZxdW90OzomcXVvdDsmcXVvdDssJnF1b3Q7Y2F0ZWlkJnF1b3Q7OjAsJnF1b3Q7Z3JvdXBuYW1lJnF1b3Q7OiZxdW90OyZxdW90OywmcXVvdDtncm91cGlkJnF1b3Q7OjAsJnF1b3Q7ZGF0YSZxdW90OzpbeyZxdW90O3RodW1iJnF1b3Q7OiZxdW90O2h0dHA6Ly9saXVkaWFuY2h1YW5nZnUub3NzLWNuLWhhbmd6aG91LmFsaXl1bmNzLmNvbS9pbWFnZXMvMS8yMDE3LzA1L1Y1WUtKcUlONTNMaEhHajB0cEhZbHloVEc1UXFRTC5qcGcmcXVvdDssJnF1b3Q7dGl0bGUmcXVvdDs6JnF1b3Q75rWL6K+V5ZWG5ZOBJnF1b3Q7LCZxdW90O2dpZCZxdW90OzomcXVvdDsxOTgmcXVvdDssJnF1b3Q7cHJpY2UmcXVvdDs6JnF1b3Q7MTAuMDAmcXVvdDssJnF1b3Q7dG90YWwmcXVvdDs6JnF1b3Q7MTAmcXVvdDssJnF1b3Q7c2FsZXMmcXVvdDs6JnF1b3Q7MCZxdW90OywmcXVvdDtzdWJ0aXRsZSZxdW90OzomcXVvdDsmcXVvdDt9XSwmcXVvdDtnb29kc2lkcyZxdW90OzpbMTk4XSwmcXVvdDtnb29kc3NvcnQmcXVvdDs6MCwmcXVvdDtnb29kc251bSZxdW90Ozo1fV0sJnF1b3Q7Y2FydGRhdGEmcXVvdDs6MCwmcXVvdDtzaG93YWR2JnF1b3Q7OjAsJnF1b3Q7YWR2cyZxdW90OzpbeyZxdW90O2ltZ3VybCZxdW90OzomcXVvdDsuLi9hZGRvbnMveXVucGhwX3Nob3AvcGx1Z2luL3F1aWNrL3N0YXRpYy9pbWFnZXMvYmFubmVyLTEuanBnJnF1b3Q7LCZxdW90O2xpbmt1cmwmcXVvdDs6JnF1b3Q7JnF1b3Q7fSx7JnF1b3Q7aW1ndXJsJnF1b3Q7OiZxdW90Oy4uL2FkZG9ucy95dW5waHBfc2hvcC9wbHVnaW4vcXVpY2svc3RhdGljL2ltYWdlcy9iYW5uZXItMi5qcGcmcXVvdDssJnF1b3Q7bGlua3VybCZxdW90OzomcXVvdDsmcXVvdDt9XSwmcXVvdDtub3RpY2VzJnF1b3Q7Olt7JnF1b3Q7dGl0bGUmcXVvdDs6JnF1b3Q75YWs5ZGK5LiA5qCH6aKYJnF1b3Q7LCZxdW90O2xpbmt1cmwmcXVvdDs6JnF1b3Q7JnF1b3Q7fSx7JnF1b3Q7dGl0bGUmcXVvdDs6JnF1b3Q75YWs5ZGK5LqM5qCH6aKYJnF1b3Q7LCZxdW90O2xpbmt1cmwmcXVvdDs6JnF1b3Q7JnF1b3Q7fV0sJnF1b3Q7c2hvcG1lbnUmcXVvdDs6W3smcXVvdDt0ZXh0JnF1b3Q7OiZxdW90O+aMiemSruWQjeensCZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90O2ljb24tc2hvcCZxdW90OywmcXVvdDtsaW5rdXJsJnF1b3Q7OiZxdW90OyZxdW90O30seyZxdW90O3RleHQmcXVvdDs6JnF1b3Q75oyJ6ZKu5ZCN56ewJnF1b3Q7LCZxdW90O2ljb24mcXVvdDs6JnF1b3Q7aWNvbi1zaG9wJnF1b3Q7LCZxdW90O2xpbmt1cmwmcXVvdDs6JnF1b3Q7JnF1b3Q7fSx7JnF1b3Q7dGV4dCZxdW90OzomcXVvdDvmjInpkq7lkI3np7AmcXVvdDssJnF1b3Q7aWNvbiZxdW90OzomcXVvdDtpY29uLXNob3AmcXVvdDssJnF1b3Q7bGlua3VybCZxdW90OzomcXVvdDsmcXVvdDt9LHsmcXVvdDt0ZXh0JnF1b3Q7OiZxdW90O+aMiemSruWQjeensCZxdW90OywmcXVvdDtpY29uJnF1b3Q7OiZxdW90O2ljb24tc2hvcCZxdW90OywmcXVvdDtsaW5rdXJsJnF1b3Q7OiZxdW90OyZxdW90O31dLCZxdW90O2RhdGFJbmRleCZxdW90OzowLCZxdW90O3NlbGVjdGVkJnF1b3Q7OjB9','0','1494218728','1494218742','','','','','','','1');


DROP TABLE IF EXISTS ims_yunphp_shop_quick_adv;
CREATE TABLE `ims_yunphp_shop_quick_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `merchid` int(11) NOT NULL DEFAULT '0',
  `advname` varchar(50) DEFAULT '',
  `link` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`) USING BTREE,
  KEY `idx_enabled` (`enabled`) USING BTREE,
  KEY `idx_displayorder` (`displayorder`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_quick_cart;
CREATE TABLE `ims_yunphp_shop_quick_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `quickid` int(11) NOT NULL DEFAULT '0',
  `openid` varchar(100) DEFAULT '',
  `goodsid` int(11) DEFAULT '0',
  `total` int(11) DEFAULT '0',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  `deleted` tinyint(1) DEFAULT '0',
  `optionid` int(11) DEFAULT '0',
  `createtime` int(11) DEFAULT '0',
  `diyformdataid` int(11) DEFAULT NULL,
  `diyformdata` text,
  `diyformfields` text,
  `diyformid` int(11) DEFAULT '0',
  `selected` tinyint(1) DEFAULT '1',
  `merchid` int(11) DEFAULT '0',
  `selectedadd` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`) USING BTREE,
  KEY `idx_goodsid` (`goodsid`) USING BTREE,
  KEY `idx_openid` (`openid`) USING BTREE,
  KEY `idx_deleted` (`deleted`) USING BTREE,
  KEY `idx_merchid` (`merchid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_receive;
CREATE TABLE `ims_yunphp_shop_receive` (
  `id` int(11) NOT NULL,
  `uniacid` int(11) DEFAULT NULL,
  `enough` decimal(10,2) DEFAULT '0.00',
  `couponid` int(11) DEFAULT '0',
  `starttime` int(11) DEFAULT '0',
  `endtime` int(11) DEFAULT '0',
  `sendnum` int(11) DEFAULT '1',
  `num` int(11) DEFAULT '0',
  `sendpoint` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `merchid` int(11) NOT NULL DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `taskname` varchar(255) NOT NULL,
  `reward_data` text NOT NULL,
  `tasktype` tinyint(1) NOT NULL DEFAULT '0',
  `createtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_unaicid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;



DROP TABLE IF EXISTS ims_yunphp_shop_receive_log;
CREATE TABLE `ims_yunphp_shop_receive_log` (
  `id` int(11) NOT NULL,
  `uniacid` int(11) DEFAULT NULL,
  `openid` varchar(50) DEFAULT NULL,
  `receiveid` int(11) DEFAULT '0',
  `couponid` int(11) DEFAULT '0',
  `sendnum` int(11) DEFAULT '0',
  `tasktype` tinyint(1) DEFAULT '0',
  `orderid` int(11) DEFAULT '0',
  `parentorderid` int(11) DEFAULT '0',
  `createtime` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `sendpoint` tinyint(1) DEFAULT '0',
  `subdata` text NOT NULL,
  `recdata` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_unaicid` (`uniacid`),
  KEY `idx_openid` (`openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_refund_address;
CREATE TABLE `ims_yunphp_shop_refund_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `title` varchar(20) DEFAULT '',
  `name` varchar(20) DEFAULT '',
  `tel` varchar(20) DEFAULT '',
  `mobile` varchar(11) DEFAULT '',
  `province` varchar(30) DEFAULT '',
  `city` varchar(30) DEFAULT '',
  `area` varchar(30) DEFAULT '',
  `address` varchar(300) DEFAULT '',
  `isdefault` tinyint(1) DEFAULT '0',
  `zipcode` varchar(255) DEFAULT '',
  `content` text,
  `deleted` tinyint(1) DEFAULT '0',
  `openid` varchar(50) DEFAULT '0',
  `merchid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_openid` (`openid`),
  KEY `idx_isdefault` (`isdefault`),
  KEY `idx_deleted` (`deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sale_coupon;
CREATE TABLE `ims_yunphp_shop_sale_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  `type` tinyint(3) DEFAULT '0',
  `ckey` decimal(10,2) DEFAULT '0.00',
  `cvalue` decimal(10,2) DEFAULT '0.00',
  `nums` int(11) DEFAULT '0',
  `createtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sale_coupon_data;
CREATE TABLE `ims_yunphp_shop_sale_coupon_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `couponid` int(11) DEFAULT '0',
  `gettime` int(11) DEFAULT '0',
  `gettype` tinyint(3) DEFAULT '0',
  `usedtime` int(11) DEFAULT '0',
  `orderid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_couponid` (`couponid`),
  KEY `idx_gettime` (`gettime`),
  KEY `idx_gettype` (`gettype`),
  KEY `idx_usedtime` (`usedtime`),
  KEY `idx_orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_saler;
CREATE TABLE `ims_yunphp_shop_saler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storeid` int(11) DEFAULT '0',
  `uniacid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `status` tinyint(3) DEFAULT '0',
  `salername` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_storeid` (`storeid`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO ims_yunphp_shop_saler VALUES 
('4','0','1','oqryZ04mcIMHZ5YzNoByO-U9lNBw','1','afk');


DROP TABLE IF EXISTS ims_yunphp_shop_seckill_adv;
CREATE TABLE `ims_yunphp_shop_seckill_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `advname` varchar(50) DEFAULT '',
  `link` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_displayorder` (`displayorder`) USING BTREE,
  KEY `idx_enabled` (`enabled`) USING BTREE,
  KEY `idx_uniacid` (`uniacid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_seckill_category;
CREATE TABLE `ims_yunphp_shop_seckill_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_seckill_task;
CREATE TABLE `ims_yunphp_shop_seckill_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `cateid` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `enabled` tinyint(3) DEFAULT '0',
  `page_title` varchar(255) DEFAULT '',
  `share_title` varchar(255) DEFAULT '',
  `share_desc` varchar(255) DEFAULT '',
  `share_icon` varchar(255) DEFAULT '',
  `tag` varchar(10) DEFAULT '',
  `closesec` int(11) DEFAULT '0',
  `oldshow` tinyint(3) DEFAULT '0',
  `times` text,
  `createtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`) USING BTREE,
  KEY `idx_status` (`enabled`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_seckill_task_goods;
CREATE TABLE `ims_yunphp_shop_seckill_task_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `taskid` int(11) DEFAULT '0',
  `roomid` int(11) DEFAULT '0',
  `timeid` int(11) DEFAULT '0',
  `goodsid` int(11) DEFAULT '0',
  `optionid` int(11) DEFAULT '0',
  `price` decimal(10,2) DEFAULT '0.00',
  `total` int(11) DEFAULT '0',
  `maxbuy` int(11) DEFAULT '0',
  `totalmaxbuy` int(11) DEFAULT '0',
  `commission1` decimal(10,2) DEFAULT '0.00',
  `commission2` decimal(10,2) DEFAULT '0.00',
  `commission3` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`) USING BTREE,
  KEY `idx_goodsid` (`goodsid`) USING BTREE,
  KEY `idx_optionid` (`optionid`) USING BTREE,
  KEY `idx_displayorder` (`displayorder`) USING BTREE,
  KEY `idx_taskid` (`taskid`) USING BTREE,
  KEY `idx_roomid` (`roomid`) USING BTREE,
  KEY `idx_time` (`timeid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;



DROP TABLE IF EXISTS ims_yunphp_shop_seckill_task_room;
CREATE TABLE `ims_yunphp_shop_seckill_task_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `taskid` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `enabled` tinyint(3) DEFAULT '0',
  `page_title` varchar(255) DEFAULT '',
  `share_title` varchar(255) DEFAULT '',
  `share_desc` varchar(255) DEFAULT '',
  `share_icon` varchar(255) DEFAULT '',
  `oldshow` tinyint(3) DEFAULT '0',
  `tag` varchar(10) DEFAULT '',
  `createtime` int(11) DEFAULT '0',
  `diypage` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_taskid` (`taskid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_seckill_task_time;
CREATE TABLE `ims_yunphp_shop_seckill_task_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `taskid` int(11) DEFAULT '0',
  `time` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;



DROP TABLE IF EXISTS ims_yunphp_shop_sign_records;
CREATE TABLE `ims_yunphp_shop_sign_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `openid` varchar(50) NOT NULL DEFAULT '',
  `credit` int(11) NOT NULL DEFAULT '0',
  `log` varchar(255) DEFAULT '',
  `type` tinyint(3) NOT NULL DEFAULT '0',
  `day` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_time` (`time`),
  KEY `idx_type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=602 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sign_set;
CREATE TABLE `ims_yunphp_shop_sign_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `iscenter` tinyint(3) NOT NULL DEFAULT '0',
  `iscreditshop` tinyint(3) NOT NULL DEFAULT '0',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `desc` varchar(255) NOT NULL DEFAULT '',
  `isopen` tinyint(3) NOT NULL DEFAULT '0',
  `signold` tinyint(3) NOT NULL DEFAULT '0',
  `signold_price` int(11) NOT NULL DEFAULT '0',
  `signold_type` tinyint(3) NOT NULL DEFAULT '0',
  `textsign` varchar(255) NOT NULL DEFAULT '',
  `textsignold` varchar(255) NOT NULL DEFAULT '',
  `textsigned` varchar(255) NOT NULL DEFAULT '',
  `textsignforget` varchar(255) NOT NULL DEFAULT '',
  `maincolor` varchar(20) NOT NULL DEFAULT '',
  `cycle` tinyint(3) NOT NULL DEFAULT '0',
  `reward_default_first` int(11) NOT NULL DEFAULT '0',
  `reward_default_day` int(11) NOT NULL DEFAULT '0',
  `reword_order` text NOT NULL,
  `reword_sum` text NOT NULL,
  `reword_special` text NOT NULL,
  `sign_rule` text NOT NULL,
  `share` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sign_user;
CREATE TABLE `ims_yunphp_shop_sign_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `openid` varchar(255) NOT NULL DEFAULT '',
  `order` int(11) NOT NULL DEFAULT '0',
  `orderday` int(11) NOT NULL DEFAULT '0',
  `sum` int(11) NOT NULL DEFAULT '0',
  `signdate` varchar(10) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=146 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sms;
CREATE TABLE `ims_yunphp_shop_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `template` tinyint(3) NOT NULL DEFAULT '0',
  `smstplid` varchar(255) NOT NULL DEFAULT '',
  `smssign` varchar(255) NOT NULL DEFAULT '',
  `content` varchar(100) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO ims_yunphp_shop_sms VALUES 
('6','1','用户注册','dayu','1','SMS_65935320','六点创服','','a:2:{i:0;a:2:{s:9:\"data_temp\";s:4:\"code\";s:9:\"data_shop\";s:11:\"[验证码]\";}i:1;a:2:{s:9:\"data_temp\";s:7:\"product\";s:9:\"data_shop\";s:14:\"[商城名称]\";}}','1'),
('7','1','身份认证','dayu','1','SMS_65935324','六点创服','','a:2:{i:0;a:2:{s:9:\"data_temp\";s:4:\"code\";s:9:\"data_shop\";s:11:\"[验证码]\";}i:1;a:2:{s:9:\"data_temp\";s:7:\"product\";s:9:\"data_shop\";s:14:\"[商城名称]\";}}','1'),
('8','1','修改密码','dayu','1','SMS_65935318','六点创服','','a:2:{i:0;a:2:{s:9:\"data_temp\";s:4:\"code\";s:9:\"data_shop\";s:11:\"[验证码]\";}i:1;a:2:{s:9:\"data_temp\";s:7:\"product\";s:9:\"data_shop\";s:14:\"[商城名称]\";}}','1');


DROP TABLE IF EXISTS ims_yunphp_shop_sms_set;
CREATE TABLE `ims_yunphp_shop_sms_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `juhe` tinyint(3) NOT NULL DEFAULT '0',
  `juhe_key` varchar(255) NOT NULL DEFAULT '',
  `dayu` tinyint(3) NOT NULL DEFAULT '0',
  `dayu_key` varchar(255) NOT NULL DEFAULT '',
  `dayu_secret` varchar(255) NOT NULL DEFAULT '',
  `emay` tinyint(3) NOT NULL DEFAULT '0',
  `emay_url` varchar(255) NOT NULL DEFAULT '',
  `emay_sn` varchar(255) NOT NULL DEFAULT '',
  `emay_pw` varchar(255) NOT NULL DEFAULT '',
  `emay_sk` varchar(255) NOT NULL DEFAULT '',
  `emay_phost` varchar(255) NOT NULL DEFAULT '',
  `emay_pport` int(11) NOT NULL DEFAULT '0',
  `emay_puser` varchar(255) NOT NULL DEFAULT '',
  `emay_ppw` varchar(255) NOT NULL DEFAULT '',
  `emay_out` int(11) NOT NULL DEFAULT '0',
  `emay_outresp` int(11) NOT NULL DEFAULT '30',
  `emay_warn` decimal(10,2) NOT NULL DEFAULT '0.00',
  `emay_mobile` varchar(11) NOT NULL DEFAULT '',
  `emay_warn_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO ims_yunphp_shop_sms_set VALUES 
('3','1','0','','1','23807633','0c8a5ec10364a0c0dbf6216ec9e65aff','0','','','','','','0','','','0','30','0.00','0','0');


DROP TABLE IF EXISTS ims_yunphp_shop_sns_adv;
CREATE TABLE `ims_yunphp_shop_sns_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `advname` varchar(50) DEFAULT '',
  `link` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_enabled` (`enabled`),
  KEY `idx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_board;
CREATE TABLE `ims_yunphp_shop_sns_board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `cid` int(11) DEFAULT '0',
  `title` varchar(50) DEFAULT '',
  `logo` varchar(255) DEFAULT '',
  `desc` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  `showgroups` text,
  `showlevels` text,
  `postgroups` text,
  `postlevels` text,
  `showagentlevels` text,
  `postagentlevels` text,
  `postcredit` int(11) DEFAULT '0',
  `replycredit` int(11) DEFAULT '0',
  `bestcredit` int(11) DEFAULT '0',
  `bestboardcredit` int(11) DEFAULT '0',
  `notagent` tinyint(3) DEFAULT '0',
  `notagentpost` tinyint(3) DEFAULT '0',
  `topcredit` int(11) DEFAULT '0',
  `topboardcredit` int(11) DEFAULT '0',
  `status` tinyint(3) DEFAULT '0',
  `noimage` tinyint(3) DEFAULT '0',
  `novoice` tinyint(3) DEFAULT '0',
  `needfollow` tinyint(3) DEFAULT '0',
  `needpostfollow` tinyint(3) DEFAULT '0',
  `share_title` varchar(255) DEFAULT '',
  `share_icon` varchar(255) DEFAULT '',
  `share_desc` varchar(255) DEFAULT '',
  `keyword` varchar(255) DEFAULT '',
  `isrecommand` tinyint(3) DEFAULT '0',
  `banner` varchar(255) DEFAULT '',
  `needcheck` tinyint(3) DEFAULT '0',
  `needcheckmanager` tinyint(3) DEFAULT '0',
  `needcheckreply` int(11) DEFAULT '0',
  `needcheckreplymanager` int(11) DEFAULT '0',
  `showsnslevels` text,
  `postsnslevels` text,
  `showpartnerlevels` text,
  `postpartnerlevels` text,
  `notpartner` tinyint(3) DEFAULT '0',
  `notpartnerpost` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_enabled` (`enabled`),
  KEY `idx_displayorder` (`displayorder`),
  KEY `idx_cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_board_follow;
CREATE TABLE `ims_yunphp_shop_sns_board_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `bid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT NULL,
  `createtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_bid` (`bid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_category;
CREATE TABLE `ims_yunphp_shop_sns_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `displayorder` tinyint(3) unsigned DEFAULT '0',
  `enabled` tinyint(1) DEFAULT '1',
  `advimg` varchar(255) DEFAULT '',
  `advurl` varchar(500) DEFAULT '',
  `isrecommand` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_enabled` (`enabled`),
  KEY `idx_isrecommand` (`isrecommand`),
  KEY `idx_displayorder` (`displayorder`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_complain;
CREATE TABLE `ims_yunphp_shop_sns_complain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(3) NOT NULL,
  `postsid` int(11) NOT NULL DEFAULT '0',
  `defendant` varchar(255) NOT NULL DEFAULT '0',
  `complainant` varchar(255) NOT NULL DEFAULT '0',
  `complaint_type` int(10) NOT NULL DEFAULT '0',
  `complaint_text` text NOT NULL,
  `images` text NOT NULL,
  `createtime` int(11) NOT NULL DEFAULT '0',
  `checkedtime` int(11) NOT NULL DEFAULT '0',
  `checked` tinyint(3) NOT NULL DEFAULT '0',
  `checked_note` varchar(255) NOT NULL,
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_complaincate;
CREATE TABLE `ims_yunphp_shop_sns_complaincate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_level;
CREATE TABLE `ims_yunphp_shop_sns_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `levelname` varchar(255) DEFAULT '',
  `credit` int(11) DEFAULT '0',
  `enabled` tinyint(3) DEFAULT '0',
  `post` int(11) DEFAULT '0',
  `color` varchar(255) DEFAULT '',
  `bg` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_enabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_like;
CREATE TABLE `ims_yunphp_shop_sns_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `pid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_manage;
CREATE TABLE `ims_yunphp_shop_sns_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `bid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `enabled` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_bid` (`bid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_member;
CREATE TABLE `ims_yunphp_shop_sns_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `createtime` int(11) DEFAULT '0',
  `credit` int(11) DEFAULT '0',
  `sign` varchar(255) DEFAULT '',
  `isblack` tinyint(3) DEFAULT '0',
  `notupgrade` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sns_post;
CREATE TABLE `ims_yunphp_shop_sns_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `bid` int(11) DEFAULT '0',
  `pid` int(11) DEFAULT '0',
  `rpid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT '',
  `avatar` varchar(255) DEFAULT '',
  `nickname` varchar(255) DEFAULT '',
  `title` varchar(50) DEFAULT '',
  `content` text,
  `images` text,
  `voice` varchar(255) DEFAULT NULL,
  `createtime` int(11) DEFAULT '0',
  `replytime` int(11) DEFAULT '0',
  `credit` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `islock` tinyint(1) DEFAULT '0',
  `istop` tinyint(1) DEFAULT '0',
  `isboardtop` tinyint(1) DEFAULT '0',
  `isbest` tinyint(1) DEFAULT '0',
  `isboardbest` tinyint(3) DEFAULT '0',
  `deleted` tinyint(3) DEFAULT '0',
  `deletedtime` int(11) DEFAULT '0',
  `checked` tinyint(3) DEFAULT NULL,
  `checktime` int(11) DEFAULT '0',
  `isadmin` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_bid` (`bid`),
  KEY `idx_pid` (`pid`),
  KEY `idx_createtime` (`createtime`),
  KEY `idx_islock` (`islock`),
  KEY `idx_istop` (`istop`),
  KEY `idx_isboardtop` (`isboardtop`),
  KEY `idx_isbest` (`isbest`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_deletetime` (`deletedtime`),
  KEY `idx_checked` (`checked`),
  KEY `idx_rpid` (`rpid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_store;
CREATE TABLE `ims_yunphp_shop_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `storename` varchar(255) DEFAULT '',
  `address` varchar(255) DEFAULT '',
  `tel` varchar(255) DEFAULT '',
  `lat` varchar(255) DEFAULT '',
  `lng` varchar(255) DEFAULT '',
  `status` tinyint(3) DEFAULT '0',
  `realname` varchar(255) DEFAULT '',
  `mobile` varchar(255) DEFAULT '',
  `fetchtime` varchar(255) DEFAULT '',
  `type` tinyint(1) DEFAULT '0',
  `logo` varchar(255) DEFAULT '',
  `saletime` varchar(255) DEFAULT '',
  `desc` text,
  `displayorder` int(11) DEFAULT '0',
  `order_printer` varchar(500) DEFAULT '',
  `order_template` int(11) DEFAULT '0',
  `ordertype` varchar(500) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_sysset;
CREATE TABLE `ims_yunphp_shop_sysset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `sets` longtext,
  `plugins` longtext,
  `sec` text,
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO ims_yunphp_shop_sysset VALUES 
('11','1','a:7:{s:4:\"shop\";a:17:{s:4:\"name\";s:12:\"六点创服\";s:4:\"logo\";s:103:\"http://liudianchuangfu.oss-cn-hangzhou.aliyuncs.com/images/1/2017/05/k5Ll5uud5DsDlI5fiGZLA5uLWG5CIP.png\";s:11:\"description\";s:118:\"中小企业一站式云服务平台，为中小企业省钱、为政府省心，助力中小企业创业一帆风顺~\";s:3:\"img\";s:0:\"\";s:7:\"signimg\";s:0:\"\";s:7:\"getinfo\";s:1:\"1\";s:7:\"saleout\";s:0:\"\";s:7:\"loading\";s:0:\"\";s:7:\"diycode\";s:0:\"\";s:10:\"order_flag\";s:1:\"0\";s:6:\"funbar\";s:1:\"0\";s:13:\"yingyong_flag\";s:1:\"0\";s:8:\"catlevel\";s:1:\"2\";s:7:\"catshow\";s:1:\"0\";s:9:\"catadvimg\";s:0:\"\";s:9:\"catadvurl\";s:0:\"\";s:5:\"style\";s:7:\"default\";}s:5:\"trade\";a:30:{s:12:\"set_realname\";s:1:\"0\";s:10:\"set_mobile\";s:1:\"0\";s:10:\"closeorder\";s:0:\"\";s:14:\"willcloseorder\";s:0:\"\";s:7:\"receive\";s:0:\"\";s:10:\"refunddays\";s:0:\"\";s:13:\"refundcontent\";s:0:\"\";s:10:\"credittext\";s:12:\"点金钱包\";s:9:\"moneytext\";s:6:\"余额\";s:13:\"closerecharge\";s:1:\"0\";s:5:\"money\";s:0:\"\";s:6:\"credit\";s:0:\"\";s:13:\"minimumcharge\";d:0;s:8:\"withdraw\";s:1:\"0\";s:13:\"withdrawmoney\";s:0:\"\";s:14:\"withdrawcharge\";s:0:\"\";s:13:\"withdrawbegin\";d:0;s:11:\"withdrawend\";d:0;s:9:\"maxcredit\";s:1:\"0\";s:12:\"closecomment\";s:1:\"0\";s:16:\"closecommentshow\";s:1:\"0\";s:14:\"commentchecked\";s:1:\"0\";s:12:\"shareaddress\";s:1:\"1\";s:10:\"istimetext\";s:9:\"限时购\";s:15:\"nodispatchareas\";s:7:\"s:0:\"\";\";s:20:\"nodispatchareas_code\";s:7:\"s:0:\"\";\";s:15:\"willclosecoupon\";s:0:\"\";s:18:\"withdrawcashweixin\";i:0;s:18:\"withdrawcashalipay\";i:0;s:16:\"withdrawcashcard\";i:0;}s:3:\"wap\";a:19:{s:4:\"open\";i:1;s:6:\"pcopen\";s:1:\"0\";s:7:\"inh5app\";s:1:\"0\";s:8:\"mustbind\";s:1:\"0\";s:5:\"style\";s:8:\"default3\";s:5:\"color\";s:7:\"#43afcf\";s:2:\"bg\";s:54:\"../addons/yunphp_shop/template/account/default3/bg.jpg\";s:10:\"smsimgcode\";s:1:\"1\";s:7:\"sms_reg\";s:1:\"6\";s:10:\"sms_forget\";s:1:\"7\";s:13:\"sms_changepwd\";s:1:\"8\";s:8:\"sms_bind\";s:1:\"7\";s:13:\"headerbgcolor\";s:0:\"\";s:11:\"headercolor\";s:0:\"\";s:15:\"headericoncolor\";s:0:\"\";s:9:\"statusbar\";s:1:\"0\";s:7:\"loginbg\";N;s:5:\"regbg\";N;s:3:\"sns\";a:2:{s:2:\"qq\";i:1;s:2:\"wx\";i:1;}}s:8:\"category\";a:5:{s:5:\"level\";s:1:\"2\";s:4:\"show\";s:1:\"0\";s:5:\"style\";s:1:\"0\";s:6:\"advimg\";s:0:\"\";s:6:\"advurl\";s:0:\"\";}s:3:\"pay\";a:13:{s:6:\"weixin\";i:0;s:10:\"weixin_sub\";i:0;s:10:\"weixin_jie\";i:0;s:14:\"weixin_jie_sub\";i:0;s:6:\"alipay\";i:0;s:6:\"credit\";i:1;s:4:\"cash\";i:0;s:10:\"app_wechat\";i:0;s:10:\"app_alipay\";i:0;s:10:\"weixin_wft\";i:0;s:11:\"weixin_zxyh\";i:0;s:14:\"weixin_zxyhapp\";i:0;s:7:\"paytype\";a:3:{s:10:\"commission\";s:1:\"0\";s:8:\"withdraw\";s:1:\"0\";s:7:\"redpack\";s:1:\"0\";}}s:8:\"template\";a:2:{s:5:\"style\";s:7:\"default\";s:11:\"detail_temp\";s:1:\"1\";}s:11:\"area_config\";a:5:{s:2:\"id\";s:1:\"1\";s:7:\"uniacid\";s:1:\"1\";s:8:\"new_area\";s:1:\"1\";s:14:\"address_street\";s:1:\"1\";s:10:\"createtime\";s:10:\"1494815569\";}}','a:2:{s:7:\"diypage\";a:6:{s:7:\"setmenu\";i:1;s:4:\"menu\";a:14:{s:4:\"shop\";s:1:\"7\";s:8:\"shop_wap\";s:1:\"7\";s:10:\"creditshop\";s:1:\"7\";s:14:\"creditshop_wap\";s:1:\"7\";s:10:\"commission\";s:1:\"7\";s:14:\"commission_wap\";s:1:\"7\";s:6:\"groups\";s:1:\"7\";s:10:\"groups_wap\";s:1:\"7\";s:3:\"sns\";s:1:\"7\";s:7:\"sns_wap\";s:1:\"7\";s:4:\"sign\";s:1:\"7\";s:8:\"sign_wap\";s:1:\"7\";s:7:\"seckill\";s:1:\"7\";s:11:\"seckill_wap\";s:1:\"7\";}s:4:\"page\";a:7:{s:4:\"home\";s:2:\"19\";s:6:\"member\";s:2:\"25\";s:6:\"detail\";s:0:\"\";s:10:\"commission\";s:2:\"38\";s:10:\"creditshop\";s:0:\"\";s:7:\"seckill\";s:0:\"\";s:8:\"exchange\";s:0:\"\";}s:9:\"followbar\";a:3:{s:6:\"params\";a:14:{s:4:\"logo\";s:103:\"http://liudianchuangfu.oss-cn-hangzhou.aliyuncs.com/images/1/2017/05/k5Ll5uud5DsDlI5fiGZLA5uLWG5CIP.png\";s:6:\"isopen\";s:1:\"1\";s:8:\"icontype\";s:1:\"1\";s:7:\"iconurl\";s:0:\"\";s:9:\"iconstyle\";s:0:\"\";s:11:\"defaulttext\";s:0:\"\";s:9:\"sharetext\";s:0:\"\";s:7:\"btntext\";s:12:\"点击关注\";s:7:\"btnicon\";s:0:\"\";s:8:\"btnclick\";s:1:\"0\";s:11:\"btnlinktype\";s:1:\"0\";s:7:\"btnlink\";s:0:\"\";s:10:\"qrcodetype\";s:1:\"0\";s:9:\"qrcodeurl\";s:0:\"\";}s:5:\"style\";a:5:{s:10:\"background\";s:7:\"#444444\";s:9:\"textcolor\";s:7:\"#ffffff\";s:10:\"btnbgcolor\";s:7:\"#04be02\";s:8:\"btncolor\";s:7:\"#ffffff\";s:9:\"highlight\";s:7:\"#ffffff\";}s:5:\"merch\";s:1:\"0\";}s:5:\"layer\";a:3:{s:6:\"params\";a:4:{s:6:\"isopen\";s:1:\"1\";s:6:\"imgurl\";s:51:\"images/1/2017/05/XK7QqebE6QqWMZK5Z6BObKQ7mnbmch.png\";s:7:\"linkurl\";s:0:\"\";s:12:\"iconposition\";s:12:\"right bottom\";}s:5:\"style\";a:3:{s:5:\"width\";s:2:\"34\";s:3:\"top\";s:3:\"162\";s:4:\"left\";s:1:\"0\";}s:5:\"merch\";s:1:\"0\";}s:5:\"gotop\";a:3:{s:6:\"params\";a:8:{s:6:\"isopen\";s:1:\"1\";s:9:\"gotoptype\";s:1:\"0\";s:10:\"gotopclick\";s:1:\"0\";s:6:\"imgurl\";s:60:\"../addons/yunphp_shop/plugin/diypage/static/images/gotop.png\";s:7:\"linkurl\";s:0:\"\";s:12:\"iconposition\";s:12:\"right bottom\";s:9:\"iconclass\";s:9:\"icon-top1\";s:11:\"gotopheight\";s:3:\"300\";}s:5:\"style\";a:6:{s:9:\"iconcolor\";s:7:\"#ffffff\";s:10:\"background\";s:7:\"#000000\";s:7:\"opacity\";s:3:\"0.5\";s:5:\"width\";s:2:\"30\";s:3:\"top\";s:2:\"20\";s:4:\"left\";s:2:\"10\";}s:5:\"merch\";s:1:\"0\";}}s:10:\"commission\";a:51:{s:5:\"level\";s:1:\"3\";s:7:\"selfbuy\";s:1:\"0\";s:12:\"become_child\";s:1:\"0\";s:6:\"become\";s:1:\"0\";s:17:\"become_ordercount\";s:0:\"\";s:17:\"become_moneycount\";s:0:\"\";s:12:\"become_check\";s:1:\"0\";s:12:\"become_order\";s:1:\"0\";s:13:\"open_protocol\";s:1:\"0\";s:10:\"become_reg\";s:1:\"0\";s:17:\"no_commission_url\";s:0:\"\";s:8:\"withdraw\";s:1:\"1\";s:14:\"commissiontype\";s:1:\"0\";s:14:\"withdrawcharge\";s:0:\"\";s:13:\"withdrawbegin\";d:0;s:11:\"withdrawend\";d:0;s:10:\"settledays\";s:0:\"\";s:8:\"levelurl\";s:0:\"\";s:9:\"leveltype\";s:1:\"0\";s:13:\"openbonustask\";s:1:\"0\";s:11:\"qrcodeshare\";s:1:\"1\";s:9:\"codeShare\";s:1:\"1\";s:15:\"openorderdetail\";s:1:\"1\";s:14:\"openorderbuyer\";s:1:\"1\";s:13:\"closed_qrcode\";s:1:\"0\";s:6:\"qrcode\";s:1:\"1\";s:12:\"qrcode_title\";s:0:\"\";s:14:\"qrcode_content\";s:0:\"\";s:13:\"qrcode_remark\";s:0:\"\";s:15:\"register_bottom\";s:1:\"0\";s:22:\"register_bottom_title1\";s:0:\"\";s:24:\"register_bottom_content1\";s:0:\"\";s:22:\"register_bottom_title2\";s:0:\"\";s:24:\"register_bottom_content2\";s:0:\"\";s:22:\"register_bottom_title3\";s:0:\"\";s:24:\"register_bottom_content3\";s:0:\"\";s:22:\"register_bottom_remark\";s:0:\"\";s:23:\"register_bottom_content\";s:0:\"\";s:11:\"closemyshop\";s:1:\"1\";s:12:\"select_goods\";s:1:\"0\";s:5:\"style\";s:7:\"default\";s:5:\"regbg\";s:0:\"\";s:10:\"applytitle\";s:0:\"\";s:12:\"applycontent\";s:0:\"\";s:10:\"cashcredit\";i:0;s:10:\"cashweixin\";i:0;s:9:\"cashother\";i:0;s:10:\"cashalipay\";i:0;s:8:\"cashcard\";i:0;s:14:\"become_goodsid\";i:0;s:5:\"texts\";a:25:{s:5:\"agent\";s:9:\"合伙人\";s:4:\"shop\";s:9:\"礼品库\";s:6:\"myshop\";s:12:\"我的创服\";s:6:\"center\";s:15:\"合伙人中心\";s:6:\"become\";s:15:\"成为合伙人\";s:8:\"withdraw\";s:6:\"提现\";s:10:\"commission\";s:9:\"协作金\";s:11:\"commission1\";s:9:\"协作金\";s:16:\"commission_total\";s:15:\"累计协作金\";s:13:\"commission_ok\";s:18:\"可提现协作金\";s:16:\"commission_apply\";s:18:\"已申请协作金\";s:16:\"commission_check\";s:18:\"待打款协作金\";s:15:\"commission_lock\";s:18:\"未结算协作金\";s:15:\"commission_wait\";s:18:\"待收货协作金\";s:15:\"commission_fail\";s:15:\"无效协作金\";s:14:\"commission_pay\";s:21:\"成功提现协作金\";s:17:\"commission_charge\";s:21:\"扣除个人所得税\";s:17:\"commission_detail\";s:15:\"协作金明细\";s:5:\"order\";s:15:\"协作金明细\";s:4:\"down\";s:6:\"下线\";s:6:\"mydown\";s:12:\"我的下线\";s:2:\"c1\";s:6:\"一级\";s:2:\"c2\";s:6:\"二级\";s:2:\"c3\";s:6:\"三级\";s:4:\"yuan\";s:3:\"元\";}}}','a:28:{s:10:\"app_wechat\";a:5:{s:5:\"appid\";s:0:\"\";s:9:\"appsecret\";s:0:\"\";s:9:\"merchname\";s:0:\"\";s:7:\"merchid\";s:0:\"\";s:6:\"apikey\";s:0:\"\";}s:10:\"alipay_pay\";a:4:{s:7:\"partner\";s:0:\"\";s:12:\"account_name\";s:0:\"\";s:5:\"email\";s:0:\"\";s:3:\"key\";s:0:\"\";}s:10:\"app_alipay\";a:3:{s:10:\"public_key\";s:0:\"\";s:11:\"private_key\";s:0:\"\";s:5:\"appid\";s:0:\"\";}s:9:\"appid_sub\";s:0:\"\";s:13:\"sub_appid_sub\";s:0:\"\";s:9:\"mchid_sub\";s:0:\"\";s:13:\"sub_mchid_sub\";s:0:\"\";s:10:\"apikey_sub\";s:0:\"\";s:5:\"appid\";s:0:\"\";s:6:\"secret\";s:0:\"\";s:5:\"mchid\";s:0:\"\";s:6:\"apikey\";s:0:\"\";s:13:\"appid_jie_sub\";s:0:\"\";s:17:\"sub_appid_jie_sub\";s:0:\"\";s:18:\"sub_secret_jie_sub\";s:0:\"\";s:13:\"mchid_jie_sub\";s:0:\"\";s:17:\"sub_mchid_jie_sub\";s:0:\"\";s:14:\"apikey_jie_sub\";s:0:\"\";s:16:\"weixin_wft_mchid\";s:0:\"\";s:14:\"weixin_wft_key\";s:0:\"\";s:17:\"weixin_wft_native\";i:0;s:17:\"weixin_zxyh_mchid\";s:0:\"\";s:15:\"weixin_zxyh_key\";s:0:\"\";s:16:\"weixin_zxyh_key2\";s:0:\"\";s:21:\"weixin_zxyh_sub_mchid\";s:0:\"\";s:20:\"weixin_zxyhapp_mchid\";s:0:\"\";s:18:\"weixin_zxyhapp_key\";s:0:\"\";s:19:\"weixin_zxyhapp_key2\";s:0:\"\";}');


DROP TABLE IF EXISTS ims_yunphp_shop_system_adv;
CREATE TABLE `ims_yunphp_shop_system_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `url` varchar(255) DEFAULT '',
  `createtime` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `module` varchar(255) DEFAULT '',
  `status` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_article;
CREATE TABLE `ims_yunphp_shop_system_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `author` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `content` text,
  `createtime` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `cate` int(11) DEFAULT '0',
  `status` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_banner;
CREATE TABLE `ims_yunphp_shop_system_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `url` varchar(255) DEFAULT '',
  `createtime` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `status` tinyint(3) DEFAULT '0',
  `background` varchar(10) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_case;
CREATE TABLE `ims_yunphp_shop_system_case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `qr` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `status` tinyint(3) DEFAULT '0',
  `cate` int(11) DEFAULT '0',
  `description` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_casecategory;
CREATE TABLE `ims_yunphp_shop_system_casecategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_category;
CREATE TABLE `ims_yunphp_shop_system_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_company_article;
CREATE TABLE `ims_yunphp_shop_system_company_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `author` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `content` text,
  `createtime` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `cate` int(11) DEFAULT '0',
  `status` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_company_category;
CREATE TABLE `ims_yunphp_shop_system_company_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_copyright;
CREATE TABLE `ims_yunphp_shop_system_copyright` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT NULL,
  `copyright` text,
  `bgcolor` varchar(255) DEFAULT '',
  `ismanage` tinyint(3) DEFAULT '0',
  `logo` varchar(255) DEFAULT '',
  `title` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO ims_yunphp_shop_system_copyright VALUES 
('7','1','<p style=\"text-align: center;\">六点创服 版权所有</p><p style=\"text-align: center;\"><span style=\"font-size: 12px;\">六点软件 技术支持</span></p>','#fff','0','',''),
('8','-1','<p style=\"text-align: center;\">六点创服 版权所有</p><p style=\"text-align: center;\"><span style=\"font-size: 12px;\">六点软件 技术支持</span></p>','#fff','0','',''),
('9','1','<p style=\"text-align: center;\">六点创服 版权所有</p><p style=\"text-align: center;\"><span style=\"font-size: 12px;\">六点软件 技术支持</span></p><p><br/></p>','','1','images/1/2017/05/sd3cDrcg00gu008ZZPVGcBPRr2i0Zt.png','六点创服智能管理系统'),
('10','-1','<p style=\"text-align: center;\">六点创服 版权所有</p><p style=\"text-align: center;\"><span style=\"font-size: 12px;\">六点软件 技术支持</span></p><p><br/></p>','','1','images/1/2017/05/sd3cDrcg00gu008ZZPVGcBPRr2i0Zt.png','六点创服智能管理系统');


DROP TABLE IF EXISTS ims_yunphp_shop_system_copyright_notice;
CREATE TABLE `ims_yunphp_shop_system_copyright_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `author` varchar(255) DEFAULT '',
  `content` text,
  `createtime` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `status` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_guestbook;
CREATE TABLE `ims_yunphp_shop_system_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` varchar(255) NOT NULL DEFAULT '',
  `nickname` varchar(255) NOT NULL DEFAULT '',
  `createtime` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `clientip` varchar(64) NOT NULL DEFAULT '',
  `mobile` varchar(11) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_link;
CREATE TABLE `ims_yunphp_shop_system_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `displayorder` int(11) DEFAULT NULL,
  `status` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_setting;
CREATE TABLE `ims_yunphp_shop_system_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) DEFAULT NULL,
  `background` varchar(10) DEFAULT '',
  `casebanner` varchar(255) DEFAULT '',
  `contact` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_system_site;
CREATE TABLE `ims_yunphp_shop_system_site` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT '',
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_task;
CREATE TABLE `ims_yunphp_shop_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL,
  `starttime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `dotime` int(11) NOT NULL DEFAULT '0',
  `donetime` int(11) NOT NULL DEFAULT '0',
  `timelimit` float(11,1) NOT NULL,
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `explain` text,
  `require_data` text NOT NULL,
  `reward_data` text NOT NULL,
  `period` int(11) NOT NULL DEFAULT '0',
  `repeat` int(11) NOT NULL DEFAULT '0',
  `maxtimes` int(11) NOT NULL DEFAULT '0',
  `everyhours` float(11,1) NOT NULL DEFAULT '0.0',
  `logo` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_adv;
CREATE TABLE `ims_yunphp_shop_task_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `advname` varchar(50) DEFAULT '',
  `link` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_default;
CREATE TABLE `ims_yunphp_shop_task_default` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `data` text,
  `addtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_extension;
CREATE TABLE `ims_yunphp_shop_task_extension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskname` varchar(255) NOT NULL DEFAULT '',
  `taskclass` varchar(25) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `classify` varchar(255) NOT NULL DEFAULT '',
  `classify_name` varchar(255) NOT NULL DEFAULT '',
  `verb` varchar(255) NOT NULL DEFAULT '',
  `unit` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

INSERT INTO ims_yunphp_shop_task_extension VALUES 
('1','推荐人数','commission_member','1','number','number','推荐','人'),
('2','分销佣金','commission_money','1','number','number','达到','元'),
('3','分销订单','commission_order','1','number','number','达到','笔'),
('4','订单满额','cost_enough','1','number','number','满','元'),
('5','累计金额','cost_total','1','number','number','累计','元'),
('6','订单数量','cost_count','1','number','number','达到','单'),
('7','指定商品','cost_goods','1','select','select','购买指定商品','件'),
('8','商品评价','cost_comment','1','number','number','评价订单','次'),
('9','累计充值','cost_rechargetotal','1','number','number','达到','元'),
('10','充值满额','cost_rechargeenough','1','number','number','满','元'),
('11','完善信息','member_info','1','boole','boole','填写手机号','');


DROP TABLE IF EXISTS ims_yunphp_shop_task_extension_join;
CREATE TABLE `ims_yunphp_shop_task_extension_join` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL,
  `taskid` int(11) NOT NULL,
  `openid` varchar(255) NOT NULL,
  `require_data` text NOT NULL,
  `progress_data` text NOT NULL,
  `reward_data` text NOT NULL,
  `completetime` int(11) NOT NULL DEFAULT '0',
  `pickuptime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `dotime` int(11) NOT NULL DEFAULT '0',
  `rewarded` text NOT NULL,
  `logo` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_join;
CREATE TABLE `ims_yunphp_shop_task_join` (
  `join_id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `join_user` varchar(100) NOT NULL DEFAULT '',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `task_type` tinyint(1) NOT NULL DEFAULT '0',
  `needcount` int(11) NOT NULL DEFAULT '0',
  `completecount` int(11) NOT NULL DEFAULT '0',
  `reward_data` text,
  `is_reward` tinyint(1) NOT NULL DEFAULT '0',
  `failtime` int(11) NOT NULL DEFAULT '0',
  `addtime` int(11) DEFAULT '0',
  PRIMARY KEY (`join_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_joiner;
CREATE TABLE `ims_yunphp_shop_task_joiner` (
  `complete_id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `task_user` varchar(100) NOT NULL DEFAULT '',
  `joiner_id` varchar(100) NOT NULL DEFAULT '',
  `join_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `task_type` tinyint(1) NOT NULL DEFAULT '0',
  `join_status` tinyint(1) NOT NULL DEFAULT '1',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`complete_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_log;
CREATE TABLE `ims_yunphp_shop_task_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `openid` varchar(100) NOT NULL DEFAULT '',
  `from_openid` varchar(100) NOT NULL DEFAULT '',
  `join_id` int(11) NOT NULL DEFAULT '0',
  `taskid` int(11) DEFAULT '0',
  `task_type` tinyint(1) NOT NULL DEFAULT '0',
  `subdata` text,
  `recdata` text,
  `createtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_poster;
CREATE TABLE `ims_yunphp_shop_task_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT NULL,
  `days` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `bg` varchar(255) DEFAULT '',
  `data` text,
  `keyword` varchar(255) DEFAULT NULL,
  `resptype` tinyint(1) NOT NULL DEFAULT '0',
  `resptext` text,
  `resptitle` varchar(255) DEFAULT NULL,
  `respthumb` varchar(255) DEFAULT NULL,
  `respdesc` varchar(255) DEFAULT NULL,
  `respurl` varchar(255) DEFAULT NULL,
  `createtime` int(11) DEFAULT NULL,
  `waittext` varchar(255) DEFAULT NULL,
  `oktext` varchar(255) DEFAULT NULL,
  `scantext` varchar(255) DEFAULT NULL,
  `beagent` tinyint(1) NOT NULL DEFAULT '0',
  `bedown` tinyint(1) NOT NULL DEFAULT '0',
  `timestart` int(11) DEFAULT NULL,
  `timeend` int(11) DEFAULT NULL,
  `is_repeat` tinyint(1) DEFAULT '0',
  `getposter` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `starttext` varchar(255) DEFAULT NULL,
  `endtext` varchar(255) DEFAULT NULL,
  `reward_data` text,
  `needcount` int(11) NOT NULL DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `poster_type` tinyint(1) DEFAULT '1',
  `reward_days` int(11) DEFAULT '0',
  `titleicon` text,
  `poster_banner` text,
  `is_goods` tinyint(1) DEFAULT '0',
  `autoposter` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_task_poster_qr;
CREATE TABLE `ims_yunphp_shop_task_poster_qr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acid` int(11) NOT NULL DEFAULT '0',
  `openid` varchar(100) NOT NULL,
  `posterid` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `sceneid` int(11) NOT NULL DEFAULT '0',
  `mediaid` varchar(255) DEFAULT NULL,
  `ticket` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `createtime` int(11) DEFAULT NULL,
  `qrimg` varchar(1000) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



DROP TABLE IF EXISTS ims_yunphp_shop_virtual_category;
CREATE TABLE `ims_yunphp_shop_virtual_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0' COMMENT '所属帐号',
  `name` varchar(50) DEFAULT NULL COMMENT '分类名称',
  `merchid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_virtual_data;
CREATE TABLE `ims_yunphp_shop_virtual_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `typeid` int(11) NOT NULL DEFAULT '0' COMMENT '类型id',
  `pvalue` varchar(255) DEFAULT '' COMMENT '主键键值',
  `fields` text NOT NULL COMMENT '字符集',
  `openid` varchar(255) NOT NULL DEFAULT '' COMMENT '使用者openid',
  `usetime` int(11) NOT NULL DEFAULT '0' COMMENT '使用时间',
  `orderid` int(11) DEFAULT '0',
  `ordersn` varchar(255) DEFAULT '',
  `price` decimal(10,2) DEFAULT '0.00',
  `merchid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_typeid` (`typeid`),
  KEY `idx_usetime` (`usetime`),
  KEY `idx_orderid` (`orderid`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_virtual_type;
CREATE TABLE `ims_yunphp_shop_virtual_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL DEFAULT '0',
  `cate` int(11) DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `fields` text NOT NULL COMMENT '字段集',
  `usedata` int(11) NOT NULL DEFAULT '0' COMMENT '已用数据',
  `alldata` int(11) NOT NULL DEFAULT '0' COMMENT '全部数据',
  `merchid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_cate` (`cate`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS ims_yunphp_shop_wxcard;
CREATE TABLE `ims_yunphp_shop_wxcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT NULL,
  `card_id` varchar(255) DEFAULT '0',
  `displayorder` int(11) DEFAULT NULL,
  `catid` int(11) DEFAULT NULL,
  `card_type` varchar(50) DEFAULT NULL,
  `logo_url` varchar(255) DEFAULT NULL,
  `wxlogourl` varchar(255) DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `code_type` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `notice` varchar(50) DEFAULT NULL,
  `service_phone` varchar(50) DEFAULT NULL,
  `description` text,
  `datetype` varchar(50) DEFAULT NULL,
  `begin_timestamp` int(11) DEFAULT NULL,
  `end_timestamp` int(11) DEFAULT NULL,
  `fixed_term` int(11) DEFAULT NULL,
  `fixed_begin_term` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `total_quantity` varchar(255) DEFAULT NULL,
  `use_limit` int(11) DEFAULT NULL,
  `get_limit` int(11) DEFAULT NULL,
  `use_custom_code` tinyint(1) DEFAULT NULL,
  `bind_openid` tinyint(1) DEFAULT NULL,
  `can_share` tinyint(1) DEFAULT NULL,
  `can_give_friend` tinyint(1) DEFAULT NULL,
  `center_title` varchar(20) DEFAULT NULL,
  `center_sub_title` varchar(20) DEFAULT NULL,
  `center_url` varchar(255) DEFAULT NULL,
  `setcustom` tinyint(1) DEFAULT NULL,
  `custom_url_name` varchar(20) DEFAULT NULL,
  `custom_url_sub_title` varchar(20) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `setpromotion` tinyint(1) DEFAULT NULL,
  `promotion_url_name` varchar(20) DEFAULT NULL,
  `promotion_url_sub_title` varchar(20) DEFAULT NULL,
  `promotion_url` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `can_use_with_other_discount` tinyint(1) DEFAULT NULL,
  `setabstract` tinyint(1) DEFAULT NULL,
  `abstract` varchar(50) DEFAULT NULL,
  `abstractimg` varchar(255) DEFAULT NULL,
  `icon_url_list` varchar(255) DEFAULT NULL,
  `accept_category` varchar(50) DEFAULT NULL,
  `reject_category` varchar(50) DEFAULT NULL,
  `least_cost` decimal(10,2) DEFAULT NULL,
  `reduce_cost` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `limitgoodtype` tinyint(1) DEFAULT '0',
  `limitgoodcatetype` tinyint(1) unsigned DEFAULT '0',
  `limitgoodcateids` varchar(255) DEFAULT NULL,
  `limitgoodids` varchar(255) DEFAULT NULL,
  `limitdiscounttype` tinyint(1) unsigned DEFAULT '0',
  `merchid` int(11) DEFAULT '0',
  `gettype` tinyint(3) DEFAULT NULL,
  `islimitlevel` tinyint(1) DEFAULT '0',
  `limitmemberlevels` varchar(500) DEFAULT '',
  `limitagentlevels` varchar(500) DEFAULT '',
  `limitpartnerlevels` varchar(500) DEFAULT '',
  `limitaagentlevels` varchar(500) DEFAULT '',
  `settitlecolor` tinyint(1) DEFAULT '0',
  `titlecolor` varchar(10) DEFAULT '',
  `tagtitle` varchar(20) DEFAULT '',
  `use_condition` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



----WeEngine MySQL Dump End