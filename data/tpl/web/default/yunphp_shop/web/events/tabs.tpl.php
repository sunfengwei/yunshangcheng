<?php defined('IN_IA') or exit('Access Denied');?><ul class="menu-head-top">
    <li <?php  if($_W['action']=='order') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('events/order')?>">活动概述 <i class="fa fa-caret-right"></i></a></li>
</ul>
<div class='menu-header'><?php  echo $this->plugintitle?></div>


<ul>
    <?php if(cv('events.partner')) { ?><li <?php  if($_W['routes']=='events.member') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('events/member')?>">主办方管理</a></li><?php  } ?>
    <?php if(cv('events.level')) { ?><li <?php  if($_W['routes']=='events.level') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('events/level')?>">主办方等级</a></li><?php  } ?>
</ul>

<style type='text/css'>
    .events-list a {
        position: relative;
    }
    .events-list span  {

        float:right;margin-right:20px;
    }
</style>

<?php  $goodstotals = $this->model->getgoodsTotals()?>
<?php if(cv('goods')) { ?><?php  } ?>
<div class='menu-header'>活动管理</div>
<ul class='events-list'>
    <li <?php  if($_GPC['goodsfrom'] == 'sale') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('events.goods',array('goodsfrom'=>'sale'))?>">报名中<span><?php  echo $goodstotals['sale'];?></span></a></li>
     <li <?php  if($_GPC['goodsfrom'] == 'out') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('events.goods',array('goodsfrom'=>'out'))?>">报名已满<span  class='text-danger'><?php  echo $goodstotals['out'];?></span></a></li>
    <li <?php  if($_GPC['goodsfrom'] == 'stock') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('events.goods',array('goodsfrom'=>'stock'))?>">仓库中<span><?php  echo $goodstotals['stock'];?></span></a></li>
    <li <?php  if($_GPC['goodsfrom'] == 'cycle') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('events.goods',array('goodsfrom'=>'cycle'))?>">回收站<span  class='text-default'><?php  echo $goodstotals['cycle'];?></span></a></li>
     <?php if(cv('goods.category')) { ?><li <?php  if($_W['action'] == 'goods.category') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('events.goods/category')?>">活动分类</a></li><?php  } ?>
    <?php if(cv('goods.group')) { ?><li <?php  if($_GPC['r'] == 'goods.group' || $_GPC['r'] == 'goods.group.edit') { ?>class="active" <?php  } ?>><a href="<?php  echo webUrl('events.goods/group')?>">活动组</a></li><?php  } ?>
    <li <?php  if($_W['action'] == 'goods.label') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('events.goods/label')?>">标签管理</a></li>
</ul>





<?php if(cv('order.list.status1|order.list.status2|order.list.status0|order.list.status3|order.list.status_1|order.list.main')) { ?>
<div class='menu-header'>报名管理</div>
<ul class='events-list'>
    <?php if(cv('order.list.status1')) { ?>
    <li <?php  if($_W['routes']=='events.order.list.status1') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list/status1')?>">待参会 <span class='text-danger status1'>--</span></a>
    </li>
    <?php  } ?>
    
    <?php if(cv('order.list.status2')) { ?>
    <li <?php  if($_W['routes']=='events.order.list.status2') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list/status2')?>">已过期 <span class='text-warning status2'>--</span></a>
    </li>
    <?php  } ?>

    <?php if(cv('order.list.status0')) { ?>
    <li <?php  if($_W['routes']=='events.order.list.status0') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list/status0')?>">待付款 <span class="status0">--</span></a>
    </li>
    <?php  } ?>
    <?php if(cv('order.list.status3')) { ?>
    <li <?php  if($_W['routes']=='events.order.list.status3') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list/status3')?>">已参会 <span class='text-primary status3'>--</span></a>
    </li>
    <?php  } ?>
    
     <?php if(cv('order.list.status_1')) { ?>
    <li <?php  if($_W['routes']=='events.order.list.status_1') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list/status_1')?>">已关闭 <span class="status_1">--</span></a>
    </li>
    <?php  } ?>

             <?php if(cv('order.list.main')) { ?>
    <li <?php  if($_W['routes']=='events.order.list' && $_GPC['status']=='' && $_GPC['refund']!='1') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list')?>" >全部报名<span class="all">--</span></a>
    </li>
    <?php  } ?>
    
    <?php  if($operation == 'detail') { ?>
    <li class="active">
        <a href="#">报名详情</a>
    </li>
    <?php  } ?>
</ul>
<?php  } ?>
  <?php if(cv('order.list.status4|order.list.status5')) { ?>
<div class='menu-header'>维权</div>
<ul class='order-list'>
    <?php if(cv('order.list.status4')) { ?>
    <li <?php  if($_W['routes']=='order.list.status4') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list/status4')?>">维权申请 <span class='text-danger status4'>--</span></a>
    </li>
    <?php  } ?>
     
    <?php if(cv('order.list.status5')) { ?>
    <li <?php  if($_W['routes']=='order.list.status5') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/list/status5')?>">维权完成 <span class="status5">--</span></a>
    </li>
    <?php  } ?>
      </ul>
<?php  } ?>

<?php if(cv('order.export|order.batchsend')) { ?>
<div class='menu-header'>工具</div>
<ul>
    <?php if(cv('order.export')) { ?>
    <li <?php  if($operation=='export') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/export')?>">自定义导出</a>
    </li>
    <?php  } ?>

    <?php if(cv('order.batchsend')) { ?>
    <li <?php  if($operation=='batchsend') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('events/order/batchsend')?>">批量发货</a>
    </li>
    <?php  } ?>
</ul>
<?php  } ?>

<?php if(cv('events.cover|events.notice|events.set')) { ?>
<div class="menu-header">设置</div>
<ul>
	<?php if(cv('events.cover')) { ?><li <?php  if($_W['routes']=='events.adv') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('events/adv')?>">广告管理</a></li><?php  } ?>
    <?php if(cv('events.cover')) { ?><li <?php  if($_W['routes']=='events.cover') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('events/cover')?>">入口设置</a></li><?php  } ?>
    <?php if(cv('events.notice')) { ?><li <?php  if($_W['routes']=='events.notice') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('events/notice')?>">通知设置</a></li><?php  } ?>
    <?php if(cv('events.set')) { ?><li <?php  if($_W['routes']=='events.set') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('events/set')?>">基础设置</a></li><?php  } ?>
</ul>

<?php  } ?>



<script>
    $(function () {
        $.ajax({type: "GET",url: "<?php  echo webUrl('order/list/ajaxgettotals')?>",dataType:"json",success: function(data){
                var res = data.result;
                $("span.status0").text(res.status0);
                $("span.status1").text(res.status1);
                $("span.status2").text(res.status2);
                $("span.status3").text(res.status3);
                $("span.status4").text(res.status4);
                $("span.status5").text(res.status5);
                $("span.status_1").text(res.status_1);
                $("span.all").text(res.all);
            }
        });
    });
</script>






<script>
    $(function () {
        $.ajax({type: "GET",async: false,url: "<?php  echo webUrl('events/bonus/totals')?>",dataType:"json",success: function(data){
            var res = data.result;
            $("#total0").text(res.total0);
            $("#total1").text(res.total1);
            $("#total2").text(res.total2);
        }
        });
    });
</script>