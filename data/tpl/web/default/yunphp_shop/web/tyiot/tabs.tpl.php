<?php defined('IN_IA') or exit('Access Denied');?><style type='text/css'>
   .mc-list a {
      position: relative;
   }
   .mc-list span  {

      float:right;margin-right:20px;
   }
</style>
<ul class="menu-head-top">
    <li <?php  if($_W['action']=='order') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('tyiot')?>">物联网概述 <i class="fa fa-caret-right"></i></a></li>
</ul>
<div class='menu-header'>订单管理</div>
<ul class="mc-list">
    <?php if(cv('tyiot.list.status1')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list.status1') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list/status1')?>">队列中订单 <span class='text-danger status1'>--</span></a>
    </li>
    <?php  } ?>
    
    <?php if(cv('tyiot.list.status2')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list.status2') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list/status2')?>">制作中订单 <span class='text-warning status2'>--</span></a>
    </li>
    <?php  } ?>

    <?php if(cv('tyiot.list.status0')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list.status0') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list/status0')?>">待付款 <span class="status0">--</span></a>
    </li>
    <?php  } ?>
    <?php if(cv('tyiot.list.status3')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list.status3') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list/status3')?>">已完成订单 <span class='text-primary status3'>--</span></a>
    </li>
    <?php  } ?>
    
    <?php if(cv('tyiot.list.status_1')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list.status_1') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list/status_1')?>">已关闭订单 <span class="status_1">--</span></a>
    </li>
    <?php  } ?>
	<?php if(cv('tyiot.list.status4')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list.status4') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list/status4')?>">故障订单 <span class='text-danger status4'>--</span></a>
    </li>
    <?php  } ?>
     
    <?php if(cv('tyiot.list.status5')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list.status5') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list/status5')?>">退款完成 <span class="status5">--</span></a>
    </li>
    <?php  } ?>

    <?php if(cv('tyiot.list.main')) { ?>
    <li <?php  if($_W['routes']=='tyiot.list' && $_GPC['status']=='' && $_GPC['refund']!='1') { ?>class="active"<?php  } ?>>
        <a href="<?php  echo webUrl('tyiot/order/list')?>" >全部订单<span class="all">--</span></a>
    </li>
    <?php  } ?>
    
    <?php  if($operation == 'detail') { ?>
    <li class="active">
        <a href="#">订单详情</a>
    </li>
    <?php  } ?>

</ul>

<div class='menu-header'>终端管理</div>
<ul class="mc-list">
	<?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/fault')?>">故障查询处理 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/terminal')?>">终端列表 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/terminal/add')?>">添加终端 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/tertype')?>">终端分类 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/instructions')?>">指令集管理 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
</ul>

<!-- <div class='menu-header'>入驻申请</div> -->
<!-- <ul class="mc-list"> -->
   <!-- <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/reg',array('status'=>0))?>">申请中 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?> -->
   <!-- <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/reg',array('status'=>-1))?>">驳回 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?> -->
<!-- </ul> -->

<div class='menu-header'>通讯管理</div>
<ul class="mc-list">
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/message')?>">全部通讯日志 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/register')?>">注册通讯日志 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/heartbeat')?>">心跳通讯日志 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/com')?>">状态通讯日志 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/yun')?>">云端推送日志 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/registerok')?>">通讯绑定查询 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
</ul>

<div class='menu-header'>商品管理</div>
<ul class="mc-list">
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/message')?>">出售中 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/register')?>">仓库中 <span class="text-danger" id="reg0">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/heartbeat')?>">回收站 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
   <?php if(cv('merch.reg')) { ?><li <?php  if($_W['routes']=='merch.reg' && $_GPC['status']==-1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('tyiot/com')?>">商品分组 <span class="text-default"  id="reg_1">-</span></a> </li><?php  } ?>
</ul>

<div class='menu-header'>代理商管理</div>
<ul class="mc-list">
   <?php if(cv('merch.user')) { ?><li <?php  if($_W['routes']=='merch.user' && $_GPC['status']==0) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/user',array('status'=>0))?>">待入驻 <span class="text-success"  id="user0">-</span></a></li><?php  } ?>
   <?php if(cv('merch.user')) { ?><li <?php  if($_W['routes']=='merch.user' && $_GPC['status']==1) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/user',array('status'=>1))?>">入驻中 <span class="text-primary"  id="user1">-</span></a></li><?php  } ?>
   <?php if(cv('merch.user')) { ?><li <?php  if($_W['routes']=='merch.user' && $_GPC['status']==2) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/user',array('status'=>2))?>">暂停中 <span class="text-warning"  id="user2">-</span></a></li><?php  } ?>
   <?php if(cv('merch.user')) { ?><li <?php  if($_W['routes']=='merch.user' && $_GPC['status']==3) { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/user',array('status'=>3))?>">即将到期 <span class="text-danger"   id="user3">-</span></a></li><?php  } ?>
   <?php if(cv('merch.group')) { ?><li <?php  if($_W['routes']=='merch.group') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/group')?>">商户分组</a></li><?php  } ?>
   <?php if(cv('merch.category')) { ?><li <?php  if($_W['routes']=='merch.category') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/category')?>">商户分类</a></li><?php  } ?>
</ul>

<div class='menu-header'>数据</div>
<ul class="mc-list">
   <?php if(cv('merch.statistics.order')) { ?><li <?php  if($_W['routes']=='merch.statistics.order') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/statistics/order')?>">订单统计</a></li><?php  } ?>
   <?php if(cv('merch.statistics.merch')) { ?><li <?php  if($_W['routes']=='merch.statistics.merch') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/statistics/merch')?>">商户统计</a></li><?php  } ?>
</ul>

<div class='menu-header'>提现申请</div>
<ul class="mc-list">
    <?php if(cv('merch.check.status1')) { ?><li <?php  if($_W['routes']=='merch.check.status1') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/check/status1')?>">待确认的 <span class="text-default"  id="status1">-</span></a></li><?php  } ?>
    <?php if(cv('merch.check.status2')) { ?><li <?php  if($_W['routes']=='merch.check.status2') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/check/status2')?>">待打款的 <span class="text-primary"  id="status2">-</span></a></li><?php  } ?>
    <?php if(cv('merch.check.status3')) { ?><li <?php  if($_W['routes']=='merch.check.status3') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/check/status3')?>">已打款的 <span class="text-success"  id="status3">-</span></a></li><?php  } ?>
    <?php if(cv('merch.check.status_1')) { ?><li <?php  if($_W['routes']=='merch.check.status_1') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/check/status_1')?>">无效的 <span class="text-success"  id="status_1">-</span></a></li><?php  } ?>
</ul>

<!--div class='menu-header'>结算订单</div>
<ul class="mc-list">

   <?php if(cv('merch.clearing.status0')) { ?><li <?php  if($_W['routes']=='merch.clearing.status0') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/clearing/status0')?>">待确认 <span class="text-default"  id="status0">-</span></a></li><?php  } ?>
   <?php if(cv('merch.clearing.status1')) { ?><li <?php  if($_W['routes']=='merch.clearing.status1') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/clearing/status1')?>">待结算 <span class="text-primary"  id="status1">-</span></a></li><?php  } ?>
   <?php if(cv('merch.clearing.status2')) { ?><li <?php  if($_W['routes']=='merch.clearing.status2') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/clearing/status2')?>">已结算 <span class="text-success"  id="status2">-</span></a></li><?php  } ?>
   <?php if(cv('merch.clearing.add')) { ?><li <?php  if($_W['routes']=='merch.clearing.add') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/clearing/add')?>">创建结算单</a></li><?php  } ?>

</ul-->

<div class='menu-header'>设置</div>
<ul>

   <?php if(cv('merch.set')) { ?><li <?php  if($_W['routes']=='merch.set') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/set')?>">基础设置</a></li><?php  } ?>
   <?php if(cv('merch.notice')) { ?><li <?php  if($_W['routes']=='merch.notice') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/notice')?>">通知设置</a></li><?php  } ?>
   <?php if(cv('merch.cover')) { ?><li <?php  if($_W['routes']=='merch.cover') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/cover')?>">入口设置</a></li><?php  } ?>
   <?php if(cv('merch.category.swipe')) { ?><li <?php  if($_W['routes']=='merch.category.swipe') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('merch/category.swipe')?>">商户分类幻灯</a></li><?php  } ?>

</ul>
<script language="javascript">
   $(function(){
      $.ajax({
         url: "<?php  echo webUrl('tyiot/util/totals')?>",
         dataType:'json',
         cache:false,
         success:function(ret){
            $('#reg0').html( ret.result.reg0);
            $('#reg_1').html( ret.result.reg_1);
            $('#user0').html( ret.result.user0);
            $('#user1').html( ret.result.user1);
            $('#user2').html( ret.result.user2);
            $('#user3').html( ret.result.user3);
//            $('#status0').html( ret.result.status0);
            $('#status1').html( ret.result.status1);
            $('#status2').html( ret.result.status2);
            $('#status3').html( ret.result.status3);
            $('#status_1').html( ret.result.status_1);
         }
      });

   });

</script>
<script>
    $(function () {
        $.ajax({type: "GET",url: "<?php  echo webUrl('tyiot/order/list/ajaxgettotals')?>",dataType:"json",success: function(data){
                var res = data.result;
                $("span.status0").text(res.status0);
                $("span.status1").text(res.status1);
                $("span.status2").text(res.status2);
                $("span.status3").text(res.status3);
                $("span.status4").text(res.status4);
                $("span.status5").text(res.status5);
                $("span.status_1").text(res.status_1);
                $("span.all").text(res.all);
            }
        });
    });
</script>