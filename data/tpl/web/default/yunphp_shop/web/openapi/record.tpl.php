<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<div class="page-heading" xmlns:border-top="http://www.w3.org/1999/xhtml">

    <h2>查询列表管理
        <?php  if(!empty($board)) { ?><small>版块: <?php  echo $board['title'];?></small><?php  } ?>
        <?php  if(!empty($m)) { ?><small>会员: <?php  echo $m['nickname'];?> </small><?php  } ?>
    </h2> </div>

<form action="./index.php" method="get" class="form-horizontal form-search" role="form">
    <input type="hidden" name="c" value="site" />
    <input type="hidden" name="a" value="entry" />
    <input type="hidden" name="m" value="yunphp_shop" />
    <input type="hidden" name="do" value="web" />
    <input type="hidden" name="r"  value="openapi.record" />
    <input type="hidden" name="id"  value="<?php  echo $id;?>" />
    <input type="hidden" name="uid"  value="<?php  echo $uid;?>" />
    <div class="page-toolbar row m-b-sm m-t-sm">
        <div class="col-sm-4">
            <div class="input-group-btn">
                <button class="btn btn-default btn-sm"  type="button" data-toggle='refresh'><i class='fa fa-refresh'></i></button>
                <?php if(cv('sns.board.delete')) { ?>
                <button class="btn btn-default btn-sm" type="button" data-toggle='batch' data-href="<?php  echo webUrl('sns/posts/delete',array('deleted'=>1))?>"><i class='fa fa-circle'></i> 恢复</button>
                <button class="btn btn-default btn-sm" type="button" data-toggle='batch'  data-href="<?php  echo webUrl('sns/posts/delete',array('deleted'=>0))?>"><i class='fa fa-circle-o'></i> 删除</button>
                <?php  } ?>

                <?php if(cv('sns.board.edit')) { ?>
                <button class="btn btn-default btn-sm" type="button" data-toggle='batch' data-href="<?php  echo webUrl('sns/posts/check',array('checked'=>1))?>"><i class='fa fa-circle'></i> 审核通过</button>
                <button class="btn btn-default btn-sm" type="button" data-toggle='batch'  data-href="<?php  echo webUrl('sns/posts/check',array('checked'=>0))?>"><i class='fa fa-circle-o'></i> 取消审核</button>
                <?php  } ?>
            </div>
        </div>


        <div class="col-sm-6 pull-right">
            <select name="checked" class='form-control input-sm select-sm'>
                <option value="" <?php  if($_GPC['checked'] == '') { ?> selected<?php  } ?>>审核</option>
                <option value="1" <?php  if($_GPC['checked'] == '1') { ?> selected<?php  } ?>>通过</option>
                <option value="0" <?php  if($_GPC['checked']== '0') { ?> selected<?php  } ?>>不通过</option>
            </select>
            <select name="deleted" class='form-control input-sm select-sm'>
                <option value="" <?php  if($_GPC['deleted'] == '') { ?> selected<?php  } ?>>状态</option>
                <option value="0" <?php  if($_GPC['deleted'] == '0') { ?> selected<?php  } ?>>正常</option>
                <option value="1" <?php  if($_GPC['deleted']== '1') { ?> selected<?php  } ?>>删除</option>
            </select>
            <div class="input-group">
                <input type="text" class="input-sm form-control" name='keyword' value="<?php  echo $_GPC['keyword'];?>" placeholder="会员信息/话题标题"> <span class="input-group-btn">
                    		
                    <button class="btn btn-sm btn-primary" type="submit"> 搜索</button> </span>
            </div>
        </div>

    </div>
</form>

<form action="" method="post">
    <?php  if(count($list)>0) { ?>
    <table class="table table-responsive table-hover" >
        <thead class="navbar-inner">
        <tr>
            <th style="width:25px;"><input type='checkbox' /></th>

            <th style='width:30px; text-align: center;'></th>
			<th style='width:80px;'>真实姓名/电话</th>
            <th style='width: 120px;'>查询关键字</th>
            <th style='width: 50px;'>查询结果</th>

            <th style='width:60px'>查询时间</th>
            <th style="width: 145px;">操作</th>
        </tr>
        </thead>
        <tbody>
        <?php  if(is_array($list)) { foreach($list as $row) { ?>
        <tr rel="pop" data-title="ID: <?php  echo $row['id'];?> " data-content="微信昵称 <br/>
				      <label class='label label-primary'><?php  echo $row['nickname'];?></label>
				      ">
            <td>
                <input type='checkbox'   value="<?php  echo $row['id'];?>"/>
            </td>

            <td style="text-align: center;">
                <img style="width:30px;height:30px;padding1px;border:1px solid #ccc" src="<?php  echo tomedia($row['avatar'])?>" />
            </td>
			<td class="full"><?php  echo $row['realname'];?><br/><?php  if($_GPC['permuid']=='') { ?>******<?php  } else { ?><?php  echo $row['mobile'];?><?php  } ?></td>

            <td class="full"><label class="label label-primary"><?php  if($row['apitype'] == 1) { ?>商标注册信息查询<?php  } else { ?>未知接口<?php  } ?></label><br/><?php  echo $row['keyword'];?></td>
            <td><?php  if(empty($row['results'])) { ?>查询成功<?php  } else { ?><?php  echo $row['results'];?><?php  } ?><br/>
				<label class="label label-primary"><?php  if(empty($row['isempty'])) { ?>已注册<?php  } else { ?>未注册<?php  } ?></label>
			</td>

            <td>
                <?php  echo date('Y-m-d', $row['createtime'])?><br/>
                <?php  echo date('H:i:s', $row['createtime'])?>
            </td>
            <td style="text-align:left;">
                
				<?php  if($_GPC['permuid']=='') { ?>
				<a data-toggle='ajaxRemove' href="<?php  echo webUrl('openapi/record/rob', array('id' => $row['id']))?>" class="btn btn-default btn-sm"  data-confirm='抢单后请立即联系客户，否则将受到处罚！' 
                style="color:red">
                    <i class='fa fa-comment'></i> 我要抢单
                </a>
				<?php  } else { ?>
                <a href="<?php  echo webUrl('openapi/shows', array('id' => $row['id']))?>" class="btn btn-default btn-sm"
                style="<?php  if($row['needchecks']>0) { ?>color:red<?php  } ?>">
                    <i class='fa fa-comment'></i> 联络客户
                </a>
				<?php  } ?>
                

                <?php if(cv('sns.posts.delete')) { ?>
                <a data-toggle='ajaxRemove' href="<?php  echo webUrl('sns/posts/delete1', array('id' => $row['id']))?>"class="btn btn-default btn-sm" data-confirm='确认要彻底删除此帖子吗?'><i class="fa fa-trash"></i> 删除记录</a>
                <?php  } ?>
            </td>
        </tr>

        <tr>
<td colspan="2" style=';border-top:none;'></td>
            <td  style='border-top:none;'>
             <a href="javascript:;" onclick="$(this).closest('tr').next('tr').toggle()">[查看结果详情]</a>
            </td>
            <td colspan="3" style='text-align: right;border-top:none;' class='aops'>

                <a class='<?php  if($row['deleted']==1) { ?>text-default<?php  } else { ?>text-danger<?php  } ?>'
                <?php if(cv('sns.posts.delete')) { ?>
                data-toggle='ajaxSwitch'
                data-switch-value='<?php  echo $row['deleted'];?>'
                data-switch-value0='0|正常|text-danger|<?php  echo webUrl('sns/posts/delete',array('deleted'=>1,'id'=>$row['id']))?>'
                data-switch-value1='1|已删除|text-default|<?php  echo webUrl('sns/posts/delete',array('deleted'=>0,'id'=>$row['id']))?>'
                <?php  } ?>>
                <?php  if($row['deleted']==1) { ?>已删除<?php  } else { ?>正常<?php  } ?>
                </a>

                <a class='<?php  if($row['checked']==1) { ?>text-danger<?php  } else { ?>text-default<?php  } ?>'
                <?php if(cv('sns.posts.edit')) { ?>
                data-toggle='ajaxSwitch'
                data-switch-value='<?php  echo $row['checked'];?>'
                data-switch-value0='0|待审核|text-default|<?php  echo webUrl('sns/posts/check',array('checked'=>1,'id'=>$row['id']))?>'
                data-switch-value1='1|已审核|text-danger|<?php  echo webUrl('sns/posts/check',array('checked'=>0,'id'=>$row['id']))?>'
                <?php  } ?>>
                <?php  if($row['checked']==1) { ?>已审核<?php  } else { ?>待审核<?php  } ?>
                </a>

                <a class='<?php  if($row['isboardbest']==1) { ?>text-danger<?php  } else { ?>text-default<?php  } ?>'
                <?php if(cv('sns.posts.edit')) { ?>
                data-toggle='ajaxSwitch'
                data-switch-value='<?php  echo $row['isboardbest'];?>'
                data-switch-value0='0|版块精华|text-default|<?php  echo webUrl('sns/posts/best',array('best'=>1,'all'=>0, 'id'=>$row['id']))?>'
                data-switch-value1='1|版块精华|text-danger|<?php  echo webUrl('sns/posts/best',array('best'=>0,'all'=>0,'id'=>$row['id']))?>'
                <?php  } ?>>
               版块精华
                </a>

                <a class='<?php  if($row['isbest']==1) { ?>text-danger<?php  } else { ?>text-default<?php  } ?>'
                <?php if(cv('sns.posts.edit')) { ?>
                data-toggle='ajaxSwitch'
                data-switch-value='<?php  echo $row['isbest'];?>'
                data-switch-value0='0|全站精华|text-default|<?php  echo webUrl('sns/posts/best',array('best'=>1,'all'=>1,'id'=>$row['id']))?>'
                data-switch-value1='1|全站精华|text-danger|<?php  echo webUrl('sns/posts/best',array('best'=>0,'all'=>1,'id'=>$row['id']))?>'
                <?php  } ?>>
                全站精华
                </a>


                <a class='<?php  if($row['isboardtop']==1) { ?>text-danger<?php  } else { ?>text-default<?php  } ?>'
                <?php if(cv('sns.posts.edit')) { ?>
                data-toggle='ajaxSwitch'
                data-switch-value='<?php  echo $row['isboardtop'];?>'
                data-switch-value0='0|版块置顶|text-default|<?php  echo webUrl('sns/posts/best',array('top'=>1,'all'=>0, 'id'=>$row['id']))?>'
                data-switch-value1='1|版块置顶|text-danger|<?php  echo webUrl('sns/posts/best',array('top'=>0,'all'=>0,'id'=>$row['id']))?>'
                <?php  } ?>>
                版块置顶
                </a>

                <a class='<?php  if($row['istop']==1) { ?>text-danger<?php  } else { ?>text-default<?php  } ?>'
                <?php if(cv('sns.posts.edit')) { ?>
                data-toggle='ajaxSwitch'
                data-switch-value='<?php  echo $row['istop'];?>'
                data-switch-value0='0|全站置顶|text-default|<?php  echo webUrl('sns/posts/top',array('top'=>1,'all'=>1,'id'=>$row['id']))?>'
                data-switch-value1='1|全站置顶|text-danger|<?php  echo webUrl('sns/posts/top',array('top'=>0,'all'=>1,'id'=>$row['id']))?>'
                <?php  } ?>>
                全站置顶
                </a>

            </td>
        </tr>



        <tr style="display:none;">
			
            <td colspan="2" style=';border-top:none;'></td>
            <td colspan="4"  style='border-top:none;' class="full">
			<?php  if(empty($row['isempty'])) { ?>
			<?php  if(is_array($row['resultdata'])) { foreach($row['resultdata'] as $data) { ?>
                <a href="http://pic.tmkoo.com/pic.php?zch=<?php  echo $data['tmImg'];?>" target="_blank"><img src="http://pic.tmkoo.com/pic.php?s=1&zch=<?php  echo $data['tmImg'];?>" style="width:50px;border:1px solid #ccc;padding:1px;margin:2px;" /></a> 商标名称：<?php  echo $data['tmName'];?>
				<br/>
				商标类别:<?php  echo $data['intCls'];?> 当前状态:<?php  echo $data['currentStatus'];?> 申请人:<?php  echo $data['applicantCn'];?><br/>
				注册号:<?php  echo $data['regNo'];?> 申请日期:<?php  echo $data['appDate'];?> 初审公告号:<?php  echo $data['announcementIssue'];?><br/>
				初审公告日期:<?php  echo $data['announcementDate'];?> 注册公告号:<?php  echo $data['regIssue'];?> 注册公告日期:<?php  echo $data['regDate'];?><br/>
			<?php  } } ?>
			<?php  } else { ?>
			<span style="color:red">查询商标未注册，请联系用户注册。</span>
			<?php  } ?>
            </td>
			
        </tr>

        <?php  } } ?>
        <tr>
            <td colspan='6'>
                <div class='pagers' style='float:right;'>
                    <?php  echo $pager;?>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <?php  echo $pager;?>
	
	<script language="javascript">
        <?php  if($opencommission) { ?>
        require(['bootstrap'],function(){
            $("[rel=pop]").popover({
                trigger:'manual',
                placement : 'left',
                title : $(this).data('title'),
                html: 'true',
                content : $(this).data('content'),
                animation: false
            }).on("mouseenter", function () {
                var _this = this;
                $(this).popover("show");
                $(this).siblings(".popover").on("mouseleave", function () {
                    $(_this).popover('hide');
                });
            }).on("mouseleave", function () {
                var _this = this;
                setTimeout(function () {
                    if (!$(".popover:hover").length) {
                        $(_this).popover("hide")
                    }
                }, 100);
            });


        });
        <?php  } ?>

    </script>
	
    <?php  } else { ?>
    <div class='panel panel-default'>
        <div class='panel-body' style='text-align: center;padding:30px;'>
		<?php  if($_GPC['permuid']=='') { ?>暂时没有任何未被抢单的查询记录!<?php  } else { ?>您没有未处理完成的已抢单客户，请先去查询列表抢单！<?php  } ?>
            
        </div>
    </div>
    <?php  } ?>

</form>


<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>