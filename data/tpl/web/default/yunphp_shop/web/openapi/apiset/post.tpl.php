<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<div class="page-heading"> 
	<span class='pull-right'>
		<?php if(cv('openapi.tertype.add')) { ?>
			<a class="btn btn-primary btn-sm" href="<?php  echo webUrl('openapi/apiset/add')?>">添加新API</a>
		<?php  } ?>
		<a class="btn btn-default  btn-sm" href="<?php  echo webUrl('openapi/apiset')?>">返回列表</a>
	</span>
	<h2><?php  if(!empty($item['id'])) { ?>编辑<?php  } else { ?>添加<?php  } ?>API <small><?php  if(!empty($item['id'])) { ?>修改【<?php  echo $item['catename'];?>】<?php  } ?></small></h2>
</div>

<form <?php if( ce('openapi.apiset' ,$item) ) { ?>action="" method="post"<?php  } ?> class="form-horizontal form-validate" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php  echo $item['id'];?>" />

<div class="form-group">
	<label class="col-sm-2 control-label">排序</label>
	<div class="col-sm-9 col-xs-12">
		<?php if( ce('openapi.apiset' ,$item) ) { ?>
		<input type="text" name="displayorder" id="displayorder" class="form-control" value="<?php  echo $item['displayorder'];?>" />
		<span class='help-block'>数字越大，排名越靠前,如果为空，默认排序方式为创建时间</span>
		<?php  } else { ?>
		<div class='form-control-static'><?php  echo $item['displayorder'];?></div>
		<?php  } ?>
	</div>
</div>

 <div class="form-group">
	<label class="col-sm-2 control-label must">标题</label>
	<div class="col-sm-9 col-xs-12 ">
		<?php if( ce('openapi.apiset' ,$item) ) { ?>
		<input type="text" id='catename' name="catename" class="form-control" value="<?php  echo $item['catename'];?>" data-rule-required="true" />
		<span class='help-block'>用于区分各个API</span>
		<?php  } else { ?>
		<div class='form-control-static'><?php  echo $item['catename'];?></div>
		<?php  } ?>
	</div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label must">API类型</label>
    <div class="col-sm-8">
        <?php if( ce('openapi.terminal' ,$item) ) { ?>
        <select class="form-control" name="apitype" required>
			<option value="0" <?php  if($item['apitype'] == '0') { ?> selected<?php  } ?>>请选择指令类型</option>
            <option value="1" <?php  if($item['apitype'] == '1') { ?> selected<?php  } ?>>聚合数据商标查询接口</option>
            <!-- <option value="2" <?php  if($item['apitype']== '2') { ?> selected<?php  } ?>>服务端查询指令</option>
            <option value="3" <?php  if($item['apitype'] == '3') { ?> selected<?php  } ?>>服务端生产指令</option>
			<option value="4" <?php  if($item['apitype'] == '4') { ?> selected<?php  } ?>>服务端设置指令</option> -->
        </select>
	<span class='help-block'>务必选择API类型</span>
        <?php  } else { ?>
        <div class="form-control-static">
            <?php  if(is_array($category)) { foreach($category as $c) { ?>
            <?php  if($item['cateid']==$c['id']) { ?><?php  echo $c['catename'];?><?php  } ?>
            <?php  } } ?>
        </div>
        <?php  } ?>
    </div>
	
</div>

<div class="form-group">
	<label class="col-sm-2 control-label must">AppKey</label>
	<div class="col-sm-9 col-xs-12 ">
		<?php if( ce('openapi.apiset' ,$item) ) { ?>
		<input type="text" id='appKey' name="appKey" class="form-control" value="<?php  echo $item['appKey'];?>" data-rule-required="true" />
		<span class='help-block'>用于区分各个API</span>
		<?php  } else { ?>
		<div class='form-control-static'><?php  echo $item['appKey'];?></div>
		<?php  } ?>
	</div>
</div>





<!-- <div class="form-group">
	<label class="col-sm-2 control-label">是否首页推荐</label>
	<div class="col-sm-9 col-xs-12">
		<?php if( ce('openapi.tertype' ,$item) ) { ?>
		<label class="radio-inline">
			<input type="radio" name='isrecommand' value="1" <?php  if($item['isrecommand']==1) { ?>checked<?php  } ?> /> 是
		</label>
		<label class="radio-inline">
			<input type="radio" name='isrecommand' value="0" <?php  if(empty($item['isrecommand'])) { ?>checked<?php  } ?> /> 否
		</label>
		<?php  } else { ?>
		<div class='form-control-static'><?php  if(empty($item['isrecommand'])) { ?>否<?php  } else { ?>是<?php  } ?></div>
		<?php  } ?>
	</div>
</div> -->

<div class="form-group">
	<label class="col-sm-2 control-label">是否启用</label>
	<div class="col-sm-9 col-xs-12">
		<?php if( ce('openapi.tertype' ,$item) ) { ?>
		<label class='radio-inline'>
			<input type='radio' name='status' value='1' <?php  if($item['status']==1) { ?>checked<?php  } ?> /> 是
		</label>
		<label class='radio-inline'>
			<input type='radio' name='status' value='0' <?php  if(empty($item['status'])) { ?>checked<?php  } ?> /> 否
		</label>
		<?php  } else { ?>
		<div class='form-control-static'><?php  if(empty($item['status'])) { ?>否<?php  } else { ?>是<?php  } ?></div>
		<?php  } ?>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-9 col-xs-12">
		<?php if( ce('openapi.tertype' ,$item) ) { ?>
		<input type="submit" value="提交" class="btn btn-primary"  />
		<?php  } ?>
		<input type="button" name="back" onclick='history.back()' <?php if(cv('openapi.tertype.add|openapi.tertype.edit')) { ?>style='margin-left:10px;'<?php  } ?> value="返回列表" class="btn btn-default" />
	</div>
</div>

</form>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>