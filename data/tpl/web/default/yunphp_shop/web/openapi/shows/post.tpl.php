<?php defined('IN_IA') or exit('Access Denied');?>    <form action="" action="" method="post"  class="form-horizontal form-validate" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php  echo $shows['id'];?>" />
		<input type="hidden" name="recordid" value="<?php  echo $_GPC['recordid'];?>" />
		<input type="hidden" name="openid" value="<?php  echo $_GPC['openid'];?>" />
		<input type="hidden" name="r" value="openapi.shows.<?php  if(empty($shows['id'])) { ?>add<?php  } else { ?>edit<?php  } ?>" />
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title"><?php  if(!empty($shows['id'])) { ?>编辑<?php  } else { ?>添加<?php  } ?>客户联系记录</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label must">联系方式</label>
                    <div class="col-sm-9 col-xs-12">
						<?php if( ce('openapi.record' ,$level) ) { ?>
                        <select class='form-control' name='severtype'>
							<option value='0' <?php  if($shows['severtype']==0) { ?>selected<?php  } ?>>请选择联系方式</option>
							<option value='1' <?php  if($shows['severtype']==1) { ?>selected<?php  } ?>>电话</option>
							<option value='2' <?php  if($shows['severtype']==2) { ?>selected<?php  } ?>>在线客服</option>
							<option value='3' <?php  if($shows['severtype']==3) { ?>selected<?php  } ?>>微信</option>
							<option value='4' <?php  if($shows['severtype']==4) { ?>selected<?php  } ?>>QQ</option>
						</select>
						<?php  } else { ?>
						<div class='form-control-static'><?php  echo $shows['levelname'];?></div>
						<?php  } ?> 
                    </div>
                </div>
				
                <div class="form-group">
                    <label class="col-sm-2 control-label must">联系号码</label>
                          <div class="col-sm-9 col-xs-12">
							<?php if( ce('openapi.record' ,$level) ) { ?>
                                                            
                        <input type="text" name="tel" class="form-control" value="<?php  echo $shows['tel'];?>" />
                        
                        
						<?php  } else { ?>
						<div class='form-control-static'><?php  echo $shows['tel'];?></div>
						<?php  } ?> 
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-2 control-label must">联系人</label>
                          <div class="col-sm-9 col-xs-12">
							<?php if( ce('openapi.record' ,$level) ) { ?>
                                                            
                        <input type="text" name="name" class="form-control" value="<?php  echo $shows['name'];?>" />
                        
                        
						<?php  } else { ?>
						<div class='form-control-static'><?php  echo $shows['name'];?></div>
						<?php  } ?> 
                    </div>
                </div>
				
                <div class="form-group">
                    <label class="col-sm-2 control-label must">内容概要</label>
                    <div class="col-sm-9 col-xs-12">
                       <?php if( ce('openapi.record' ,$level) ) { ?>
                       
                        
						<textarea name="content" class="form-control" ><?php  echo $shows['content'];?></textarea>
                        
                        
						<?php  } else { ?>
						<div class='form-control-static'><?php  echo $shows['content'];?></div>
						<?php  } ?> 
                    </div>
                </div>
				
                <div class="form-group">
                    <label class="col-sm-2 control-label">备注</label>
                    <div class="col-sm-9 col-xs-12">
                      <?php if( ce('openapi.record' ,$level) ) { ?>
                        
                        <input type="text" name="note" class="form-control" value="<?php  echo $shows['note'];?>" />
                        
                        
						<?php  } else { ?>
						<div class='form-control-static'><?php  echo $shows['note'];?></div>
						<?php  } ?> 
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-2 control-label must">状态</label>
                    <div class="col-sm-9 col-xs-12">
						<?php if( ce('openapi.record' ,$level) ) { ?>
                        <select class='form-control' name='status'>
							<option value='0' <?php  if($shows['status']==0) { ?>selected<?php  } ?>>请选择后续处理方式</option>
							<option value='1' <?php  if($shows['status']==1) { ?>selected<?php  } ?>>有意向需跟进</option>
							<option value='2' <?php  if($shows['status']==2) { ?>selected<?php  } ?>>需后续上门拜访</option>
							<option value='3' <?php  if($shows['status']==3) { ?>selected<?php  } ?>>无意向</option>
							<option value='4' <?php  if($shows['status']==4) { ?>selected<?php  } ?>>捣乱的</option>
							<option value='5' <?php  if($shows['status']==5) { ?>selected<?php  } ?>>电话未接通或未回复</option>
							<option value='6' <?php  if($shows['status']==6) { ?>selected<?php  } ?>>其他</option>
						</select>
						<?php  } else { ?>
						<div class='form-control-static'><?php  echo $shows['levelname'];?></div>
						<?php  } ?> 
                    </div>
                </div>
				
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit">提交</button>
                <button data-dismiss="modal" class="btn btn-default" type="button">取消</button>
            </div>
        </div>
</form>
