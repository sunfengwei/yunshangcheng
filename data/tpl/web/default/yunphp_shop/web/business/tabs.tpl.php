<?php defined('IN_IA') or exit('Access Denied');?><div class='menu-header'><?php  echo $this->plugintitle?></div>

<?php if(cv('business.partner|business.level')) { ?>
<ul>
    <?php if(cv('business.partner')) { ?><li <?php  if($_W['routes']=='business.partner') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('business/partner')?>">会员管理</a></li><?php  } ?>
    <?php if(cv('business.level')) { ?><li <?php  if($_W['routes']=='business.level') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('business/level')?>">会员等级</a></li><?php  } ?>
</ul>
<?php  } ?>

<style type='text/css'>
    .business-list a {
        position: relative;
    }
    .business-list span  {

        float:right;margin-right:20px;
    }
</style>


<?php  $totals = $this->model->getgoodsTotals()?>
<?php  $ordertotals = $this->model->getorderTotals()?>
<?php if(cv('goods')) { ?>
<div class='menu-header'>服务管理</div>
<ul class='business-list'>
    <li <?php  if($_GPC['goodsfrom'] == 'sale') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods',array('goodsfrom'=>'sale'))?>">出售中<span><?php  echo $totals['sale'];?></span></a></li>
     <li <?php  if($_GPC['goodsfrom'] == 'out') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods',array('goodsfrom'=>'out'))?>">已售罄<span  class='text-danger'><?php  echo $totals['out'];?></span></a></li>
    <li <?php  if($_GPC['goodsfrom'] == 'stock') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods',array('goodsfrom'=>'stock'))?>">仓库中<span><?php  echo $totals['stock'];?></span></a></li>
    <li <?php  if($_GPC['goodsfrom'] == 'cycle') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods',array('goodsfrom'=>'cycle'))?>">回收站<span  class='text-default'><?php  echo $totals['cycle'];?></span></a></li>
     <?php if(cv('goods.category')) { ?><li <?php  if($_W['action'] == 'business.goods.category') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/category')?>">服务分类</a></li><?php  } ?>
    <?php if(cv('goods.group')) { ?><li <?php  if($_GPC['r'] == 'business.goods.group' || $_GPC['r'] == 'business.goods.group.edit') { ?>class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/group')?>">服务分组</a></li><?php  } ?>
    <li <?php  if($_W['action'] == 'business.goods.label') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/label')?>">标签管理</a></li>
</ul>

<div class='menu-header'>订单管理</div>
<ul class='business-list'>
    <li <?php  if($_GPC['order'] == 'business') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/order',array('order'=>'business'))?>">工商服务<span><?php  echo $ordertotals['business'];?></span></a></li>
     <li <?php  if($_GPC['order'] == 'act') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/order',array('order'=>'act'))?>">代理记账<span  class='text-danger'><?php  echo $ordertotals['act'];?></span></a></li>
    <li <?php  if($_GPC['order'] == 'visual') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/order',array('order'=>'visual'))?>">视觉设计<span><?php  echo $ordertotals['visual'];?></span></a></li>
    <li <?php  if($_GPC['order'] == 'intellectual') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/order',array('order'=>'intellectual'))?>">知识产权<span  class='text-default'><?php  echo $ordertotals['intellectual'];?></span></a></li>
    <li <?php  if($_GPC['order'] == 'software') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/order',array('order'=>'software'))?>">软件开发<span  class='text-default'><?php  echo $ordertotals['software'];?></span></a></li>
    <li <?php  if($_GPC['order'] == 'marketing') { ?> class="active" <?php  } ?>><a href="<?php  echo webUrl('business/goods/order',array('order'=>'marketing'))?>">营销推广<span  class='text-default'><?php  echo $ordertotals['marketing'];?></span></a></li>
    
</ul>
<?php  } ?>





<?php if(cv('business.cover|business.notice|business.set')) { ?>
<div class="menu-header">设置</div>
<ul>
    <?php if(cv('business.cover')) { ?><li <?php  if($_W['routes']=='business.cover') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('business/cover')?>">入口设置</a></li><?php  } ?>
    <?php if(cv('business.notice')) { ?><li <?php  if($_W['routes']=='business.notice') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('business/notice')?>">通知设置</a></li><?php  } ?>
    <?php if(cv('business.set')) { ?><li <?php  if($_W['routes']=='business.set') { ?>class="active"<?php  } ?>><a href="<?php  echo webUrl('business/set')?>">基础设置</a></li><?php  } ?>
</ul>

<?php  } ?>