<?php defined('IN_IA') or exit('Access Denied');?><div class='fui-content params-block'>
    <div class="inner">

        <div class="param-item param-type">
            <div class="fui-title">活动类型</div>
            <div class="fui-cell-group fui-cell-click">
                <div class="fui-cell submit-params" data-value="21">
                    <div class="fui-cell-text">线下活动</div>
                    <div class="fui-cell-remark">选择</div>
                </div>
                <div class="fui-cell submit-params" data-value="22">
                    <div class="fui-cell-text">线上活动</div>
                    <div class="fui-cell-remark">选择</div>
                </div>
            </div>
        </div>

        <div class="param-item param-cate">
            <div class="fui-title">活动分类(多选)</div>
            <div class="cate-list" data-catlevel="<?php  echo $catlevel;?>">
                <div class="item" data-level="1">
                    <?php  if(is_array($allcategory['parent'])) { foreach($allcategory['parent'] as $category_item) { ?>
                        <nav data-id="<?php  echo $category_item['id'];?>"><?php  echo $category_item['name'];?></nav>
                    <?php  } } ?>
                </div>
                <?php  if($catlevel>=2) { ?>
                <div class="item" data-level="2">
                        <div class="empty">请选择上级</div>
                    </div>
                <?php  } ?>
                <?php  if($catlevel>=3) { ?>
                    <div class="item" data-level="3">
                        <div class="empty">请选择上级</div>
                    </div>
                <?php  } ?>
            </div>
            <div class="btn btn-success btn-sm block btn-choose-cate">选择此分类</div>
            <div class="fui-title">已选择分类</div>
            <div class="small-block">
                <?php  if(is_array($category)) { foreach($category as $category_item) { ?>
                    <?php  if(is_array($cates) &&  in_array($category_item['id'],$cates)) { ?>
                        <span class="item" data-cateid="<?php  echo $category_item['id'];?>"><?php  echo $category_item['name'];?> <i class="icon icon-close"></i></span>
                    <?php  } ?>
                <?php  } } ?>
            </div>
        </div>

        <div class="param-item param-price" >
		
		<div class="fui-cell-group">
            <div class="fui-cell ">
                <div class="fui-cell-label ">可否退款</div>
                <div class="fui-cell-info"><input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right" id="cannotrefund" <?php  if(empty($item['cannotrefund'])) { ?>checked="checked"<?php  } ?>></div>
            </div>
        </div>
		
		<div class="fui-title">活动费用(可添加多项)</div>
		<?php  if(empty($params)) { ?>
		<table class="table">
		<tbody id="param-items">
		
			<div class="fui-cell-group" >
			<div class="fui-cell">
                <div class="fui-cell-label">费用名称</div>
                <div class="fui-cell-info">
                    <input type="text" placeholder="请输入费用名称" class="fui-input param-child" value="<?php echo empty($item['marketprice'])?默认:$item['marketprice']?>" id="price_title" />
                </div>
            </div>
			<div class="fui-cell">
                <div class="fui-cell-label">原价</div>
                <div class="fui-cell-info">
                    <input type="number" placeholder="原价仅用于展示，实际价格按照现价计算" class="fui-input param-child" value="<?php echo empty($item['marketprice'])?0:$item['marketprice']?>" id="price_productprice" />
                </div>
				
            </div>
			<div class="fui-title">原价仅用于展示，实际价格按照现价计算</div>
            <div class="fui-cell">
                <div class="fui-cell-label">现价</div>
                <div class="fui-cell-info">
                    <input type="number" placeholder="实际交易价格，免费活动请留空或填写0" class="fui-input param-child" value="<?php echo empty($item['marketprice'])?0:$item['marketprice']?>" id="marketprice" />
                </div>
				
            </div>
			<div class="fui-title">实际交易价格，免费活动请留空或填写0</div>
			<div class="fui-cell">
                <div class="fui-cell-label">票数</div>
                <div class="fui-cell-info">
                    <input type="number" placeholder="此规格可以提供的票数，0为不限制。" class="fui-input param-child" value="<?php echo empty($item['marketprice'])?0:$item['marketprice']?>" id="marketprice" />
                </div>
				
            </div>
			<div class="fui-title">此规格可以提供的票数，0为不限制。</div>
			</div>
		
		</tbody>
			

         
        <tbody>
            <tr>
                
                <td colspan="3">
                    <a href="javascript:;" id='add-param' onclick="addParam()" class="btn btn-default"  title="添加费用项"><i class='fa fa-plus'></i> 添加费用项</a>
                </td>
            </tr>
        </tbody>
        
    </table>
		<?php  } else { ?>
			
			
			    <table class="table">
        <tbody id="param-items">
            <?php  if(is_array($params)) { foreach($params as $p) { ?>
            <tr>
               
                <td>
                    
                    <input name="param_title[]" type="text" class="form-control param_title" value="<?php  echo $p['title'];?>"/>
                    
                    <input name="param_id[]" type="hidden" class="form-control" value="<?php  echo $p['id'];?>"/>
                </td>
                <td>
                
                    <input name="param_value[]" type="text" class="form-control param_value" value="<?php  echo $p['value'];?>"/>
                
                </td>
	<td>
                   
                    <a href="javascript:;" class='btn btn-default btn-sm' onclick="deleteParam(this)" title="删除"><i class='fa fa-remove'></i></a>
                
                </td>
            </tr>
            <?php  } } ?>
        </tbody>
         
        <tbody>
            <tr>
                
                <td colspan="3">
                    <a href="javascript:;" id='add-param' onclick="addParam()" class="btn btn-default"  title="添加费用项"><i class='fa fa-plus'></i> 添加费用项</a>
                </td>
            </tr>
        </tbody>
        
    </table>
	<?php  } ?>
 

<script language="javascript">
    
    function addParam() {
        var url = "<?php  echo mobileUrl('events/member/goods/tpl/',array('tpl'=>'param'))?>";
//        return false;
        $.ajax({
            "url": url,
            success: function(data) {
                $('#param-items').append(data);
            }
        });
        return;
    }
    function deleteParam(o) {
        $(o).parent().parent().remove();
    }
</script>
		</div>

        <div class="param-item param-prop">
            <div class="fui-title">活动属性</div>
            <div class="fui-cell-group">
                <div class="fui-cell ">
                    <div class="fui-cell-label ">推荐活动</div>
                    <div class="fui-cell-info">
                        <input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right param-child" id="isrecommand" data-text="推荐" <?php  if(!empty($item['isrecommand'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                </div>
                <div class="fui-cell ">
                    <div class="fui-cell-label ">新上活动</div>
                    <div class="fui-cell-info">
                        <input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right param-child" id="isnew" data-text="新品" <?php  if(!empty($item['isnew'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                </div>
                <div class="fui-cell ">
                    <div class="fui-cell-label ">热卖活动</div>
                    <div class="fui-cell-info">
                        <input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right param-child" id="ishot" data-text="热卖" <?php  if(!empty($item['ishot'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                </div>
                <!-- <div class="fui-cell ">
                    <div class="fui-cell-label ">包邮商品</div>
                    <div class="fui-cell-info">
                        <input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right param-child" id="issendfree" data-text="包邮" <?php  if(!empty($item['issendfree'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                </div> -->
            </div>
        </div>

        <div class="param-item param-viewlevel">
            <div class="fui-title">可浏览等级(多选)</div>
            <div class="fui-list-group">
                <?php  if(is_array($levels)) { foreach($levels as $level_item) { ?>
                    <div class="fui-list bindclick">
                        <div class="fui-list-media">
                            <input type="checkbox" class="param-child fui-radio fui-radio-success" name="viewlevel" value="<?php  echo $level_item['id'];?>" data-text="<?php  echo $level_item['levelname'];?>" <?php  if(!empty($level_item['checked_view'])) { ?>checked="checked"<?php  } ?> />
                        </div>
                        <div class="fui-list-inner">
                            <div class="subtitle"><?php  echo $level_item['levelname'];?></div>
                        </div>
                    </div>
                <?php  } } ?>
            </div>
        </div>

        <div class="param-item param-viewgroup">
            <div class="fui-title">可浏览分组(多选)</div>
            <div class="fui-list-group">
                <?php  if(is_array($groups)) { foreach($groups as $group_item) { ?>
                    <div class="fui-list bindclick">
                        <div class="fui-list-media">
                            <input type="checkbox" class="param-child fui-radio fui-radio-success" name="viewgroup" value="<?php  echo $group_item['id'];?>" data-text="<?php  echo $group_item['groupname'];?>" <?php  if(!empty($group_item['checked_view'])) { ?>checked="checked"<?php  } ?> />
                        </div>
                        <div class="fui-list-inner">
                            <div class="subtitle"><?php  echo $group_item['groupname'];?></div>
                        </div>
                    </div>
                <?php  } } ?>
            </div>
        </div>

        <div class="param-item param-buylevel">
            <div class="fui-title">可购买等级(多选)</div>
            <div class="fui-list-group">
                <?php  if(is_array($levels)) { foreach($levels as $level_item) { ?>
                <div class="fui-list bindclick">
                    <div class="fui-list-media">
                        <input type="checkbox" class="param-child fui-radio fui-radio-success" name="viewlevel" value="<?php  echo $level_item['id'];?>" data-text="<?php  echo $level_item['levelname'];?>" <?php  if(!empty($level_item['checked_buy'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                    <div class="fui-list-inner">
                        <div class="subtitle"><?php  echo $level_item['levelname'];?></div>
                    </div>
                </div>
                <?php  } } ?>
            </div>
        </div>
        <div class="param-item param-buygroup">
            <div class="fui-title">可购买分组(多选)</div>
            <div class="fui-list-group">
                <?php  if(is_array($groups)) { foreach($groups as $group_item) { ?>
                <div class="fui-list bindclick">
                    <div class="fui-list-media">
                        <input type="checkbox" class="param-child fui-radio fui-radio-success" name="viewgroup" value="<?php  echo $group_item['id'];?>" data-text="<?php  echo $group_item['groupname'];?>" <?php  if(!empty($group_item['checked_buy'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                    <div class="fui-list-inner">
                        <div class="subtitle"><?php  echo $group_item['groupname'];?></div>
                    </div>
                </div>
                <?php  } } ?>
            </div>
        </div>

        <div class="param-item param-diypage">
            <div class="fui-title">自定义模板</div>
            <div class="fui-cell-group fui-cell-click">
                <div class="fui-cell submit-params" data-id="0">
                    <div class="fui-cell-text">默认模板</div>
                    <div class="fui-cell-remark">选择</div>
                </div>
                <?php  if(is_array($diypage)) { foreach($diypage as $diypage_item) { ?>
                    <div class="fui-cell submit-params" data-id="<?php  echo $diypage_item['id'];?>">
                        <div class="fui-cell-text"><?php  echo $diypage_item['name'];?></div>
                        <div class="fui-cell-remark">选择</div>
                    </div>
                <?php  } } ?>
            </div>
        </div>

        <div class="param-item param-diyform">
            <div class="fui-title">自定义表单</div>
            <div class="fui-cell-group fui-cell-click">
                <div class="fui-cell submit-params" data-id="0">
                    <div class="fui-cell-text">不设置</div>
                    <div class="fui-cell-remark">确定</div>
                </div>
            </div>
            <div class="fui-cell-group fui-cell-click">
                <?php  if(is_array($diyform)) { foreach($diyform as $diyform_item) { ?>
                <div class="fui-cell submit-params" data-id="<?php  echo $diyform_item['id'];?>">
                    <div class="fui-cell-text"><?php  echo $diyform_item['title'];?></div>
                    <div class="fui-cell-remark">选择</div>
                </div>
                <?php  } } ?>
            </div>
        </div>

        <div class="param-item param-dispatch">
            <div class="fui-title">运费设置</div>
            <div class="fui-list-group">
                <div class="fui-list bindclick" data-show="dispatchtype_0" data-hide="dispatchtype_1">
                    <div class="fui-list-media">
                        <input type="radio" class="fui-radio fui-radio-success" name="dispatchtype" value="0" id="dispatchtype_0" <?php  if(empty($item['dispatchtype'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                    <label class="fui-list-inner">
                        <div class="subtitle">运费模板</div>
                    </label>
                </div>
                <div class="fui-list bindclick" data-show="dispatchtype_1" data-hide="dispatchtype_0">
                    <div class="fui-list-media">
                        <input type="radio" class="fui-radio fui-radio-success" name="dispatchtype" value="1" <?php  if(!empty($item['dispatchtype'])) { ?>checked="checked"<?php  } ?> />
                    </div>
                    <label class="fui-list-inner">
                        <div class="subtitle">统一邮费</div>
                    </label>
                </div>
            </div>
            <div class="dispatchtype_0" style="display: <?php  if(empty($item['dispatchtype'])) { ?>block<?php  } else { ?>none<?php  } ?>;">
                <div class="fui-title">运费模板</div>
                <div class="fui-list-group">
                    <div class="fui-list bindclick">
                        <div class="fui-list-media">
                            <input type="radio" class="fui-radio fui-radio-success" name="dispatchid" value="0" <?php  if(empty($item['dispatchid'])) { ?>checked="checked"<?php  } ?> />
                        </div>
                        <div class="fui-list-inner">
                            <div class="subtitle">默认模板</div>
                        </div>
                    </div>
                    <?php  if(is_array($dispatch_data)) { foreach($dispatch_data as $dispatch_item) { ?>
                        <div class="fui-list bindclick">
                            <div class="fui-list-media">
                                <input type="radio" class="fui-radio fui-radio-success" name="dispatchid" value="<?php  echo $dispatch_item['id'];?>" <?php  if($item['dispatchid']==$dispatch_item['id']) { ?>checked="checked"<?php  } ?> />
                            </div>
                            <div class="fui-list-inner">
                                <div class="subtitle"><?php  echo $dispatch_item['dispatchname'];?></div>
                            </div>
                        </div>
                    <?php  } } ?>
                </div>
            </div>
            <div class="dispatchtype_1" style="display: <?php  if(!empty($item['dispatchtype'])) { ?>block<?php  } else { ?>none<?php  } ?>;">
                <div class="fui-title">统一邮费</div>
                <div class="fui-cell-group">
                    <div class="fui-cell">
                        <div class="fui-cell-label">邮费价格</div>
                        <div class="fui-cell-info">
                            <input type="number" placeholder="请输入统一运费" class="fui-input" value="<?php  echo $item['dispatchprice'];?>" id="dispatchprice" />
                        </div>
                        <div class="fui-cell-remark noremark">元</div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="fui-navbar">
        <div class="nav-item btn btn-success submit-params">确定</div>
        <div class="nav-item btn btn-gray cancel-params">取消</div>
    </div>
</div>