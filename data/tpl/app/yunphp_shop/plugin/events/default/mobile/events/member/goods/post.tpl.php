<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('events/common', TEMPLATE_INCLUDEPATH)) : (include template('events/common', TEMPLATE_INCLUDEPATH));?>
<script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="foxui.citydatanew" src="../addons/yunphp_shop/static/js/dist/foxui/js/foxui.citydatanew.min.js?v=<?php  echo time()?>"></script>
<div class='fui-page fui-page-current'>
    <div class="fui-header fui-header-success">
        <div class="fui-header-left">
            <a class="back btn-back">返回</a>
            <a class="btn-cancel cancel-params">取消</a>
        </div>
        <div class="title"><?php  if(!empty($item)) { ?>活动编辑<?php  } else { ?>发布活动<?php  } ?></div>
        <div class="fui-header-right"></div>
    </div>
    <div class='fui-content navbar'>

        <input type="hidden" id="id" value="<?php  echo $item['id'];?>" />
        <input type="hidden" id="type" value="<?php  echo $item['type'];?>" />
        <input type="hidden" id="cates" value="<?php  echo $item['cates'];?>" />
        <input type="hidden" id="hasoption" value="<?php  echo $item['hasoption'];?>" />
        <input type="hidden" id="totalcnf" value="<?php  echo $item['totalcnf'];?>" />
        <input type="hidden" id="showlevels" value="<?php  echo $item['showlevels'];?>" />
        <input type="hidden" id="showgroups" value="<?php  echo $item['showgroups'];?>" />
        <input type="hidden" id="buylevels" value="<?php  echo $item['buylevels'];?>" />
        <input type="hidden" id="buygroups" value="<?php  echo $item['buygroups'];?>" />

        <input type="hidden" id="diypage" value="<?php  echo $item['diypage'];?>" />
        <input type="hidden" id="diyformid" value="<?php  echo $item['diyformid'];?>" />

        <?php  if(!empty($merch_user)) { ?>
            <div class="fui-title">活动所属: <?php  echo $merch_user['merchname'];?></div>
        <?php  } ?>

        <div class="fui-cell-group">
            <div class="fui-cell fui-cell-textarea">
                <div class="fui-cell-label ">活动主题</div>
                <div class="fui-cell-info">
                    <textarea rows="3" placeholder="请输入活动标题" id="title"><?php  echo $item['title'];?></textarea>
                </div>
            </div>
			
			<div class="fui-cell-group">
            <div class="fui-cell borderb noactive">
                <div class="fui-cell-info">
                    <ul class="fui-images" id="thumbs">
                        <?php  if(is_array($piclist)) { foreach($piclist as $picitem) { ?>
                            <li style="background-image:url(<?php  echo tomedia($picitem)?>)" class="image" data-filename="<?php  echo $picitem;?>">
                                <span class="image-remove">×</span>
                            </li>
                        <?php  } } ?>
                    </ul>
                    <div class="fui-uploader" data-count="<?php  echo count($piclist)?>" data-max="10" data-name="images[]" <?php  if(count($piclist)>=10) { ?>style="display: none;"<?php  } ?>>
                        <input type="file" name='imgFile0' id='imgFile0' multiple="" accept="image/*" >
                    </div>
                </div>
            </div>
            <div class="fui-title">首张图片为活动主图(拖拽可排序)，在活动头部幻灯片展示</div>
        
		
			<div class="fui-cell">
                <div class="fui-cell-label">副标题</div>
                <div class="fui-cell-info">
					<textarea rows="3" placeholder="介绍活动的特色、卖点等，将在活动标题下以高亮字体展示。" id="subtitle"><?php  echo $item['subtitle'];?></textarea>
                    
                </div>
            </div>
		</div>
			
			<div class="fui-cell">
				<div class="fui-cell-label">开始时间</div>
				<div class="fui-cell-info"><input type="text"  class='fui-input'  id='atimestart' name='atimestart' placeholder="请选择活动开始时间"  value="<?php  echo date('Y-m-d H:i',$atimestart)?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label">结束时间</div>
				<div class="fui-cell-info"><input type="text"  class='fui-input'  id='atimeend' name='atimeend' placeholder="请选择活动结束时间"  value="<?php  echo date('Y-m-d H:i',$atimeend)?>" readonly/></div>
			</div>
			
			<!-- <div class='fui-cell'>
				<div class='fui-cell-label'>所在地区</div>
				<div class='fui-cell-info'><input type="text" id='city'  name='city' data-value="<?php  if(!empty($show_data) && !empty($address)) { ?><?php  echo $address['datavalue'];?><?php  } ?>" value="<?php  if(!empty($show_data) && !empty($address)) { ?><?php  echo $address['province'];?> <?php  echo $address['city'];?> <?php  echo $address['area'];?><?php  } ?>" placeholder="所在地区"  class="fui-input" readonly=""/></div>
			</div> -->
			<div class="fui-cell">
				<div class="fui-cell-label ">所在城市</div>
				<div class="fui-cell-info"><input type="text"  class='fui-input'  id='city' name='city' placeholder="请选择城市"  value="<?php  if(!empty($show_data)) { ?><?php  echo $item['province'];?> <?php  echo $item['city'];?> <?php  echo $item['area'];?><?php  } ?>" data-value="<?php  if(!empty($show_data)) { ?><?php  echo $item['datavalue'];?><?php  } ?>" readonly=""/></div>
			</div>
			<div class='fui-cell'>
				<div class='fui-cell-label'>详细地址</div>
				<div class='fui-cell-info'><input type="text" id='address' name='address' value="<?php  echo $item['address'];?>" placeholder='街道，楼牌号等'  class="fui-input"/></div>
			</div>
			
			<div class='fui-cell'>
				<div class='fui-cell-label'>联系电话</div>
				<div class='fui-cell-info'><input type="text" id='tel' name='tel' value="<?php  echo $item['tel'];?>" placeholder='请输入主办方联系电话'  class="fui-input"/></div>
			</div>
			
            <!-- <div class="fui-cell">
                <div class="fui-cell-label">单位</div>
                <div class="fui-cell-info">
                    <input type="text" placeholder="请输入商品单位" class="fui-input" value="<?php  echo $item['unit'];?>" id="unit" />
                </div>
            </div> -->
        </div>
		
		<div class="fui-cell-group">
            <div class="fui-cell fui-cell-textarea">
                <div class="fui-cell-label ">活动详情文字描述</div>
                <div class="fui-cell-info">
                    <textarea rows="6" placeholder="请详细说明活动的内容、形式、注意事项等，在活动详情栏展示" id="content"><?php  echo $item['content'];?></textarea>
                </div>
            </div>
			<div class="fui-cell borderb noactive">
                <div class="fui-cell-info">
                    <ul class="fui-images" id="thumbs1">
                        <?php  if(is_array($piclist1)) { foreach($piclist1 as $picitem) { ?>
                            <li style="background-image:url(<?php  echo tomedia($picitem)?>)" class="image" data-filename="<?php  echo $picitem;?>">
                                <span class="image-remove">×</span>
                            </li>
                        <?php  } } ?>
                    </ul>
                    <div class="fui-uploader" data-count="<?php  echo count($piclist)?>" data-max="10" data-name="images[]" <?php  if(count($piclist)>=10) { ?>style="display: none;"<?php  } ?>>
                        <input type="file" name='imgFile1' id='imgFile1' multiple="" accept="image/*" >
                    </div>
                </div>
            </div>
            <div class="fui-title">活动详情图片(拖拽可排序)，在活动详情文字下面展现，最多10张</div>
		</div>
        
		
		

        <div class="fui-cell-group fui-cell-click" id="thumbs3">
            <?php  if(empty($item['id'])) { ?>
                <div class="fui-cell check-param" data-action="type" id="sss">
                    <div class="fui-cell-label">活动类型</div>
                    <div class="fui-cell-info"><?php  echo $type_title;?></div>
                    <div class="fui-cell-remark"></div>
                </div>
            <?php  } else { ?>
                <div class="fui-cell noactive">
                    <div class="fui-cell-label">活动类型</div>
                    <div class="fui-cell-info"><?php  echo $type_title;?></div>
                    <div class="fui-cell-remark noremark">不可修改</div>
                </div>
            <?php  } ?>
            <?php  if($catlevel!=-1) { ?>
                <div class="fui-cell check-param" data-action="cate">
                    <div class="fui-cell-label">活动分类</div>
                    <div class="fui-cell-info"><?php  echo $category_title;?></div>
                    <div class="fui-cell-remark"></div>
                </div>
            <?php  } ?>
            
        </div>

        
        <div class="fui-cell-group fui-cell-click">
            <div class="fui-cell check-param" data-action="price">
                <div class="fui-cell-text">报名费用</div>
                <div class="fui-cell-remark">点击设置</div>
            </div>
        </div>

		
		

        <div class="fui-cell-group fui-cell-click">
            <div class="fui-cell noactive">
                <div class="fui-cell-label ">会员折扣</div>
                <div class="fui-cell-info">建议开启<input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right" id="isnodiscount" <?php  if(empty($item['isnodiscount'])) { ?>checked="checked"<?php  } ?>></div>
            </div>
			<div class="fui-title">用户根据本站会员等级享受折扣优惠，开启有利于活动推广。</div>
            <div class="fui-cell noactive">
                <div class="fui-cell-label ">付费推广</div>
                <div class="fui-cell-info">建议开启<input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right" id="nocommission" <?php  if(empty($item['nocommission'])) { ?>checked="checked"<?php  } ?>></div>
            </div>
			<div class="fui-title">开启后本站合伙人推广本活动将获得奖励，有利于您的活动推广，推广费用将在您的票款中扣除。</div>
            <?php  if(p('diyform')) { ?>
                <?php  if(!empty($diyform) || $item['diyformtype']==2) { ?>
                    <?php  if($item['diyformtype']==2) { ?>
                        <div class="fui-cell">
                            <div class="fui-cell-label">表单选择</div>
                            <div class="fui-cell-info">商品自定义</div>
                            <div class="fui-cell-remark noremark">PC可编辑</div>
                        </div>
                    <?php  } else if(!empty($diyform)) { ?>
                        <div class="fui-cell check-param" data-action="diyform">
                            <div class="fui-cell-label">表单选择</div>
                            <div class="fui-cell-info"><?php  echo $diyform_title;?></div>
                            <div class="fui-cell-remark"></div>
                        </div>
                    <?php  } ?>
                <?php  } ?>
            <?php  } ?>
            <?php  if($diypage) { ?>
                <div class="fui-cell check-param" data-action="diypage">
                    <div class="fui-cell-label">模板选择</div>
                    <div class="fui-cell-info"><?php  echo $diypage_title;?></div>
                    <div class="fui-cell-remark"></div>
                </div>
            <?php  } ?>
        </div>

        <div class="fui-cell-group">
            <div class="fui-cell ">
                <div class="fui-cell-label ">提供发票</div>
                <div class="fui-cell-info"><input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right" id="invoice" <?php  if(!empty($item['invoice'])) { ?>checked="checked"<?php  } ?>></div>
            </div>
        </div>


        <!-- <div class="fui-cell-group fui-cell-click">
            <div class="fui-cell check-param" data-action="dispatch">
                <div class="fui-cell-label">运费设置</div>
                <div class="fui-cell-info"><?php  echo $dispatch_title;?></div>
                <div class="fui-cell-remark"></div>
            </div>
        </div> -->

        <div class="fui-cell-group fui-cell-click">
            <div class="fui-cell noactive">
                <div class="fui-cell-label ">立即发布</div>
                <div class="fui-cell-info"><input type="checkbox" class="fui-switch fui-switch-small fui-switch-success pull-right" id="status" <?php  if(!empty($item['status'])) { ?>checked="checked"<?php  } ?>></div>
            </div>
        </div>

        <!-- <div class="fui-cell-group">
            <div class="fui-cell">
                <div class="fui-cell-label">商品排序</div>
                <div class="fui-cell-info">
                    <input type="number" placeholder="数字越大，排名越靠前" class="fui-input" value="<?php  echo $item['displayorder'];?>" id="displayorder" />
                </div>
            </div>
        </div> -->

        <div class="fui-title center">更多设置请至PC端后台</div>

    </div>

    <?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('events/member/goods/_tpl/goods_params', TEMPLATE_INCLUDEPATH)) : (include template('events/member/goods/_tpl/goods_params', TEMPLATE_INCLUDEPATH));?>

    <div class="fui-navbar">
        <div class="nav-item btn btn-success btn-submit">发布活动</div>
    </div>

    <script language="javascript">
        require(['../addons/yunphp_shop/plugin/events/static/js/member/goods/goods-detail.js'],function(modal){
            modal.initDetail({
			new_area:<?php  echo $new_area?>,
			category: <?php  echo $category_json;?>
			});
        });
    </script>
</div>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>