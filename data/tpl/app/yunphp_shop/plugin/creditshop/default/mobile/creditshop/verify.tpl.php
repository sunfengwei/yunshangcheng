<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<style>
	.creditshop-qrcode,.exchange_eno{text-align: center;padding:1rem 0.5rem;}
</style>
<div class='fui-page fui-page-current'>
	<div class="fui-header">
		<div class="fui-header-left">
			<a class="back"></a>
		</div>
		<div class="title">二维码兑换</div>
		<div class="fui-header-right">&nbsp;</div>
	</div>
	<div class='fui-content'>
		<div class="creditshop-qrcode">
			<img src="<?php  echo $qrcode;?>" width="100%" alt="">
			<p>请将此二维码出示给店员</p>
			<div class="btn btn-warning" onclick=" $('.creditshop-qrcode').hide();$('.exchange_eno').show();">无法扫描？</div>
		</div>
		<div class="exchange_eno" style="display: none;">
			<div class="title">兑奖码：<?php  echo $verifycode;?></div>
			<div class="tip">请将兑奖码出示给店员</div>
			<div class="btn btn-danger" onclick=" $('.creditshop-qrcode').show();$('.exchange_eno').hide();">返回二维码</div>
		</div>
	</div>
</div>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>
