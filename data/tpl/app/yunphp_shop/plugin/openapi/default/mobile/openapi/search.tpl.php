<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<div class='fui-page  fui-page-current'>
    <div class="fui-header">
		<div class="fui-header-left">
			<a class="back" onclick='location.back()'></a>
		</div>
		<div class="title"><?php  if(empty($keyword)) { ?>未输入查询商标名称<?php  } else { ?>“<?php  echo $keyword;?>”的查询结果<?php  } ?></div> 
		<div class="fui-header-right">&nbsp;</div>
	</div>

	<div class='fui-content' style='margin-top:5px;'>
	<?php  if(!empty($error_code)) { ?>
		<div class="fui-cell-group">
			<div class="fui-cell must">
				<div class="fui-cell-label">错误信息</div>
				<div class="fui-cell-info"><?php  echo $error_code;?></div>
				<a class="fui-cell-remark external" href="<?php  echo mobileUrl('member/bind')?>"><?php  echo $appkey;?><?php  if(empty($member['mobile'])) { ?>绑定<?php  } else { ?>更换绑定<?php  } ?></a>
			</div>
		</div>
	<?php  } else if(!empty($result_data)) { ?>
		
		<?php  if(is_array($result_data)) { foreach($result_data as $info) { ?>
		<div class="fui-list-group">
			<a class="fui-list" href="#" data-nocache="true">
			<div class="fui-list-media">
					<img class="round avatar" src="http://pic.tmkoo.com/pic.php?s=0&zch=<?php  echo $info['tmImg'];?>" />
				</div>
				<div class="fui-list-inner">
					<div class="title nickname"><?php  echo $info['tmName'];?></div>
				</div>
				<div class="fui-list-angle">
					点击查看商标详细信息
				</div>
			</a>
		</div>
		
			
				
		<div class="fui-cell-group">
			<div class="fui-cell">
				<div class="fui-cell-label ">商标类别</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['intCls'];?>类" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">当前状态</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['currentStatus'];?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">申请人</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['applicantCn'];?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">注册号</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['regNo'];?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">申请日期</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['appDate'];?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">初审公告号</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['announcementIssue'];?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">初审公告日期</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['announcementDate'];?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">注册公告号</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['regIssue'];?>" readonly/></div>
			</div>
			<div class="fui-cell">
				<div class="fui-cell-label ">注册公告日期</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder=""  value="<?php  echo $info['regDate'];?>" readonly/></div>
			</div>
			<a href='tel:13075348345' id='btn-submit' class='btn btn-success block' style='background:#00b7ff'>立即咨询商标专家</a>
				
			
		</div>


		<?php  } } ?>
	<?php  } else { ?>
	<a href='tel:13075348345' id='btn-submit' class='btn btn-success block' style='background:#00b7ff'>您查询的商标尚未有人注册，马上联系商标注册专家，帮您抢注吧~</a>
		
	<?php  } ?>
	

	</div>


</div>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>
