<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<div class='fui-page  fui-page-current'>
    <div class="fui-header">
		<div class="fui-header-left">
			<a class="back" onclick='location.back()'></a>
		</div>
		<div class="title">商标信息查询</div> 
		<div class="fui-header-right">&nbsp;</div>
	</div>

	<div class='fui-content' style='margin-top:5px;'>

		<div class="fui-list-group">
			<a class="fui-list" href="#" data-nocache="true">
				<div class="fui-list-media">
					<img class="round avatar" src="<?php  echo tomedia($member['avatar'])?>" />
				</div>
				<div class="fui-list-inner">
					<div class="title nickname"><?php  echo $member['nickname'];?></div>
				</div>
				<div class="fui-list-angle">
					<div class="angle"></div>
				</div>
			</a>
		</div>

		<?php  if(!empty($wapset['open']) && empty($member['mobile'])) { ?>
		<div class="fui-cell-group">
			<div class="fui-cell must">
				<div class="fui-cell-label">手机号</div>
				<div class="fui-cell-info">请首先绑定手机号后查询</div>
				<a class="fui-cell-remark external" href="<?php  echo $url;?>">绑定</a>
			</div>
		</div>
		<a href="<?php  echo $backurl;?>" class='btn btn-success block' style='background:#00b7ff'>立即绑定手机号查询商标</a>
		<?php  } else { ?>

		<div class="fui-list-group">
			<a class="fui-list" href="#" data-nocache="true">
				<div class="fui-list-media">
					
				</div>
				<div class="fui-list-inner">
					<div class="title nickname"><span style='color:#00b7ff; font-weight: bold;'>与国家商标局数据库同步，免费查询商标</span></div>
				</div>
				<div class="fui-list-angle">
					
				</div>
			</a>
		</div>
			 
		<div class="fui-cell-group">
			<div class="fui-cell">
                <div class="fui-cell-label" style="width: 120px;"><span class="re-g">查询方式</span></div>
                <div class="fui-cell-info">

                    <select id="searchType">
						<option value="1" selected="selected">商标名</option>
						<option value="2" >注册号</option>
						<option value="3" >申请人</option>
						<option value="4" >模糊查询(商标名称/申请人/注册号)</option>
                    </select>
                </div>
                <div class="fui-cell-remark"></div>
            </div>
			<div class="fui-cell must ">
				<div class="fui-cell-label ">关键词</div>
				<div class="fui-cell-info"><input type="text" class='fui-input' id='keyword' name='keyword' placeholder="请输入您要查询的商标名称"  value="<?php  echo $member['keyword'];?>" /></div>
			</div>
			<!-- <div class="fui-cell">
				<div class="fui-cell-label ">所在城市</div>
				<div class="fui-cell-info"><input type="text"  class='fui-input'  id='city' name='city' placeholder="请选择城市"  value="<?php  if(!empty($show_data) && !empty($member) && !empty($member['city'])) { ?><?php  echo $member['province'];?> <?php  echo $member['city'];?><?php  } ?>" data-value="<?php  if(!empty($show_data)) { ?><?php  echo $member['datavalue'];?><?php  } ?>" readonly/></div>
			</div> -->
		</div>
		
		<div class="fui-list-group">
			<a class="fui-list" href="#" data-nocache="true">
				<div class="fui-list-media">
					
				</div>
				<div class="fui-list-inner">
					<div class="title nickname">已有&nbsp;16893+&nbsp;家企业查询</div>
				</div>
				<div class="fui-list-angle">
					
				</div>
			</a>
		</div>
		<a href='#' id='btn-submit' class='btn btn-success block' style='background:#00b7ff'>立即查询</a>
		<?php  } ?>
		
	</div>
	<script language='javascript'>
		require(['../addons/yunphp_shop/plugin/openapi/static/js/trademark.js'], function (modal) {
		  	modal.init({
			    new_area:<?php  echo $new_area?>,
			    returnurl:"<?php  echo $returnurl?>",
			    template_flag: <?php  echo intval($template_flag)?>,
				wapopen: <?php  echo intval($wapset['open'])?>
			});
	});
</script>

</div>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>

