<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('commission/common', TEMPLATE_INCLUDEPATH)) : (include template('commission/common', TEMPLATE_INCLUDEPATH));?>
<div class="fui-page fui-page-current page-commission-down">
    <div class="fui-header">
        <div class="fui-header-left">
            <a class="back"></a>
        </div>
        <div class="title"><?php  echo $this->set['texts']['mydown']?>(<?php  echo $total;?>)</div>
    </div>
    <div class="fui-content navbar">
        <?php  if($this->set['level']>=2) { ?>
        <div class="fui-tab fui-tab-warning" id="tab">
            <a class="active" href="javascript:void(0)" data-tab='level1'><?php  echo $this->set['texts']['c1']?>(<?php  echo $level1;?>)</a>
            <?php  if($this->set['level']>=2) { ?><a href="javascript:void(0)" data-tab='level2'><?php  echo $this->set['texts']['c2']?>(<?php  echo $level2;?>)</a><?php  } ?>
            <?php  if($this->set['level']>=3) { ?><a href="javascript:void(0)" data-tab='level3'><?php  echo $this->set['texts']['c3']?>(<?php  echo $level3;?>)</a><?php  } ?>
        </div>
        <?php  } ?>


        <div class="fui-title">成员信息 <i class="icon icon-favor text-danger"></i> 为已经成为<?php  echo $this->set['texts']['agent']?>的<?php  echo $this->set['texts']['down']?>
            
        </div>
		
		<div class="fui-title">若该会员已经成为<?php  echo $this->set['texts']['agent']?>，其右侧信息为其可取得的佣金总额和现有下级推广员总数。</div>
		<div class="fui-title">若该会员未成为<?php  echo $this->set['texts']['agent']?>，其右侧信息为其消费金额和消费订单数量。</div>
		<div class="fui-title">注意：这里统计的信息包含了<?php  echo $this->set['texts']['down']?>的所有订单信息，包括未付款订单；<?php  echo $this->set['texts']['agent']?>的所有下级客户包含未成为<?php  echo $this->set['texts']['agent']?>的客户，佣金统计包含了所有订单，包括未付款、已退款等的佣金。因此这里的信息仅用来展示团队成员的推广业绩，不作为结算依据。</div>
        <div class="fui-list-group" id="container"></div>
        <div class='infinite-loading'><span class='fui-preloader'></span><span class='text'> 正在加载...</span></div>

		<div class='content-empty' style='display:none;'>
			<i class='icon icon-group'></i><br/>暂时没有任何数据
		</div>

    </div>


	<script id='tpl_commission_down_list' type='text/html'>
		<%each list as user%>
		<div class="fui-list">
			<div class="fui-list-media">
				<%if user.avatar%>
				<img data-lazy="<%user.avatar%>" class="round">
				<%else%>
				<i class="icon icon-my2"></i>
				<%/if%>
			</div>
			<div class="fui-list-inner">
				<div class="row">
				      <div class="row-text">
					 
					  <%if user.nickname%><%user.nickname%><%else%>未获取<%/if%>
					   <%if user.isagent==1 && user.status==1%>
					  <i class="icon icon-favor text-danger"></i>
					  <%/if%>
				      
				      </div>
				</div>
				<div class="subtitle">
				      <%if user.isagent==1 && user.status==1%>
				    成为<?php  echo $this->set['texts']['agent']?>时间: <%user.agenttime%>
				    <%else%>
				    注册时间:  <%user.createtime%>
				    <%/if%>
				    
				</div>
			</div>
			<div class="row-remark">
				<%if user.isagent==1 && user.status==1%>
				<p>+<%user.commission_total%></p>
				<p><%user.agentcount%>个成员</p>
				<%else%>
				<p>消费: <%user.moneycount%><?php  echo $this->set['texts']['yuan']?></p>
				<p><%user.ordercount%>个订单</p>
				<%/if%>
				
			</div>
		</div>
		<%/each%>
	</script>

	<script language='javascript'>
		require(['../addons/yunphp_shop/plugin/commission/static/js/down.js'], function (modal) {
			modal.init({fromDetail: false});
		});
	</script>
</div>
<?php  $this->footerMenus()?>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>
